<?php

namespace Keirus\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use PUGX\MultiUserBundle\Validator\Constraints\UniqueEntity;

/**
 * SuperAdministrator
 *
 * @ORM\Table("SuperAdministrator")
 * @ORM\Entity
 * @UniqueEntity(fields = "username", targetClass = "Keirus\UserBundle\Entity\User", message="fos_user.username.already_used")
 * @UniqueEntity(fields = "email", targetClass = "Keirus\UserBundle\Entity\User", message="fos_user.email.already_used")
 */
class SuperAdministrator extends User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->addRole('ROLE_SUPERADMIN');
    }

}
