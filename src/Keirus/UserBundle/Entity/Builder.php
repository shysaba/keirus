<?php

namespace Keirus\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use PUGX\MultiUserBundle\Validator\Constraints\UniqueEntity;

/**
 * Builder
 *
 * @ORM\Table("Builder")
 * @ORM\Entity()
 * @UniqueEntity(fields = "username", targetClass = "Keirus\UserBundle\Entity\User", message="fos_user.username.already_used")
 * @UniqueEntity(fields = "email", targetClass = "Keirus\UserBundle\Entity\User", message="fos_user.email.already_used")
 */
class Builder extends User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text")
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="Keirus\LocalizationBundle\Entity\City")
     * @ORM\JoinColumn(nullable=false)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="contactPhone", type="string", length=255)
     */
    private $contactPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="contactMobile", type="string", length=255)
     */
    private $contactMobile;

    /**
     * @var string
     *
     * @ORM\Column(name="contactEmail", type="string", length=255)
     */
    private $contactEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="profile", type="text")
     */
    private $profile;

    /**
     * @ORM\ManyToMany(targetEntity="Keirus\LocalizationBundle\Entity\City", cascade={"persist"})
     * @ORM\JoinTable(name="OperatingCities")
     */
    private $operatingCities;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->addRole('ROLE_BUILDER');
        $this->operatingCities = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Builder
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Builder
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set contactPhone
     *
     * @param string $contactPhone
     * @return Builder
     */
    public function setContactPhone($contactPhone)
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    /**
     * Get contactPhone
     *
     * @return string
     */
    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    /**
     * Set contactMobile
     *
     * @param string $contactMobile
     * @return Builder
     */
    public function setContactMobile($contactMobile)
    {
        $this->contactMobile = $contactMobile;

        return $this;
    }

    /**
     * Get contactMobile
     *
     * @return string
     */
    public function getContactMobile()
    {
        return $this->contactMobile;
    }

    /**
     * Set contactEmail
     *
     * @param string $contactEmail
     * @return Builder
     */
    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    /**
     * Get contactEmail
     *
     * @return string
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * Set profile
     *
     * @param string $profile
     * @return Builder
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile
     *
     * @return string
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set city
     *
     * @param \Keirus\LocalizationBundle\Entity\City $city
     * @return Builder
     */
    public function setCity(\Keirus\LocalizationBundle\Entity\City $city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \Keirus\LocalizationBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return string
     */
    function __toString()
    {
        return $this->name;
    }


    /**
     * Add operatingCity
     *
     * @param \Keirus\LocalizationBundle\Entity\City $operatingCity
     *
     * @return Builder
     */
    public function addOperatingCity(\Keirus\LocalizationBundle\Entity\City $operatingCity)
    {
        $this->operatingCities[] = $operatingCity;

        return $this;
    }

    /**
     * Remove operatingCity
     *
     * @param \Keirus\LocalizationBundle\Entity\City $operatingCity
     */
    public function removeOperatingCity(\Keirus\LocalizationBundle\Entity\City $operatingCity)
    {
        $this->operatingCities->removeElement($operatingCity);
    }

    /**
     * Get operatingCities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOperatingCities()
    {
        return $this->operatingCities;
    }
}
