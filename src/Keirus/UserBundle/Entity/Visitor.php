<?php

namespace Keirus\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use PUGX\MultiUserBundle\Validator\Constraints\UniqueEntity;

/**
 * Visitor
 *
 * @ORM\Table("Visitor")
 * @ORM\Entity
 * @UniqueEntity(fields = "username", targetClass = "Keirus\UserBundle\Entity\User", message="fos_user.username.already_used")
 * @UniqueEntity(fields = "email", targetClass = "Keirus\UserBundle\Entity\User", message="fos_user.email.already_used")
 */
class Visitor extends User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Keirus\LocalizationBundle\Entity\City")
     * @ORM\JoinColumn(nullable=false)
     */
    private $citySearch;

    /**
     * @var boolean
     *
     * @ORM\Column(name="nri", type="boolean")
     */
    private $nri;


    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->addRole('ROLE_VISITOR');
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nri
     *
     * @param boolean $nri
     * @return Visitor
     */
    public function setNri($nri)
    {
        $this->nri = $nri;

        return $this;
    }

    /**
     * Get nri
     *
     * @return boolean
     */
    public function getNri()
    {
        return $this->nri;
    }

    /**
     * Set citySearch
     *
     * @param \Keirus\LocalizationBundle\Entity\City $citySearch
     * @return Visitor
     */
    public function setCitySearch(\Keirus\LocalizationBundle\Entity\City $citySearch)
    {
        $this->citySearch = $citySearch;

        return $this;
    }

    /**
     * Get citySearch
     *
     * @return \Keirus\LocalizationBundle\Entity\City
     */
    public function getCitySearch()
    {
        return $this->citySearch;
    }
}
