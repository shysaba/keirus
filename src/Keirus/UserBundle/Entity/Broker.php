<?php

namespace Keirus\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use PUGX\MultiUserBundle\Validator\Constraints\UniqueEntity;

/**
 * Broker
 *
 * @ORM\Table("Broker")
 * @ORM\Entity()
 * @UniqueEntity(fields = "username", targetClass = "Keirus\UserBundle\Entity\User", message="fos_user.username.already_used")
 * @UniqueEntity(fields = "email", targetClass = "Keirus\UserBundle\Entity\User", message="fos_user.email.already_used")
 */
class Broker extends User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="agreementNo", type="string", length=255)
     */
    private $agreementNo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="verifiedAccount", type="boolean")
     */
    private $verifiedAccount;


    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        $this->verifiedAccount = false;
        $this->addRole('ROLE_BROKER');
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set agreementNo
     *
     * @param string $agreementNo
     * @return Broker
     */
    public function setAgreementNo($agreementNo)
    {
        $this->agreementNo = $agreementNo;

        return $this;
    }

    /**
     * Get agreementNo
     *
     * @return string
     */
    public function getAgreementNo()
    {
        return $this->agreementNo;
    }

    /**
     * Set verifiedAccount
     *
     * @param boolean $verifiedAccount
     * @return Broker
     */
    public function setVerifiedAccount($verifiedAccount)
    {
        $this->verifiedAccount = $verifiedAccount;

        return $this;
    }

    /**
     * Get verifiedAccount
     *
     * @return boolean
     */
    public function getVerifiedAccount()
    {
        return $this->verifiedAccount;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Broker
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }
}
