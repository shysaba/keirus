<?php

namespace Keirus\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * User
 *
 * @ORM\Table("User")
 * @ORM\Entity(repositoryClass="Keirus\UserBundle\Repository\UserRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorMap({"broker": "Broker", "individual": "Individual", "visitor": "Visitor", "builder": "Builder", "user": "User", "administrator": "Administrator", "superAdministrator": "SuperAdministrator"})
 */
abstract class User extends BaseUser
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255)
     */
    private $lastName;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creationDate", type="datetime")
     */
    private $creationDate;


    /**
     * @var boolean
     *
     * @ORM\Column(name="optInMailing", type="boolean")
     */
    private $optInMailing;

    /**
     * @ORM\ManyToOne(targetEntity="Keirus\UserBundle\Entity\Language")
     * @ORM\JoinColumn(nullable=true)
     */
    private $preferredLanguage;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->creationDate = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }


    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return User
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }


    /**
     * Set optInMailing
     *
     * @param boolean $optInMailing
     * @return User
     */
    public function setOptInMailing($optInMailing)
    {
        $this->optInMailing = $optInMailing;

        return $this;
    }

    /**
     * Get optInMailing
     *
     * @return boolean
     */
    public function getOptInMailing()
    {
        return $this->optInMailing;
    }

    /**
     * Set preferredLanguage
     *
     * @param \Keirus\UserBundle\Entity\Language $preferredLanguage
     * @return User
     */
    public function setPreferredLanguage(\Keirus\UserBundle\Entity\Language $preferredLanguage)
    {
        $this->preferredLanguage = $preferredLanguage;

        return $this;
    }

    /**
     * Get preferredLanguage
     *
     * @return \Keirus\UserBundle\Entity\Language
     */
    public function getPreferredLanguage()
    {
        return $this->preferredLanguage;
    }

}
