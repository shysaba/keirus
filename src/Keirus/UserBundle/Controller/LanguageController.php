<?php
namespace Keirus\UserBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Keirus\CoreBundle\Manager\BasePaginatorManager;
use Keirus\UserBundle\Form\Type\LanguageForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keirus\UserBundle\Entity\Language;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class LanguageController
 * @package Keirus\UserBundle\Controller
 */
class LanguageController extends Controller
{

    /**
     * @return mixed
     */
    public function listAction($page, $key)
    {

        return $this->dataResult($page, $key);
    }

    /**
     * @param $page
     * @param $key
     * @return Response
     */
    private function dataResult($page, $key)
    {

        $manager = $this->get('user_language_manager');
        $rpp = $this->container->getParameter('elements_per_page');


        list($result, $totalCount) = $manager->getAllLanguagesPaginated($page, $rpp, $key);

        $paginator = new BasePaginatorManager($page, $totalCount, $rpp);

        if ($totalCount < 1) {
            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.alert.info',
                'admin.search.noResults'
            );
        }

        return $this->render(
            'KeirusUserBundle:Language:list.html.twig',
            array(
                'languages' => $result,
                'paginator' => $paginator->getPagesList(),
                'cur' => $page,
                'total' => $paginator->getTotalPages(),
                'key' => $key
            )
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $key = $request->request->get('key');
        $page = 1;

        return $this->dataResult($page, $key);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction()
    {

        $formHandler = $this->get('user_language_handler');

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'info',
                'admin.action.created',
                'admin.user.language.created'
            );

            return $this->redirect(
                $this->generateUrl('user_language_list')
            );
        }

        return $this->render(
            'KeirusUserBundle:Language:create.html.twig',
            array('form' => $formHandler->getForm()->createView())
        );
    }


    /**
     * @param $id
     * @return RedirectResponse|Response
     */
    public function updateAction($id)
    {

        $manager = $this->get('user_language_manager');
        $language = $manager->getLanguage($id);

        $formHandler = $this->get('user_language_handler');
        $form = $this->createForm(new LanguageForm(), $language);
        $formHandler->setForm($form);

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'success',
                'admin.action.updated',
                'admin.user.language.updated'
            );

            return $this->redirect(
                $this->generateUrl('user_language_list')
            );
        }

        return $this->render(
            'KeirusUserBundle:Language:update.html.twig',
            array(
                'form' => $formHandler->getForm()->createView(),
                'language' => $language
            )
        );
    }


    /**
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction($id)
    {
        $manager = $this->get('user_language_manager');
        $language = $manager->getLanguage($id);

        if ($language instanceof Language) {
            $manager->removeAndFlush($language);

            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.action.deleted',
                'admin.user.language.deleted'
            );
        } else {
            throw new NotFoundHttpException("Language not found");
        }

        return $this->redirect($this->generateUrl('user_language_list'));
    }


    /**
     * @param $id
     * @return Response
     * @throws EntityNotFoundException
     */
    public function showAction($id)
    {
        $manager = $this->get('user_language_manager');
        $language = $manager->getLanguage($id);


        return $this->render('KeirusUserBundle:Language:show.html.twig', array('language' => $language));
    }
}