<?php
namespace Keirus\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\RestBundle\Controller\Annotations as Rest;


/**
 * Class IndividualController
 * @package Keirus\HousingStockBundle\Controller
 */
class IndividualController extends Controller
{

    /**
     * @return RedirectResponse
     */
    public function registerAction()
    {
        return $this->container
            ->get('pugx_multi_user.registration_manager')
            ->register('Keirus\UserBundle\Entity\Individual');
    }
}