<?php
namespace Keirus\UserBundle\Controller;

use Keirus\CoreBundle\Manager\BasePaginatorManager;
use Keirus\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class UserController
 * @package Keirus\UserBundle\Controller
 */
class UserController extends Controller
{

    /**
     * @return mixed
     */
    public function listAction($page, $key)
    {

        return $this->dataResult($page, $key);
    }


    /**
     * @param $page
     * @param $key
     * @return Response
     */
    private function dataResult($page, $key)
    {

        $manager = $this->get('user_user_manager');
        $rpp = $this->container->getParameter('elements_per_page');


        list($result, $totalCount) = $manager->getAllUsersPaginated($page, $rpp, $key);

        $paginator = new BasePaginatorManager($page, $totalCount, $rpp);

        if ($totalCount < 1) {
            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.alert.info',
                'admin.search.noResults'
            );
        }

        return $this->render(
            'KeirusUserBundle:User:list.html.twig',
            array(
                'users' => $result,
                'paginator' => $paginator->getPagesList(),
                'cur' => $page,
                'total' => $paginator->getTotalPages(),
                'key' => $key
            )
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $key = $request->request->get('key');
        $page = 1;

        return $this->dataResult($page, $key);
    }


    /**
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction($id)
    {
        $manager = $this->get('user_user_manager');
        $role = $manager->getUser($id);

        if ($role instanceof User) {
            $manager->removeAndFlush($role);

            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.action.deleted',
                'admin.user.user.deleted'
            );
        } else {
            throw new NotFoundHttpException("User not found");
        }

        return $this->redirect($this->generateUrl('user_user_list'));
    }


    /**
     * @param $id
     * @return Response
     */
    public function showAction($id)
    {
        $manager = $this->get('user_user_manager');
        $user = $manager->getUser($id);


        return $this->render('KeirusUserBundle:User:show.html.twig', array('user' => $user));
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getRolesAction(Request $request)
    {
        $term = $request->query->get('term', null);

        $manager = $this->get('user_role_manager');
        $states = $manager->getAllRolesBy($term, 'r.label', 'ASC');

        return new JsonResponse($states);
    }
}