<?php
namespace Keirus\UserBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Keirus\CoreBundle\Manager\BasePaginatorManager;
use Keirus\UserBundle\Form\Type\RoleForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keirus\UserBundle\Entity\Role;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class RoleController
 * @package Keirus\UserBundle\Controller
 */
class RoleController extends Controller
{

    /**
     * @return mixed
     */
    public function listAction($page, $key)
    {

        return $this->dataResult($page, $key);
    }

    /**
     * @param $page
     * @param $key
     * @return Response
     */
    private function dataResult($page, $key)
    {

        $manager = $this->get('user_role_manager');
        $rpp = $this->container->getParameter('elements_per_page');


        list($result, $totalCount) = $manager->getAllRolesPaginated($page, $rpp, $key);

        $paginator = new BasePaginatorManager($page, $totalCount, $rpp);

        if ($totalCount < 1) {
            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.alert.info',
                'admin.search.noResults'
            );
        }

        return $this->render(
            'KeirusUserBundle:Role:list.html.twig',
            array(
                'roles' => $result,
                'paginator' => $paginator->getPagesList(),
                'cur' => $page,
                'total' => $paginator->getTotalPages(),
                'key' => $key
            )
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $key = $request->request->get('key');
        $page = 1;

        return $this->dataResult($page, $key);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction()
    {

        $formHandler = $this->get('user_role_handler');

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'info',
                'admin.action.created',
                'admin.user.role.created'
            );

            return $this->redirect(
                $this->generateUrl('user_role_list')
            );
        }

        return $this->render(
            'KeirusUserBundle:Role:create.html.twig',
            array('form' => $formHandler->getForm()->createView())
        );
    }


    /**
     * @param $id
     * @return RedirectResponse|Response
     */
    public function updateAction($id)
    {

        $manager = $this->get('user_role_manager');
        $role = $manager->getRole($id);

        $formHandler = $this->get('user_role_handler');
        $form = $this->createForm(new RoleForm(), $role);
        $formHandler->setForm($form);

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'success',
                'admin.action.updated',
                'admin.user.role.updated'
            );

            return $this->redirect(
                $this->generateUrl('user_role_list')
            );
        }

        return $this->render(
            'KeirusUserBundle:Role:update.html.twig',
            array(
                'form' => $formHandler->getForm()->createView(),
                'role' => $role
            )
        );
    }


    /**
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction($id)
    {
        $manager = $this->get('user_role_manager');
        $role = $manager->getRole($id);

        if ($role instanceof Role) {
            $manager->removeAndFlush($role);

            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.action.deleted',
                'admin.user.role.deleted'
            );
        } else {
            throw new NotFoundHttpException("Role not found");
        }

        return $this->redirect($this->generateUrl('user_role_list'));
    }


    /**
     * @param $id
     * @return Response
     * @throws EntityNotFoundException
     */
    public function showAction($id)
    {
        $manager = $this->get('user_role_manager');
        $role = $manager->getRole($id);


        return $this->render('KeirusUserBundle:Role:show.html.twig', array('role' => $role));
    }
}