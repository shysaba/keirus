<?php

namespace Keirus\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class KeirusUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
