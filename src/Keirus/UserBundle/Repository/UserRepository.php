<?php

namespace Keirus\UserBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class UserRepository
 * @package Keirus\UserBundle\Repository
 */
class UserRepository extends EntityRepository
{

    /**
     * Find all roles
     * @param string $sort
     * @param string $order
     * @return array
     */
    public function getAllUsersOrderBy($sort, $order)
    {
        return $this->createQueryBuilder('u')
            ->orderBy($sort, $order)
            ->getQuery();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllUsersPaginated($page, $rpp, $key = null)
    {
        $count = $this->getCountUsers($key);

        $query = $this->createQueryBuilder('u')
            ->setMaxResults($rpp)
            ->setFirstResult(($page - 1) * $rpp)
            ->orderBy('u.firstName', 'DESC');

        if ($key <> null) {
            $query->Where('u.firstName LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $result = $query->getQuery()
            ->getResult();


        return array($result, $count);
    }

    /**
     * @param $key
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getCountUsers($key)
    {
        $query = $this->createQueryBuilder('u');
        $query->select('COUNT(u)');

        if (!empty($key)) {
            $query->Where('u.firstName LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $menu = $query->getQuery()
            ->getSingleScalarResult();

        return $menu;
    }


    /**
     * Find all roles by terms
     * @param $search
     * @param $sort
     * @param $order
     * @return array
     */
    public function getAllUsersBy($search, $sort, $order)
    {

        return $this->createQueryBuilder('u')
            ->select('u.id', 'u.firstName as value')
            ->Where('u.firstName LIKE :firstName')
            ->setParameter('firstName', '%'.$search.'%')
            ->orderBy($sort, $order)
            ->getQuery()
            ->getArrayResult();
    }
}