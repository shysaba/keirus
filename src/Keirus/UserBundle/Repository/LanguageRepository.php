<?php

namespace Keirus\UserBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class LanguageRepository
 * @package Keirus\UserBundle\Repository
 */
class LanguageRepository extends EntityRepository
{

    /**
     * Find all languages
     * @param string $sort
     * @param string $order
     * @return array
     */
    public function getAllLanguagesOrderBy($sort, $order)
    {
        return $this->createQueryBuilder('l')
            ->orderBy($sort, $order)
            ->getQuery();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllLanguagesPaginated($page, $rpp, $key = null)
    {
        $count = $this->getCountLanguages($key);

        $query = $this->createQueryBuilder('l')
            ->setMaxResults($rpp)
            ->setFirstResult(($page - 1) * $rpp)
            ->orderBy('l.name', 'DESC');

        if ($key <> null) {
            $query->Where('l.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $result = $query->getQuery()
            ->getResult();


        return array($result, $count);
    }

    /**
     * @param $key
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getCountLanguages($key)
    {
        $query = $this->createQueryBuilder('l');
        $query->select('COUNT(l)');

        if (!empty($key)) {
            $query->Where('l.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $menu = $query->getQuery()
            ->getSingleScalarResult();

        return $menu;
    }
}