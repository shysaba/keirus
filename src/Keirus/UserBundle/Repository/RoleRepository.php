<?php

namespace Keirus\UserBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class RoleRepository
 * @package Keirus\UserBundle\Repository
 */
class RoleRepository extends EntityRepository
{

    /**
     * Find all roles
     * @param string $sort
     * @param string $order
     * @return array
     */
    public function getAllRolesOrderBy($sort, $order)
    {
        return $this->createQueryBuilder('r')
            ->orderBy($sort, $order)
            ->getQuery();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllRolesPaginated($page, $rpp, $key = null)
    {
        $count = $this->getCountRoles($key);

        $query = $this->createQueryBuilder('r')
            ->setMaxResults($rpp)
            ->setFirstResult(($page - 1) * $rpp)
            ->orderBy('r.name', 'DESC');

        if ($key <> null) {
            $query->Where('r.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $result = $query->getQuery()
            ->getResult();


        return array($result, $count);
    }

    /**
     * @param $key
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getCountRoles($key)
    {
        $query = $this->createQueryBuilder('r');
        $query->select('COUNT(r)');

        if (!empty($key)) {
            $query->Where('r.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $menu = $query->getQuery()
            ->getSingleScalarResult();

        return $menu;
    }


    /**
     * Find all roles by terms
     * @param $search
     * @param $sort
     * @param $order
     * @return array
     */
    public function getAllRolesBy($search, $sort, $order)
    {

        return $this->createQueryBuilder('r')
            ->select('r.id', 'r.name as value')
            ->Where('r.name LIKE :name')
            ->setParameter('name', '%'.$search.'%')
            ->orderBy($sort, $order)
            ->getQuery()
            ->getArrayResult();
    }
}