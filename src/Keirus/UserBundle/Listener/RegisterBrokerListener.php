<?php

namespace Keirus\UserBundle\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Keirus\UserBundle\Manager\RoleManager;
use Keirus\UserBundle\Entity\Broker;


class RegisterBrokerListener
{


    private $roleManager;


    public function __construct(RoleManager $roleManager)
    {
        $this->roleManager = $roleManager;
    }


    public function prePersist(LifecycleEventArgs $args)
    {
        $broker = $args->getEntity();
        $entityManager = $args->getEntityManager();

        $role = $this->roleManager->getRole('1');


        if ($broker instanceof Broker) {
            $broker->addRole('ROLE_WHATEVER');
            // or
            // $entity->addGroup('students');
            // ...
        }
    }
}