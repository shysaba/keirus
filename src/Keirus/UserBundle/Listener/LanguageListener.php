<?php

namespace Keirus\UserBundle\Listener;

use Keirus\CoreBundle\Manager\LoggerManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

/**
 * Class LanguageListener
 * @package Keirus\UserBundle\Listener
 */
class LanguageListener
{

    /**
     * @var LoggerManager
     */
    private $loggerManager;

    /**
     * @param LoggerManager $loggerManager
     */
    public function __construct(LoggerManager $loggerManager)
    {
        $this->loggerManager = $loggerManager;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function process(GetResponseEvent $event)
    {
        if ($event->getRequest()->attributes->get('_route') == "user_language_delete") {
            $this->loggerManager->write();
        }
    }
}