<?php

namespace Keirus\UserBundle\Listener;

use Keirus\CoreBundle\Manager\LoggerManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

/**
 * Class RoleListener
 * @package Keirus\UserBundle\Listener
 */
class RoleListener
{

    /**
     * @var LoggerManager
     */
    private $loggerManager;

    /**
     * @param LoggerManager $loggerManager
     */
    public function __construct(LoggerManager $loggerManager)
    {
        $this->loggerManager = $loggerManager;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function process(GetResponseEvent $event)
    {
        if ($event->getRequest()->attributes->get('_route') == "user_role_delete") {
            $this->loggerManager->write();
        }
    }
}