<?php
namespace Keirus\UserBundle\Manager;

use Doctrine\ORM\EntityManager;
use Keirus\UserBundle\Entity\Role;


use Keirus\UserBundle\Repository\RoleRepository;

class RoleHierarchyManager extends \Symfony\Component\Security\Core\Role\RoleHierarchy
{
    private $em;


    public function __construct(array $hierarchy, EntityManager $em)
    {
        $this->em = $em;
        parent::__construct($this->buildRolesTree());
    }


    private function buildRolesTree()
    {
        $hierarchy = array();
        $roles = $this->em->createQuery('select r from KeirusUserBundle:Role r')->execute();
        foreach ($roles as $role) {
            /** @var $role Role */
            if ($role->getParent()) {
                if (!isset($hierarchy[$role->getParent()->getName()])) {
                    $hierarchy[$role->getParent()->getName()] = array();
                }
                $hierarchy[$role->getParent()->getName()][] = $role->getName();
            } else {
                if (!isset($hierarchy[$role->getName()])) {
                    $hierarchy[$role->getName()] = array();
                }
            }
        }

        return $hierarchy;
    }
}