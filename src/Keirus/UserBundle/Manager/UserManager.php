<?php
namespace Keirus\UserBundle\Manager;

use Keirus\CoreBundle\Manager\BaseManager;
use Keirus\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;


/**
 * Class UserManager
 * @package Keirus\UserBundle\Manager
 */
class UserManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;


    /**
     * @var \Keirus\UserBundle\Repository\UserRepository
     */
    protected $repository;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('KeirusUserBundle:User');
    }
    

    /**
     * Delete a role
     * @param User $user
     * @param boolean $flush
     */
    public function delete(User $user, $flush = true)
    {
        if ($flush) {
            $this->removeAndFlush($user);
        } else {
            $this->remove($user);
        }
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllUsersPaginated($page, $rpp, $key = null)
    {
        return $this->repository->getAllUsersPaginated($page, $rpp, $key);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getUser($id)
    {
        return $this->repository->find($id);
    }


    /**
     * @param $search
     * @param $sort
     * @param $order
     * @return array
     */
    public function getAllUsersBy($search, $sort, $order)
    {
        return $this->repository->getAllUsersBy($search, $sort, $order);
    }
}