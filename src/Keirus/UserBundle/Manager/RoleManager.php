<?php
namespace Keirus\UserBundle\Manager;

use Keirus\CoreBundle\Manager\BaseManager;
use Keirus\UserBundle\Entity\Role;
use Doctrine\ORM\EntityManager;


/**
 * Class RoleManager
 * @package Keirus\UserBundle\Manager
 */
class RoleManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;


    /**
     * @var \Keirus\UserBundle\Repository\RoleRepository
     */
    protected $repository;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('KeirusUserBundle:Role');
    }

    /**
     * Persist and flush a unit type
     * @param Role $role
     */
    public function save(Role $role)
    {
        $this->persistAndFlush($role);
    }

    /**
     * Delete a unit type
     * @param Role $role
     * @param boolean $flush
     */
    public function delete(Role $role, $flush = true)
    {
        if ($flush) {
            $this->removeAndFlush($role);
        } else {
            $this->remove($role);
        }
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllRolesPaginated($page, $rpp, $key = null)
    {
        return $this->repository->getAllRolesPaginated($page, $rpp, $key);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getRole($id)
    {
        return $this->repository->find($id);
    }


    /**
     * @param $search
     * @param $sort
     * @param $order
     * @return array
     */
    public function getAllRolesBy($search, $sort, $order)
    {
        return $this->repository->getAllRolesBy($search, $sort, $order);
    }
}