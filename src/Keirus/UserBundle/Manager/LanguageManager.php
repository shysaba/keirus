<?php
namespace Keirus\UserBundle\Manager;

use Keirus\CoreBundle\Manager\BaseManager;
use Keirus\UserBundle\Entity\Language;
use Doctrine\ORM\EntityManager;


/**
 * Class LanguageManager
 * @package Keirus\UserBundle\Manager
 */
class LanguageManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;


    /**
     * @var \Keirus\UserBundle\Repository\LanguageRepository
     */
    protected $repository;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('KeirusUserBundle:Language');
    }

    /**
     * Persist and flush a unit type
     * @param Language $language
     */
    public function save(Language $language)
    {
        $this->persistAndFlush($language);
    }

    /**
     * Delete a unit type
     * @param Language $language
     * @param boolean $flush
     */
    public function delete(Language $language, $flush = true)
    {
        if ($flush) {
            $this->removeAndFlush($language);
        } else {
            $this->remove($language);
        }
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllLanguagesPaginated($page, $rpp, $key = null)
    {
        return $this->repository->getAllLanguagesPaginated($page, $rpp, $key);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getLanguage($id)
    {
        return $this->repository->find($id);
    }
}