<?php

namespace Keirus\UserBundle\Form\Handler;


use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Keirus\CoreBundle\Form\Handler\BaseHandler;
use Keirus\UserBundle\Manager\LanguageManager;

/**
 * Class LanguageHandler
 * @package Keirus\RealEstateBundle\Form\Handler
 */
class LanguageHandler extends BaseHandler
{


    /**
     * @var LanguageManager
     */
    protected $languageManager;


    /**
     * @param Form $form
     * @param Request $request
     * @param SecurityContext $security
     * @param LanguageManager $languageManager
     */
    public function __construct(
        Form $form,
        Request $request,
        SecurityContext $security,
        LanguageManager $languageManager
    ) {
        parent::__construct($form, $request, $security);
        $this->propertyTypeManager = $languageManager;
    }


    /**
     *
     */
    protected function onSuccess()
    {
        $entity = $this->form->getData();

        $this->propertyTypeManager->save($entity);
    }

}