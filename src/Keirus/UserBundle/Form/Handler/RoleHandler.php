<?php

namespace Keirus\UserBundle\Form\Handler;


use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Keirus\CoreBundle\Form\Handler\BaseHandler;
use Keirus\UserBundle\Manager\RoleManager;

/**
 * Class RoleHandler
 * @package Keirus\RealEstateBundle\Form\Handler
 */
class RoleHandler extends BaseHandler
{


    /**
     * @var RoleManager
     */
    protected $roleManager;


    /**
     * @param Form $form
     * @param Request $request
     * @param SecurityContext $security
     * @param RoleManager $roleManager
     */
    public function __construct(
        Form $form,
        Request $request,
        SecurityContext $security,
        RoleManager $roleManager
    ) {
        parent::__construct($form, $request, $security);
        $this->propertyTypeManager = $roleManager;
    }


    /**
     *
     */
    protected function onSuccess()
    {
        $entity = $this->form->getData();

        $this->propertyTypeManager->save($entity);
    }

}