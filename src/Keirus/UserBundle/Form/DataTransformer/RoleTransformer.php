<?php

namespace Keirus\UserBundle\Form\DataTransformer;

use Keirus\CoreBundle\Form\DataTransformer\BaseTransformer;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class RoleTransformer
 * @package Keirus\UserBundle\Form\DataTransformer
 */
class RoleTransformer extends BaseTransformer
{

    /**
     * @param ObjectManager $objectManager
     * @param $entityClass
     */
    public function __construct(ObjectManager $objectManager, $entityClass)
    {
        parent::__construct($objectManager, $entityClass);
    }

    /**
     * @param mixed $id
     * @return object
     */
    public function reverseTransform($id)
    {
        $entity = parent::getObjectManager()->getRepository($this->entityClass)->find($id);

        if (null === $entity) {
            return null;
        }

        return $entity;
    }
}