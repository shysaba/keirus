<?php

namespace Keirus\UserBundle\Form\DataTransformer;

use Keirus\CoreBundle\Form\DataTransformer\BaseTransformer;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class CityTransformer
 * @package Keirus\UserBundle\Form\DataTransformer
 */
class CityTransformer extends BaseTransformer
{

    /**
     * @param ObjectManager $objectManager
     * @param $entityClass
     */
    public function __construct(ObjectManager $objectManager, $entityClass)
    {
        parent::__construct($objectManager, $entityClass);
    }

}