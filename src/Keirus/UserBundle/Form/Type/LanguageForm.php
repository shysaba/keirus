<?php

namespace Keirus\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class LanguageForm
 * @package Keirus\UserBundle\Form\Type
 */
class LanguageForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('label' => 'admin.user.language.name', 'error_bubbling' => true))
            ->add('label', 'text', array('label' => 'admin.user.language.label', 'error_bubbling' => true))
            ->add('submit', 'submit');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'translation_domain' => 'admin',
                'data_class' => 'Keirus\UserBundle\Entity\Language'
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user_language_form';
    }
}