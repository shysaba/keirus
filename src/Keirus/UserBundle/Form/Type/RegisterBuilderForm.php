<?php

namespace Keirus\UserBundle\Form\Type;

use FOS\UserBundle\Propel\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Keirus\UserBundle\Form\Type\UserForm;

/**
 * Class RegisterBuilderForm
 * @package Keirus\UserBundle\Form\Type
 */
class RegisterBuilderForm extends UserForm
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('name', 'text', array('label' => 'admin.user.builder.name', 'error_bubbling' => true))
            ->add('address', 'text', array('label' => 'admin.user.builder.address', 'error_bubbling' => true))
            ->add(
                'city',
                'autocomplete_city',
                [
                    'label' => 'admin.user.builder.city',
                    'error_bubbling' => true,
                    'class' => 'KeirusLocalizationBundle:City',
                    'update_route' => 'localization_cities_list',
                    'required' => true,
                    'attr' => ['class' => 'form-control']
                ]
            )
            ->add('contactPhone', 'number', array('label' => 'admin.user.builder.contactPhone', 'error_bubbling' => true))
            ->add('contactMobile', 'number', array('label' => 'admin.user.builder.contactMobile', 'error_bubbling' => true))
            ->add('contactEmail', 'email', array('label' => 'admin.user.builder.contactEmail', 'error_bubbling' => true))
            ->add('profile', 'textarea', array('label' => 'admin.user.builder.profile', 'error_bubbling' => true, 'attr' => array('class' => 'textarea textareaHTML')))
;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'translation_domain' => 'admin',
                'data_class' => 'Keirus\UserBundle\Entity\Builder'
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user_builder_form';
    }
}