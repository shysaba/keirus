<?php

namespace Keirus\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class RoleForm
 * @package Keirus\UserBundle\Form\Type
 */
class RoleForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('label' => 'admin.user.role.name', 'error_bubbling' => true))
            ->add('label', 'text', array('label' => 'admin.user.role.label', 'error_bubbling' => true))
            ->add(
                'parent',
                'autocomplete_role',
                [
                    'label' => 'admin.user.role.parent',
                    'error_bubbling' => true,
                    'class' => 'KeirusUserBundle:Role',
                    'update_route' => 'user_roles_list',
                    'required' => false,
                    'attr' => ['class' => 'form-control']
                ]
            )
            ->add('submit', 'submit');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'translation_domain' => 'admin',
                'data_class' => 'Keirus\UserBundle\Entity\Role'
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user_role_form';
    }
}