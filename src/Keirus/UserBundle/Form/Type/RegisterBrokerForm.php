<?php

namespace Keirus\UserBundle\Form\Type;

use FOS\UserBundle\Propel\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Keirus\UserBundle\Form\Type\UserForm;

/**
 * Class RegisterBrokerForm
 * @package Keirus\UserBundle\Form\Type
 */
class RegisterBrokerForm extends UserForm
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('phone', 'text', array('label' => 'admin.user.broker.phone', 'error_bubbling' => true))
            ->add('agreementNo', 'text', array('label' => 'admin.user.broker.agreementNo', 'error_bubbling' => true))
;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'translation_domain' => 'admin',
                'data_class' => 'Keirus\UserBundle\Entity\Broker'
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user_broker_form';
    }
}