<?php

namespace Keirus\UserBundle\Form\Type;

use FOS\UserBundle\Propel\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Keirus\UserBundle\Form\Type\UserForm;

class RegisterSuperAdministratorForm extends UserForm
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        
;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'translation_domain' => 'admin',
                'data_class' => 'Keirus\UserBundle\Entity\SuperAdministrator'
            ]
        );
    }

    public function getName()
    {
        return 'user_super_administrator_form';
    }
}