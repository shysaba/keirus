<?php

namespace Keirus\UserBundle\Form\Type;

use FOS\UserBundle\Propel\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Keirus\UserBundle\Form\Type\UserForm;

class RegisterVisitorForm extends UserForm
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add(
                'citySearch',
                'autocomplete_city',
                [
                    'label' => 'admin.user.visitor.citySearch',
                    'class' => 'KeirusLocalizationBundle:City',
                    'update_route' => 'localization_cities_list',
                    'required' => true,
                    'attr' => ['class' => 'form-control']
                ]
            )
            ->add(
                'nri',
                'choice',
                array(
                    'label' => 'admin.user.visitor.nri',
                    'attr' => ['class' => 'col-sm-8'],
                    'choices' => [
                        '1' => 'admin.choice.yes',
                        '0' => 'admin.choice.no'

                    ],
                    'required' => true,
                    'expanded' => true,
                    'multiple' => false
                )
            );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'translation_domain' => 'admin',
                'data_class' => 'Keirus\UserBundle\Entity\Visitor'
            ]
        );
    }

    public function getName()
    {
        return 'user_visitor_form';
    }
}