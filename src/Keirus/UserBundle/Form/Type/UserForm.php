<?php

namespace Keirus\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class UserForm
 * @package Keirus\UserBundle\Form\Type
 */
class UserForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', 'text', array('label' => 'admin.user.user.firstName', 'error_bubbling' => true))
            ->add('lastName', 'text', array('label' => 'admin.user.user.lastName', 'error_bubbling' => true))
            ->add('email', 'email', array('label' => 'admin.user.user.email', 'error_bubbling' => true))
            ->add(
                'preferredLanguage',
                'entity',
                array(
                    'class' => 'KeirusUserBundle:Language',
                    'property' => 'label',
                    'multiple' => false,
                    'label' => 'admin.user.user.preferredLanguage',
                    'error_bubbling' => true
                )
            )
            ->add('username', 'text', array('label' => 'admin.user.user.userName', 'error_bubbling' => true))
            ->add(
                'plainPassword',
                'repeated',
                array(
                    'type' => 'password',
                    'options' => array('translation_domain' => 'FOSUserBundle'),
                    'first_options' => array('label' => 'form.password', 'attr' => ['class' => 'form-control']),
                    'second_options' => array(
                        'label' => 'form.password_confirmation',
                        'attr' => ['class' => 'form-control']
                    ),
                    'invalid_message' => 'fos_user.password.mismatch',
                )
            )
            ->add(
                'optInMailing',
                'choice',
                array(
                    'label' => 'admin.user.user.optInMailing',
                    'error_bubbling' => true,
                    'attr' => ['class' => 'col-sm-8'],
                    'choices' => [
                        '1' => 'admin.choice.yes',
                        '0' => 'admin.choice.no'

                    ],
                    'required' => true,
                    'expanded' => true,
                    'multiple' => false
                )
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'translation_domain' => 'admin',
                'data_class' => 'Keirus\UserBundle\Entity\User'
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user_user_form';
    }
}