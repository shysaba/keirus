<?php

namespace Keirus\UserBundle\Form\Type;

use Keirus\CoreBundle\Form\Type\BaseAutoComplete;
use Keirus\UserBundle\Form\DataTransformer\RoleTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\Common\Persistence\ObjectManager;


/**
 * Class RoleAutoComplete
 * @package Keirus\UserBundle\Form\Type
 */
class RoleAutoComplete extends BaseAutoComplete
{


    /**
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        parent::__construct($objectManager);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $class = $options['class'];

        $transformer = new RoleTransformer(parent::getObjectManager(), $class);
        $builder->addViewTransformer($transformer);
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'autocomplete_role';
    }
}