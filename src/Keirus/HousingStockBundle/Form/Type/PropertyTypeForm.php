<?php

namespace Keirus\HousingStockBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class PropertyTypeForm
 * @package Keirus\HousingStockBundle\Form\Type
 */
class PropertyTypeForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', 'text', array('label' => 'admin.housingStock.propertyType.name', 'error_bubbling' => true))
            ->add('submit', 'submit');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'translation_domain' => 'admin',
                'data_class' => 'Keirus\HousingStockBundle\Entity\PropertyType'
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'housingstock_propertytype_form';
    }
}