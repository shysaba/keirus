<?php

namespace Keirus\HousingStockBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class ProjectFileForm
 * @package Keirus\HousingStockBundle\Form\Type
 */
class ProjectFileForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array('label' => 'admin.housingStock.projectFile.name', 'error_bubbling' => true))
            ->add(
                'file',
                'file',
                array(
                    'label' => 'admin.housingStock.projectFile.file',
                    'required' => false,
                    'image_path' => 'webPath',
                    'error_bubbling' => true
                )
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'translation_domain' => 'admin',
                'data_class' => 'Keirus\HousingStockBundle\Entity\ProjectFile',
                'cascade_validation' => true
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'housingstock_projectfile_form';
    }
}