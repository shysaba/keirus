<?php

namespace Keirus\HousingStockBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class ProjectPropertyForm
 * @package Keirus\HousingStockBundle\Form\Type
 */
class ProjectPropertyForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'reference',
                'text',
                array('label' => 'admin.housingStock.projectProperty.reference', 'error_bubbling' => true)
            )
            ->add(
                'propertyType',
                'entity',
                array(
                    'class' => 'KeirusHousingStockBundle:PropertyType',
                    'property' => 'type',
                    'multiple' => false,
                    'error_bubbling' => true,
                    'label' => 'admin.housingStock.projectProperty.propertyType'
                )
            )
            ->add(
                'unitType',
                'entity',
                array(
                    'class' => 'KeirusHousingStockBundle:UnitType',
                    'property' => 'type',
                    'multiple' => false,
                    'label' => 'admin.housingStock.projectProperty.unitType',
                    'error_bubbling' => true
                )
            )
            ->add(
                'sqftArea',
                'number',
                array('label' => 'admin.housingStock.projectProperty.sqftArea', 'error_bubbling' => true)
            )
            ->add(
                'price',
                'number',
                array('label' => 'admin.housingStock.projectProperty.price', 'error_bubbling' => true)
            )
            ->add(
                'priceAvailable',
                'choice',
                array(
                    'label' => 'admin.housingStock.projectProperty.priceAvailable',
                    'error_bubbling' => true,
                    'attr' => ['class' => 'col-sm-8'],
                    'choices' => [
                        '1' => 'admin.choice.yes',
                        '0' => 'admin.choice.no'

                    ],
                    'required' => true,
                    'expanded' => true,
                    'multiple' => false
                )
            )
            ->add(
                'available',
                'choice',
                array(
                    'label' => 'admin.housingStock.projectProperty.available',
                    'error_bubbling' => true,
                    'attr' => ['class' => 'col-sm-8'],
                    'choices' => [
                        '1' => 'admin.choice.yes',
                        '0' => 'admin.choice.no'

                    ],
                    'required' => true,
                    'expanded' => true,
                    'multiple' => false
                )
            )
            ->add(
                'attachedFile',
                new ProjectPropertyFileForm(),
                [
                    'label' => 'admin.housingStock.projectProperty.attachedFile',
                    'error_bubbling' => true,
                    'data_class' => 'Keirus\HousingStockBundle\Entity\ProjectPropertyFile',
                ]
            )
            ->add('submit', 'submit', array('attr' => ['class' => 'btn btn-lg btn-block bg-olive margin']));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'translation_domain' => 'admin',
                'data_class' => 'Keirus\HousingStockBundle\Entity\ProjectProperty',
                'cascade_validation' => true
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'housingstock_projectproperty_form';
    }
}