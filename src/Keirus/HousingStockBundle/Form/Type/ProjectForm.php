<?php

namespace Keirus\HousingStockBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class ProjectForm
 * @package Keirus\HousingStockBundle\Form\Type
 */
class ProjectForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('label' => 'admin.housingStock.project.name', 'error_bubbling' => true))
            ->add(
                'description',
                'textarea',
                array('label' => 'admin.housingStock.project.description', 'error_bubbling' => true)
            )
            ->add(
                'additionalDetails',
                'textarea',
                array(
                    'label' => 'admin.housingStock.project.additionalDetails',
                    'error_bubbling' => true,
                    'attr' => array('class' => 'textarea textareaHTML')
                )
            )
            ->add('address', 'text', array('label' => 'admin.housingStock.project.address', 'error_bubbling' => true))
            ->add(
                'city',
                'autocomplete_city',
                [
                    'label' => 'admin.user.builder.city',
                    'class' => 'KeirusLocalizationBundle:City',
                    'update_route' => 'localization_cities_list',
                    'required' => true,
                    'attr' => ['class' => 'form-control'],
                    'error_bubbling' => true
                ]
            )
            ->add(
                'projectStatus',
                'entity',
                array(
                    'class' => 'KeirusHousingStockBundle:ProjectStatus',
                    'property' => 'name',
                    'multiple' => false,
                    'label' => 'admin.housingStock.project.projectStatus',
                    'error_bubbling' => true
                )
            )
            ->add(
                'possessionFrom',
                'date',
                array('label' => 'admin.housingStock.project.possessionFrom', 'error_bubbling' => true)
            )
            ->add(
                'projectFiles',
                'collection',
                [
                    'label' => 'admin.housingStock.project.projectFiles',
                    'error_bubbling' => true,
                    'required' => false,
                    'attr' => ['class' => 'col-sm-8'],
                    'type' => new ProjectFileForm(),
                    'allow_add' => true,
                    'by_reference' => false,
                    'allow_delete' => true
                ]
            )
            ->add('submit', 'submit', array('attr' => ['class' => 'btn btn-lg btn-block bg-olive margin']));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'translation_domain' => 'admin',
                'data_class' => 'Keirus\HousingStockBundle\Entity\Project',
                'cascade_validation' => true
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'housingstock_project_form';
    }
}