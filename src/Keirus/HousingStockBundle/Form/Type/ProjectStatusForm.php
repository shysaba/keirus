<?php

namespace Keirus\HousingStockBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class ProjectStatusForm
 * @package Keirus\HousingStockBundle\Form\Type
 */
class ProjectStatusForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('label' => 'admin.housingStock.projectStatus.name', 'error_bubbling' => true))
            ->add('ranking', 'number', array('label' => 'admin.housingStock.projectStatus.ranking', 'error_bubbling' => true))
            ->add('submit', 'submit');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'translation_domain' => 'admin',
                'data_class' => 'Keirus\HousingStockBundle\Entity\ProjectStatus'
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'housingstock_projectstatus_form';
    }
}