<?php

namespace Keirus\HousingStockBundle\Form\Handler;


use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Keirus\CoreBundle\Form\Handler\BaseHandler;
use Keirus\HousingStockBundle\Manager\PropertyTypeManager;

/**
 * Class PropertyTypeHandler
 * @package Keirus\HousingStockBundle\Form\Handler
 */
class PropertyTypeHandler extends BaseHandler
{


    /**
     * @var PropertyTypeManager
     */
    protected $propertyTypeManager;


    /**
     * @param Form $form
     * @param Request $request
     * @param SecurityContext $security
     * @param PropertyTypeManager $propertyTypeManager
     */
    public function __construct(
        Form $form,
        Request $request,
        SecurityContext $security,
        PropertyTypeManager $propertyTypeManager
    ) {
        parent::__construct($form, $request, $security);
        $this->propertyTypeManager = $propertyTypeManager;
    }


    /**
     *
     */
    protected function onSuccess()
    {
        $entity = $this->form->getData();

        $this->propertyTypeManager->save($entity);
    }

}