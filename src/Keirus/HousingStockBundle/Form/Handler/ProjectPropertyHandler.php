<?php

namespace Keirus\HousingStockBundle\Form\Handler;


use Keirus\HousingStockBundle\Manager\ProjectManager;
use Keirus\HousingStockBundle\Manager\ProjectPropertyFileManager;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Keirus\CoreBundle\Form\Handler\BaseHandler;
use Keirus\HousingStockBundle\Manager\ProjectPropertyManager;
use Symfony\Component\Validator\Constraints\Null;

/**
 * Class ProjectPropertyHandler
 * @package Keirus\HousingStockBundle\Form\Handler
 */
class ProjectPropertyHandler extends BaseHandler
{


    /**
     * @var ProjectPropertyManager
     */
    protected $projectPropertyManager;

    /**
     * @var ProjectPropertyFileManager
     */
    protected $projectPropertyFileManager;

    /**
     * @var ProjectPropertyManager
     */
    protected $projectManager;

    /**
     * @param Form $form
     * @param Request $request
     * @param SecurityContext $security
     * @param ProjectPropertyManager $projectPropertyManager
     */
    public function __construct(
        Form $form,
        Request $request,
        SecurityContext $security,
        ProjectPropertyManager $projectPropertyManager,
        ProjectPropertyFileManager $projectPropertyFileManager,
        ProjectManager $projectManager
    ) {
        parent::__construct($form, $request, $security);
        $this->projectPropertyManager = $projectPropertyManager;
        $this->projectPropertyFileManager = $projectPropertyFileManager;
        $this->projectManager = $projectManager;
    }


    /**
     *
     */
    protected function onSuccess()
    {
        $entity = $this->form->getData();
        if ($entity->getId()) {
            $project = $entity->getProject();
        } else {
            $idProject = $this->request->get('project');
            $project = $this->projectManager->getProject($idProject);
        }
        $entity->setProject($project);

        $attachedFile = $entity->getAttachedFile();
        if ($attachedFile->file != null) {
            if ($attachedFile->getId()) {
              $attachedFile->upload();
            }
            $this->projectPropertyManager->persist($attachedFile);
        }


        $this->projectPropertyManager->save($entity);

    }

}