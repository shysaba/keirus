<?php

namespace Keirus\HousingStockBundle\Form\Handler;


use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Keirus\CoreBundle\Form\Handler\BaseHandler;
use Keirus\HousingStockBundle\Manager\UnitTypeManager;

/**
 * Class UnitTypeHandler
 * @package Keirus\HousingStockBundle\Form\Handler
 */
class UnitTypeHandler extends BaseHandler
{


    /**
     * @var UnitTypeManager
     */
    protected $unitTypeManager;


    /**
     * @param Form $form
     * @param Request $request
     * @param SecurityContext $security
     * @param UnitTypeManager $unitTypeManager
     */
    public function __construct(
        Form $form,
        Request $request,
        SecurityContext $security,
        UnitTypeManager $unitTypeManager
    ) {
        parent::__construct($form, $request, $security);
        $this->unitTypeManager = $unitTypeManager;
    }


    /**
     *
     */
    protected function onSuccess()
    {
        $entity = $this->form->getData();

        $this->unitTypeManager->save($entity);
    }

}