<?php

namespace Keirus\HousingStockBundle\Form\Handler;


use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Keirus\CoreBundle\Form\Handler\BaseHandler;
use Keirus\HousingStockBundle\Manager\ProjectFileManager;

/**
 * Class ProjectFileHandler
 * @package Keirus\HousingStockBundle\Form\Handler
 */
class ProjectFileHandler extends BaseHandler
{


    /**
     * @var ProjectFileManager
     */
    protected $projectFileManager;


    /**
     * @param Form $form
     * @param Request $request
     * @param SecurityContext $security
     * @param ProjectFileManager $projectFileManager
     */
    public function __construct(
        Form $form,
        Request $request,
        SecurityContext $security,
        ProjectFileManager $projectFileManager
    ) {
        parent::__construct($form, $request, $security);
        $this->propertyTypeManager = $projectFileManager;
    }


    /**
     *
     */
    protected function onSuccess()
    {
        $entity = $this->form->getData();

        $this->propertyTypeManager->save($entity);
    }

}