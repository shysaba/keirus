<?php

namespace Keirus\HousingStockBundle\Form\Handler;


use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Keirus\CoreBundle\Form\Handler\BaseHandler;
use Keirus\HousingStockBundle\Manager\ProjectStatusManager;

/**
 * Class ProjectStatusHandler
 * @package Keirus\HousingStockBundle\Form\Handler
 */
class ProjectStatusHandler extends BaseHandler
{


    /**
     * @var ProjectStatusManager
     */
    protected $projectStatusManager;


    /**
     * @param Form $form
     * @param Request $request
     * @param SecurityContext $security
     * @param ProjectStatusManager $projectStatusManager
     */
    public function __construct(
        Form $form,
        Request $request,
        SecurityContext $security,
        ProjectStatusManager $projectStatusManager
    ) {
        parent::__construct($form, $request, $security);
        $this->projectStatusManager = $projectStatusManager;
    }


    /**
     *
     */
    protected function onSuccess()
    {
        $entity = $this->form->getData();

        $this->projectStatusManager->save($entity);
    }

}