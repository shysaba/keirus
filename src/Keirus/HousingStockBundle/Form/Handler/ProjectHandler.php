<?php

namespace Keirus\HousingStockBundle\Form\Handler;


use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Keirus\CoreBundle\Form\Handler\BaseHandler;
use Keirus\HousingStockBundle\Manager\ProjectManager;

/**
 * Class ProjectHandler
 * @package Keirus\HousingStockBundle\Form\Handler
 */
class ProjectHandler extends BaseHandler
{


    /**
     * @var ProjectManager
     */
    protected $projectManager;


    /**
     * @param Form $form
     * @param Request $request
     * @param SecurityContext $security
     * @param ProjectManager $projectManager
     */
    public function __construct(
        Form $form,
        Request $request,
        SecurityContext $security,
        ProjectManager $projectManager
    ) {
        parent::__construct($form, $request, $security);
        $this->projectManager = $projectManager;
    }


    /**
     *
     */
    protected function onSuccess()
    {
        $entity = $this->form->getData();
        $user = $this->security->getToken()->getUser();
        $projectFiles = $entity->getProjectFiles();

        foreach ($projectFiles as $file) {
            $this->projectManager->persist($file);
            $entity->addProjectFile($file);
        }

        $entity->setBuilder($user);


        $this->projectManager->save($entity);
    }

}