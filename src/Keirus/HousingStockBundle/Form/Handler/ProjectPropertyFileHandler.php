<?php

namespace Keirus\HousingStockBundle\Form\Handler;


use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Keirus\CoreBundle\Form\Handler\BaseHandler;
use Keirus\HousingStockBundle\Manager\ProjectPropertyFileManager;

/**
 * Class ProjectPropertyFileHandler
 * @package Keirus\HousingStockBundle\Form\Handler
 */
class ProjectPropertyFileHandler extends BaseHandler
{


    /**
     * @var ProjectPropertyFileManager
     */
    protected $projectPropertyFileManager;


    /**
     * @param Form $form
     * @param Request $request
     * @param SecurityContext $security
     * @param ProjectPropertyFileManager $projectPropertyFileManager
     */
    public function __construct(
        Form $form,
        Request $request,
        SecurityContext $security,
        ProjectPropertyFileManager $projectPropertyFileManager
    ) {
        parent::__construct($form, $request, $security);
        $this->projectPropertyFileManager = $projectPropertyFileManager;
    }


    /**
     *
     */
    protected function onSuccess()
    {
        $entity = $this->form->getData();

        $this->projectPropertyFileManager->save($entity);
    }

}