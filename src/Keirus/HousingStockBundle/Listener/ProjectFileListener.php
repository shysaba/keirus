<?php

namespace Keirus\HousingStockBundle\Listener;

use Keirus\CoreBundle\Manager\LoggerManager;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;


class ProjectFileListener
{

    /**
     * @var LoggerManager
     */
    private $loggerManager;

    /**
     * @param LoggerManager $loggerManager
     */
    public function __construct(LoggerManager $loggerManager)
    {
        $this->loggerManager = $loggerManager;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function process(GetResponseEvent $event)
    {
        if ($event->getRequest()->attributes->get('_route') == "housingstock_projectFile_delete") {
            $this->loggerManager->write();
        }
    }
}