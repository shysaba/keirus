<?php

namespace Keirus\HousingStockBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectProperty
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Keirus\HousingStockBundle\Repository\ProjectPropertyRepository")
 */
class ProjectProperty
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255)
     */
    private $reference;

    /**
     * @ORM\ManyToOne(targetEntity="Keirus\HousingStockBundle\Entity\PropertyType")
     * @ORM\JoinColumn(nullable=false)
     */
    private $propertyType;


    /**
     * @var float
     *
     * @ORM\Column(name="sqftArea", type="float")
     */
    private $sqftArea;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    /**
     * @var boolean
     *
     * @ORM\Column(name="priceAvailable", type="boolean")
     */
    private $priceAvailable;

    /**
     * @var boolean
     *
     * @ORM\Column(name="available", type="boolean")
     */
    private $available;

    /**
     *
     * @ORM\OneToOne(targetEntity="Keirus\HousingStockBundle\Entity\ProjectPropertyFile", inversedBy="projectProperty", cascade={"persist"}, orphanRemoval=true)
     * @ORM\JoinColumn(name="attachedFile_id", referencedColumnName="id", nullable=true)
     */
    private $attachedFile;

    /**
     * @ORM\ManyToOne(targetEntity="Keirus\HousingStockBundle\Entity\Project")
     * @ORM\JoinColumn(nullable=false)
     */
    private $project;


    /**
     * @ORM\ManyToOne(targetEntity="Keirus\HousingStockBundle\Entity\UnitType")
     * @ORM\JoinColumn(nullable=false)
     */
    private $unitType;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }


    /**
     * Set sqftArea
     *
     * @param float $sqftArea
     *
     * @return ProjectProperty
     */
    public function setSqftArea($sqftArea)
    {
        $this->sqftArea = $sqftArea;

        return $this;
    }

    /**
     * Get sqftArea
     *
     * @return float
     */
    public function getSqftArea()
    {
        return $this->sqftArea;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return ProjectProperty
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set priceAvailable
     *
     * @param boolean $priceAvailable
     *
     * @return ProjectProperty
     */
    public function setPriceAvailable($priceAvailable)
    {
        $this->priceAvailable = $priceAvailable;

        return $this;
    }

    /**
     * Get priceAvailable
     *
     * @return boolean
     */
    public function getPriceAvailable()
    {
        return $this->priceAvailable;
    }

    /**
     * Set available
     *
     * @param boolean $available
     *
     * @return ProjectProperty
     */
    public function setAvailable($available)
    {
        $this->available = $available;

        return $this;
    }

    /**
     * Get available
     *
     * @return boolean
     */
    public function getAvailable()
    {
        return $this->available;
    }


    /**
     * Set propertyType
     *
     * @param \Keirus\HousingStockBundle\Entity\PropertyType $propertyType
     *
     * @return ProjectProperty
     */
    public function setPropertyType(\Keirus\HousingStockBundle\Entity\PropertyType $propertyType)
    {
        $this->propertyType = $propertyType;

        return $this;
    }

    /**
     * Get propertyType
     *
     * @return \Keirus\HousingStockBundle\Entity\PropertyType
     */
    public function getPropertyType()
    {
        return $this->propertyType;
    }

    /**
     * Set project
     *
     * @param \Keirus\HousingStockBundle\Entity\Project $project
     *
     * @return ProjectProperty
     */
    public function setProject(\Keirus\HousingStockBundle\Entity\Project $project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \Keirus\HousingStockBundle\Entity\Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set unitType
     *
     * @param \Keirus\HousingStockBundle\Entity\UnitType $unitType
     *
     * @return ProjectProperty
     */
    public function setUnitType(\Keirus\HousingStockBundle\Entity\UnitType $unitType)
    {
        $this->unitType = $unitType;

        return $this;
    }

    /**
     * Get unitType
     *
     * @return \Keirus\HousingStockBundle\Entity\UnitType
     */
    public function getUnitType()
    {
        return $this->unitType;
    }

    /**
     * Set attachedFile
     *
     * @param \Keirus\HousingStockBundle\Entity\ProjectPropertyFile $attachedFile
     *
     * @return ProjectProperty
     */
    public function setAttachedFile(\Keirus\HousingStockBundle\Entity\ProjectPropertyFile $attachedFile = null)
    {
        if ($attachedFile != null) {
            $this->attachedFile = $attachedFile;
            $attachedFile->setProjectProperty($this);
        } else {
            $this->attachedFile = null;
        }

        return $this;

    }

    /**
     * Get attachedFile
     *
     * @return \Keirus\HousingStockBundle\Entity\ProjectPropertyFile
     */
    public function getAttachedFile()
    {
        return $this->attachedFile;
    }
}
