<?php

namespace Keirus\HousingStockBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UnitType
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Keirus\HousingStockBundle\Repository\UnitTypeRepository")
 */
class UnitType
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, unique=true)
     */
    private $type;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return UnitType
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    function __toString()
    {
        return $this->type;
    }
}
