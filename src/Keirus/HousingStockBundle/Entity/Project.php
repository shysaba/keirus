<?php

namespace Keirus\HousingStockBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Project
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Keirus\HousingStockBundle\Repository\ProjectRepository")
 */
class Project
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="additionalDetails", type="text")
     */
    private $additionalDetails;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text")
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="Keirus\LocalizationBundle\Entity\City")
     * @ORM\JoinColumn(nullable=false)
     */
    private $city;

    /**
     * @ORM\ManyToOne(targetEntity="Keirus\HousingStockBundle\Entity\ProjectStatus")
     * @ORM\JoinColumn(nullable=false)
     */
    private $projectStatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="possessionFrom", type="date")
     */
    private $possessionFrom;


    /**
     * @ORM\ManyToOne(targetEntity="Keirus\UserBundle\Entity\Builder")
     * @ORM\JoinColumn(nullable=false)
     */
    private $builder;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creationDate", type="datetime")
     */
    private $creationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedDate", type="datetime")
     */
    private $updatedDate;

    /**
     * @ORM\OneToMany(targetEntity="Keirus\HousingStockBundle\Entity\ProjectFile", mappedBy="project", cascade={"all"}, orphanRemoval=true)
     * @ORM\JoinColumn(nullable=true)
     */
    private $projectFiles;

    /**
     * @var boolean
     *
     * @ORM\Column(name="online", type="boolean")
     */
    private $online;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->projectFiles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->creationDate = new \DateTime();
        $this->updatedDate = new \DateTime();
        $this->online = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Project
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set additionalDetails
     *
     * @param string $additionalDetails
     *
     * @return Project
     */
    public function setAdditionalDetails($additionalDetails)
    {
        $this->additionalDetails = $additionalDetails;

        return $this;
    }

    /**
     * Get additionalDetails
     *
     * @return string
     */
    public function getAdditionalDetails()
    {
        return $this->additionalDetails;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Project
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set possessionFrom
     *
     * @param \DateTime $possessionFrom
     *
     * @return Project
     */
    public function setPossessionFrom($possessionFrom)
    {
        $this->possessionFrom = $possessionFrom;

        return $this;
    }

    /**
     * Get possessionFrom
     *
     * @return \DateTime
     */
    public function getPossessionFrom()
    {
        return $this->possessionFrom;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     *
     * @return Project
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set updatedDate
     *
     * @param \DateTime $updatedDate
     *
     * @return Project
     */
    public function setUpdatedDate($updatedDate)
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * Get updatedDate
     *
     * @return \DateTime
     */
    public function getUpdatedDate()
    {
        return $this->updatedDate;
    }

    /**
     * Set city
     *
     * @param \Keirus\LocalizationBundle\Entity\City $city
     *
     * @return Project
     */
    public function setCity(\Keirus\LocalizationBundle\Entity\City $city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \Keirus\LocalizationBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set projectStatus
     *
     * @param \Keirus\HousingStockBundle\Entity\ProjectStatus $projectStatus
     *
     * @return Project
     */
    public function setProjectStatus(\Keirus\HousingStockBundle\Entity\ProjectStatus $projectStatus)
    {
        $this->projectStatus = $projectStatus;

        return $this;
    }

    /**
     * Get projectStatus
     *
     * @return \Keirus\HousingStockBundle\Entity\ProjectStatus
     */
    public function getProjectStatus()
    {
        return $this->projectStatus;
    }

    /**
     * Set builder
     *
     * @param \Keirus\UserBundle\Entity\Builder $builder
     *
     * @return Project
     */
    public function setBuilder(\Keirus\UserBundle\Entity\Builder $builder)
    {
        $this->builder = $builder;

        return $this;
    }

    /**
     * Get builder
     *
     * @return \Keirus\UserBundle\Entity\Builder
     */
    public function getBuilder()
    {
        return $this->builder;
    }

    /**
     * Add projectFile
     *
     * @param \Keirus\HousingStockBundle\Entity\ProjectFile $projectFile
     *
     * @return Project
     */
    public function addProjectFile(\Keirus\HousingStockBundle\Entity\ProjectFile $projectFile)
    {
        $this->projectFiles[] = $projectFile;
        $projectFile->setProject($this);

        return $this;
    }

    /**
     * Remove projectFile
     *
     * @param \Keirus\HousingStockBundle\Entity\ProjectFile $projectFile
     */
    public function removeProjectFile(\Keirus\HousingStockBundle\Entity\ProjectFile $projectFile)
    {
        $this->projectFiles->removeElement($projectFile);
    }

    /**
     * Get projectFiles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjectFiles()
    {
        return $this->projectFiles;
    }

    /**
     * @return boolean
     */
    public function isOnline()
    {
        return $this->online;
    }

    /**
     * @param boolean $online
     */
    public function setOnline($online)
    {
        $this->online = $online;
    }


}
