<?php

namespace Keirus\HousingStockBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Keirus\CoreBundle\Entity\FileManager;

/**
 * ProjectProperty
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Keirus\HousingStockBundle\Repository\ProjectPropertyFileRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ProjectPropertyFile extends FileManager
{
    /**
     * @ORM\OneToOne(targetEntity="Keirus\HousingStockBundle\Entity\ProjectProperty", mappedBy="attachedFile", cascade={"persist"})
     * */
    private $projectProperty;

    /**
     * @return string
     */
    protected function getUploadDir()
    {
        return 'uploads/projectPropertyFiles';
    }


    /**
     * Set projectProperty
     *
     * @param \Keirus\HousingStockBundle\Entity\ProjectProperty $projectProperty
     *
     * @return ProjectPropertyFile
     */
    public function setProjectProperty(\Keirus\HousingStockBundle\Entity\ProjectProperty $projectProperty = null)
    {
        $this->projectProperty = $projectProperty;
        return $this;
    }

    /**
     * Get projectProperty
     *
     * @return \Keirus\HousingStockBundle\Entity\ProjectProperty
     */
    public function getProjectProperty()
    {
        return $this->projectProperty;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }


}
