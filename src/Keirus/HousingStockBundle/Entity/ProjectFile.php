<?php

namespace Keirus\HousingStockBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Keirus\CoreBundle\Entity\FileManager;

/**
 * ProjectFile
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ProjectFile extends FileManager
{

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;


    /**
     * @ORM\ManyToOne(targetEntity="Keirus\HousingStockBundle\Entity\Project", inversedBy="projectFiles", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $project;


    /**
     * Set title
     *
     * @param string $title
     *
     * @return ProjectFile
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }


    /**
     * Set project
     *
     * @param \Keirus\HousingStockBundle\Entity\Project $project
     *
     * @return ProjectFile
     */
    public function setProject(\Keirus\HousingStockBundle\Entity\Project $project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \Keirus\HousingStockBundle\Entity\Project
     */
    public function getProject()
    {
        return $this->project;
    }


    /**
     * @return string
     */
    protected function getUploadDir()
    {
        return 'uploads/projectFiles';
    }
}
