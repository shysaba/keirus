<?php
namespace Keirus\HousingStockBundle\Manager;

use Keirus\CoreBundle\Manager\BaseManager;
use Keirus\HousingStockBundle\Entity\ProjectFile;
use Doctrine\ORM\EntityManager;


/**
 * Class ProjectFileManager
 * @package Keirus\HousingStockBundle\Manager
 */
class ProjectFileManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;


    /**
     * @var \Keirus\HousingStockBundle\Repository\ProjectFileRepository
     */
    protected $repository;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('KeirusHousingStockBundle:ProjectFile');
    }

    /**
     * Persist and flush a unit type
     * @param ProjectFile $projectFile
     */
    public function save(ProjectFile $projectFile)
    {
        $this->persistAndFlush($projectFile);
    }

    /**
     * Delete a unit type
     * @param ProjectFile $projectFile
     * @param boolean $flush
     */
    public function delete(ProjectFile $projectFile, $flush = true)
    {
        if ($flush) {
            $this->removeAndFlush($projectFile);
        } else {
            $this->remove($projectFile);
        }
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllProjectFilesPaginated($page, $rpp, $key = null)
    {
        return $this->repository->getAllProjectFilesPaginated($page, $rpp, $key);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getProjectFile($id)
    {
        return $this->repository->find($id);
    }
}