<?php
namespace Keirus\HousingStockBundle\Manager;

use Keirus\CoreBundle\Manager\BaseManager;
use Keirus\HousingStockBundle\Entity\UnitType;
use Doctrine\ORM\EntityManager;


/**
 * Class UnitTypeManager
 * @package Keirus\HousingStockBundle\Manager
 */
class UnitTypeManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;


    /**
     * @var \Keirus\HousingStockBundle\Repository\UnitTypeRepository
     */
    protected $repository;


    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('KeirusHousingStockBundle:UnitType');
    }

    /**
     * Persist and flush a unit type
     * @param UnitType $unitType
     */
    public function save(UnitType $unitType)
    {
        $this->persistAndFlush($unitType);
    }

    /**
     * Delete a unit type
     * @param UnitType $unitType
     * @param boolean $flush
     */
    public function delete(UnitType $unitType, $flush = true)
    {
        if ($flush) {
            $this->removeAndFlush($unitType);
        } else {
            $this->remove($unitType);
        }
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllUnitTypesPaginated($page, $rpp, $key = null)
    {
        return $this->repository->getAllUnitTypesPaginated($page, $rpp, $key);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getUnitType($id)
    {
        return $this->repository->find($id);
    }
}