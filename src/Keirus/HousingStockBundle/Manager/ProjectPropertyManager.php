<?php
namespace Keirus\HousingStockBundle\Manager;

use Keirus\CoreBundle\Manager\BaseManager;
use Keirus\HousingStockBundle\Entity\ProjectProperty;
use Doctrine\ORM\EntityManager;


/**
 * Class ProjectPropertyManager
 * @package Keirus\HousingStockBundle\Manager
 */
class ProjectPropertyManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;


    /**
     * @var \Keirus\HousingStockBundle\Repository\ProjectPropertyRepository
     */
    protected $repository;


    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('KeirusHousingStockBundle:ProjectProperty');
    }

    /**
     * Persist and flush a unit type
     * @param ProjectProperty $projectProperty
     */
    public function save(ProjectProperty $projectProperty)
    {
        $this->persistAndFlush($projectProperty);
    }

    /**
     * Delete a unit type
     * @param ProjectProperty $projectProperty
     * @param boolean $flush
     */
    public function delete(ProjectProperty $projectProperty, $flush = true)
    {
        if ($flush) {
            $this->removeAndFlush($projectProperty);
        } else {
            $this->remove($projectProperty);
        }
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllProjectPropertiesPaginated($id, $page, $rpp, $key = null)
    {
        return $this->repository->getAllProjectPropertiesPaginated($id, $page, $rpp, $key);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getProjectProperty($id)
    {
        return $this->repository->getProjectProperty($id);
    }

    /**
     * @param $idProjectProperty
     * @return mixed
     * @internal param $id
     */
    public function getProjectOfProperty($idProjectProperty)
    {
        return $this->repository->getProjectOfProperty($idProjectProperty);
    }
}