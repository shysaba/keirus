<?php
namespace Keirus\HousingStockBundle\Manager;

use Keirus\CoreBundle\Manager\BaseManager;
use Keirus\HousingStockBundle\Entity\ProjectStatus;
use Doctrine\ORM\EntityManager;


/**
 * Class ProjectStatusManager
 * @package Keirus\HousingStockBundle\Manager
 */
class ProjectStatusManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;


    /**
     * @var \Keirus\HousingStockBundle\Repository\ProjectStatusRepository
     */
    protected $repository;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('KeirusHousingStockBundle:ProjectStatus');
    }

    /**
     * Persist and flush a unit type
     * @param ProjectStatus $projectStatus
     */
    public function save(ProjectStatus $projectStatus)
    {
        $this->persistAndFlush($projectStatus);
    }

    /**
     * Delete a unit type
     * @param ProjectStatus $projectStatus
     * @param boolean $flush
     */
    public function delete(ProjectStatus $projectStatus, $flush = true)
    {
        if ($flush) {
            $this->removeAndFlush($projectStatus);
        } else {
            $this->remove($projectStatus);
        }
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllProjectsStatusPaginated($page, $rpp, $key = null)
    {
        return $this->repository->getAllProjectsStatusPaginated($page, $rpp, $key);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getProjectStatus($id)
    {
        return $this->repository->find($id);
    }
}