<?php
namespace Keirus\HousingStockBundle\Manager;

use Keirus\CoreBundle\Manager\BaseManager;
use Keirus\HousingStockBundle\Entity\Project;
use Doctrine\ORM\EntityManager;


/**
 * Class ProjectManager
 * @package Keirus\HousingStockBundle\Manager
 */
class ProjectManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;


    /**
     * @var \Keirus\HousingStockBundle\Repository\ProjectRepository
     */
    protected $repository;


    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('KeirusHousingStockBundle:Project');
    }

    /**
     * Persist and flush a unit type
     * @param Project $project
     */
    public function save(Project $project)
    {
        $this->persistAndFlush($project);
    }

    /**
     * Delete a unit type
     * @param Project $project
     * @param boolean $flush
     */
    public function delete(Project $project, $flush = true)
    {
        if ($flush) {
            $this->removeAndFlush($project);
        } else {
            $this->remove($project);
        }
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllProjectsPaginated($page, $rpp, $key = null)
    {
        return $this->repository->getAllProjectsPaginated($page, $rpp, $key);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getProject($id)
    {
        return $this->repository->getProject($id);
    }
}