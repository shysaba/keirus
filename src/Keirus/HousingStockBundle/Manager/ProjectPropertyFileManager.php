<?php
namespace Keirus\HousingStockBundle\Manager;

use Keirus\CoreBundle\Manager\BaseManager;
use Keirus\HousingStockBundle\Entity\ProjectPropertyFile;
use Doctrine\ORM\EntityManager;


/**
 * Class ProjectPropertyFileManager
 * @package Keirus\HousingStockBundle\Manager
 */
class ProjectPropertyFileManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;


    /**
     * @var \Keirus\HousingStockBundle\Repository\ProjectPropertyFileRepository
     */
    protected $repository;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('KeirusHousingStockBundle:ProjectPropertyFile');
    }

    /**
     * Persist and flush a unit type
     * @param ProjectPropertyFile $projectPropertyFile
     */
    public function save(ProjectPropertyFile $projectPropertyFile)
    {
        $this->persistAndFlush($projectPropertyFile);
    }

    /**
     * Delete a unit type
     * @param ProjectPropertyFile $projectPropertyFile
     * @param boolean $flush
     */
    public function delete(ProjectPropertyFile $projectPropertyFile, $flush = true)
    {
        if ($flush) {
            $this->removeAndFlush($projectPropertyFile);
        } else {
            $this->remove($projectPropertyFile);
        }
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllProjectPropertyFilesPaginated($page, $rpp, $key = null)
    {
        return $this->repository->getAllProjectPropertyFilesPaginated($page, $rpp, $key);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getProjectPropertyFile($id)
    {
        return $this->repository->find($id);
    }
}