<?php
namespace Keirus\HousingStockBundle\Manager;

use Keirus\CoreBundle\Manager\BaseManager;
use Keirus\HousingStockBundle\Entity\PropertyType;
use Doctrine\ORM\EntityManager;


/**
 * Class PropertyTypeManager
 * @package Keirus\HousingStockBundle\Manager
 */
class PropertyTypeManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;


    /**
     * @var \Keirus\HousingStockBundle\Repository\PropertyTypeRepository
     */
    protected $repository;


    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('KeirusHousingStockBundle:PropertyType');
    }

    /**
     * Persist and flush a unit type
     * @param PropertyType $propertyType
     */
    public function save(PropertyType $propertyType)
    {
        $this->persistAndFlush($propertyType);
    }

    /**
     * Delete a unit type
     * @param PropertyType $propertyType
     * @param boolean $flush
     */
    public function delete(PropertyType $propertyType, $flush = true)
    {
        if ($flush) {
            $this->removeAndFlush($propertyType);
        } else {
            $this->remove($propertyType);
        }
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllPropertyTypesPaginated($page, $rpp, $key = null)
    {
        return $this->repository->getAllPropertyTypesPaginated($page, $rpp, $key);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getPropertyType($id)
    {
        return $this->repository->find($id);
    }
}