<?php

namespace Keirus\HousingStockBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class ProjectRepository
 * @package Keirus\HousingStockBundle\Repository
 */
class ProjectRepository extends EntityRepository
{

    /**
     * Find all unit types
     * @param string $sort
     * @param string $order
     * @return array
     */
    public function getAllProjectsOrderBy($sort, $order)
    {
        return $this->createQueryBuilder('p')
            ->orderBy($sort, $order)
            ->getQuery();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllProjectsPaginated($page, $rpp, $key = null)
    {
        $count = $this->getCountProjects($key);

        $query = $this->createQueryBuilder('p')
            ->leftJoin('p.projectStatus', 's')
            ->addSelect('s')
            ->leftJoin('p.city', 'c')
            ->addSelect('c')
            ->setMaxResults($rpp)
            ->setFirstResult(($page - 1) * $rpp)
            ->orderBy('p.name', 'DESC');

        if ($key <> null) {
            $query->Where('p.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $result = $query->getQuery()
            ->getResult();


        return array($result, $count);
    }

    /**
     * @param $key
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getCountProjects($key)
    {
        $query = $this->createQueryBuilder('p');
        $query->select('COUNT(p)');

        if (!empty($key)) {
            $query->Where('p.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $menu = $query->getQuery()
            ->getSingleScalarResult();

        return $menu;
    }



    public function getProject($id)
    {
        $query = $this->createQueryBuilder('p')
            ->leftJoin('p.projectStatus', 's')
            ->addSelect('s')
            ->leftJoin('p.city', 'c')
            ->addSelect('c')
            ->leftJoin('p.builder', 'b')
            ->addSelect('b')
            ->leftJoin('p.projectFiles', 'f')
            ->addSelect('f')
            ->where('p.id = :id')
            ->setParameter('id', $id);

        $result = $query->getQuery()
            ->getOneOrNullResult();


        return $result;
    }
}