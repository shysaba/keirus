<?php

namespace Keirus\HousingStockBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class UnitTypeRepository
 * @package Keirus\HousingStockBundle\Repository
 */
class UnitTypeRepository extends EntityRepository
{

    /**
     * Find all unit types
     * @param string $sort
     * @param string $order
     * @return array
     */
    public function getAllUnitTypesOrderBy($sort, $order)
    {
        return $this->createQueryBuilder('u')
            ->orderBy($sort, $order)
            ->getQuery();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllUnitTypesPaginated($page, $rpp, $key = null)
    {
        $count = $this->getCountUnitTypes($key);

        $query = $this->createQueryBuilder('u')
            ->setMaxResults($rpp)
            ->setFirstResult(($page - 1) * $rpp)
            ->orderBy('u.type', 'DESC');

        if ($key <> null) {
            $query->Where('u.type LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $result = $query->getQuery()
            ->getResult();


        return array($result, $count);
    }

    /**
     * @param $key
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getCountUnitTypes($key)
    {
        $query = $this->createQueryBuilder('u');
        $query->select('COUNT(u)');

        if (!empty($key)) {
            $query->Where('u.type LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $menu = $query->getQuery()
            ->getSingleScalarResult();

        return $menu;
    }
}