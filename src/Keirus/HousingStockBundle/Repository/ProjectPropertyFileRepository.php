<?php

namespace Keirus\HousingStockBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class ProjectPropertyFileRepository
 * @package Keirus\HousingStockBundle\Repository
 */
class ProjectPropertyFileRepository extends EntityRepository
{

    /**
     * Find all unit types
     * @param string $sort
     * @param string $order
     * @return array
     */
    public function getAllProjectPropertyFileOrderBy($sort, $order)
    {
        return $this->createQueryBuilder('p')
            ->orderBy($sort, $order)
            ->getQuery();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllProjectsStatusPaginated($page, $rpp, $key = null)
    {
        $count = $this->getCountProjectPropertyFile($key);

        $query = $this->createQueryBuilder('p')
            ->setMaxResults($rpp)
            ->setFirstResult(($page - 1) * $rpp)
            ->orderBy('p.name', 'DESC');

        if ($key <> null) {
            $query->Where('p.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $result = $query->getQuery()
            ->getResult();


        return array($result, $count);
    }

    /**
     * @param $key
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getCountProjectPropertyFile($key)
    {
        $query = $this->createQueryBuilder('p');
        $query->select('COUNT(p)');

        if (!empty($key)) {
            $query->Where('p.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $menu = $query->getQuery()
            ->getSingleScalarResult();

        return $menu;
    }
}