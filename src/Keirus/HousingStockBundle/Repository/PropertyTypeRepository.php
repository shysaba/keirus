<?php

namespace Keirus\HousingStockBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class PropertyTypeRepository
 * @package Keirus\HousingStockBundle\Repository
 */
class PropertyTypeRepository extends EntityRepository
{

    /**
     * Find all unit types
     * @param string $sort
     * @param string $order
     * @return array
     */
    public function getAllPropertyTypesOrderBy($sort, $order)
    {
        return $this->createQueryBuilder('p')
            ->orderBy($sort, $order)
            ->getQuery();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllPropertyTypesPaginated($page, $rpp, $key = null)
    {
        $count = $this->getCountPropertyTypes($key);

        $query = $this->createQueryBuilder('p')
            ->setMaxResults($rpp)
            ->setFirstResult(($page - 1) * $rpp)
            ->orderBy('p.type', 'DESC');

        if ($key <> null) {
            $query->Where('p.type LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $result = $query->getQuery()
            ->getResult();


        return array($result, $count);
    }

    /**
     * @param $key
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getCountPropertyTypes($key)
    {
        $query = $this->createQueryBuilder('p');
        $query->select('COUNT(p)');

        if (!empty($key)) {
            $query->Where('p.type LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $menu = $query->getQuery()
            ->getSingleScalarResult();

        return $menu;
    }
}