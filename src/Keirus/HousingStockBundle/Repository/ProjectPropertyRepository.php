<?php

namespace Keirus\HousingStockBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class ProjectPropertyRepository
 * @package Keirus\HousingStockBundle\Repository
 */
class ProjectPropertyRepository extends EntityRepository
{

    /**
     * Find all unit types
     * @param string $sort
     * @param string $order
     * @return array
     */
    public function getAllProjectPropertiesOrderBy($sort, $order)
    {
        return $this->createQueryBuilder('p')
            ->orderBy($sort, $order)
            ->getQuery();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllProjectPropertiesPaginated($id, $page, $rpp, $key = null)
    {
        $count = $this->getCountProjectProperties($key);

        $query = $this->createQueryBuilder('p')
            ->leftJoin('p.project', 'r')
            ->addSelect('r')
            ->leftJoin('p.unitType', 'u')
            ->addSelect('u')
            ->leftJoin('p.propertyType', 't')
            ->addSelect('t')
            ->setMaxResults($rpp)
            ->setFirstResult(($page - 1) * $rpp)
            ->orderBy('p.reference', 'ASC');

        if ($key <> null) {
            $query->Where('p.name LIKE :key')
                ->setParameter('key', '%'.$key.'%')
                ->Andwhere('p.id = :id')
                ->setParameter('id', $id);
        } else {
            $query->Where('p.project = :id')
                ->setParameter('id', $id);
        }

        $result = $query->getQuery()
            ->getResult();


        return array($result, $count);
    }

    /**
     * @param $key
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getCountProjectProperties($key)
    {
        $query = $this->createQueryBuilder('p');
        $query->select('COUNT(p)');

        if (!empty($key)) {
            $query->Where('p.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $menu = $query->getQuery()
            ->getSingleScalarResult();

        return $menu;
    }


    public function getProjectProperty($id)
    {
        $query = $this->createQueryBuilder('p')
            ->leftJoin('p.unitType', 'u')
            ->addSelect('u')
            ->leftJoin('p.propertyType', 't')
            ->addSelect('t')
            ->leftJoin('p.attachedFile', 'a')
            ->addSelect('a')
            ->leftJoin('p.project', 'r')
            ->addSelect('r')
            ->where('p.id = :id')
            ->setParameter('id', $id);

        $result = $query->getQuery()
            ->getOneOrNullResult();


        return $result;
    }



    public function getProjectOfProperty($idProjectProperty) {
        $query = $this->createQueryBuilder('p')
            ->leftJoin('p.project', 'r')
            ->addSelect('r')
            ->where('p.id = :id')
            ->setParameter('id', $idProjectProperty);

        $result = $query->getQuery()
            ->getOneOrNullResult();


        return $result;
    }
}