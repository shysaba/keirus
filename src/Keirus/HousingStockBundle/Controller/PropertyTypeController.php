<?php
namespace Keirus\HousingStockBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Keirus\CoreBundle\Manager\BasePaginatorManager;
use Keirus\HousingStockBundle\Form\Type\PropertyTypeForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keirus\HousingStockBundle\Entity\PropertyType;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class PropertyTypeController
 * @package Keirus\HousingStockBundle\Controller
 */
class PropertyTypeController extends Controller
{

    /**
     * @return mixed
     */
    public function listAction($page, $key)
    {

        return $this->dataResult($page, $key);
    }

    /**
     * @param $page
     * @param $key
     * @return Response
     */
    private function dataResult($page, $key)
    {

        $manager = $this->get('housingstock_propertytype_manager');
        $rpp = $this->container->getParameter('elements_per_page');


        list($result, $totalCount) = $manager->getAllPropertyTypesPaginated($page, $rpp, $key);

        $paginator = new BasePaginatorManager($page, $totalCount, $rpp);

        if ($totalCount < 1) {
            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.alert.info',
                'admin.search.noResults'
            );
        }

        return $this->render(
            'KeirusHousingStockBundle:PropertyType:list.html.twig',
            array(
                'propertyTypes' => $result,
                'paginator' => $paginator->getPagesList(),
                'cur' => $page,
                'total' => $paginator->getTotalPages(),
                'key' => $key
            )
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $key = $request->request->get('key');
        $page = 1;

        return $this->dataResult($page, $key);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction()
    {

        $formHandler = $this->get('housingstock_propertytype_handler');

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'info',
                'admin.action.created',
                'admin.housingStock.propertyType.created'
            );

            return $this->redirect(
                $this->generateUrl('housingStock_propertyType_list')
            );
        }

        return $this->render(
            'KeirusHousingStockBundle:PropertyType:create.html.twig',
            array('form' => $formHandler->getForm()->createView())
        );
    }


    /**
     * @param $id
     * @return RedirectResponse|Response
     */
    public function updateAction($id)
    {

        $manager = $this->get('housingstock_propertytype_manager');
        $propertyType = $manager->getPropertyType($id);

        $formHandler = $this->get('housingstock_propertytype_handler');
        $form = $this->createForm(new PropertyTypeForm(), $propertyType);
        $formHandler->setForm($form);

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'success',
                'admin.action.updated',
                'admin.housingStock.propertyType.updated'
            );

            return $this->redirect(
                $this->generateUrl('housingStock_propertyType_list')
            );
        }

        return $this->render(
            'KeirusHousingStockBundle:PropertyType:update.html.twig',
            array(
                'form' => $formHandler->getForm()->createView(),
                'propertyType' => $propertyType
            )
        );
    }


    /**
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction($id)
    {
        $manager = $this->get('housingstock_propertytype_manager');
        $propertyType = $manager->getPropertyType($id);

        if ($propertyType instanceof PropertyType) {
            $manager->removeAndFlush($propertyType);

            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.action.deleted',
                'admin.housingStock.propertyType.deleted'
            );
        } else {
            throw new NotFoundHttpException("PropertyType not found");
        }

        return $this->redirect($this->generateUrl('housingStock_propertyType_list'));
    }


    /**
     * @param $id
     * @return Response
     * @throws EntityNotFoundException
     */
    public function showAction($id)
    {
        $manager = $this->get('housingstock_propertytype_manager');
        $propertyType = $manager->getPropertyType($id);


        return $this->render(
            'KeirusHousingStockBundle:PropertyType:show.html.twig',
            array('propertyType' => $propertyType)
        );
    }
}