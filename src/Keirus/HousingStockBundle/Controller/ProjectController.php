<?php
namespace Keirus\HousingStockBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Keirus\CoreBundle\Manager\BasePaginatorManager;
use Keirus\HousingStockBundle\Form\Type\ProjectForm;
use Keirus\UserBundle\Entity\Builder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keirus\HousingStockBundle\Entity\Project;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use JMS\SecurityExtraBundle\Annotation\Secure;


/**
 * Class ProjectController
 * @package Keirus\HousingStockBundle\Controller
 */
class ProjectController extends Controller
{

    /**
     * @return mixed
     */
    public function listAction($page, $key)
    {

        return $this->dataResult($page, $key);
    }

    /**
     * @param $page
     * @param $key
     * @return Response
     */
    private function dataResult($page, $key)
    {

        $manager = $this->get('housingstock_project_manager');
        $rpp = $this->container->getParameter('elements_per_page');


        list($result, $totalCount) = $manager->getAllProjectsPaginated($page, $rpp, $key);

        $paginator = new BasePaginatorManager($page, $totalCount, $rpp);

        if ($totalCount < 1) {
            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.alert.info',
                'admin.search.noResults'
            );
        }

        return $this->render(
            'KeirusHousingStockBundle:Project:list.html.twig',
            array(
                'projects' => $result,
                'paginator' => $paginator->getPagesList(),
                'cur' => $page,
                'total' => $paginator->getTotalPages(),
                'key' => $key
            )
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $key = $request->request->get('key');
        $page = 1;

        return $this->dataResult($page, $key);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @Secure(roles="ROLE_BUILDER")
     */
    public function createAction()
    {

        $formHandler = $this->get('housingstock_project_handler');


        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'info',
                'admin.action.created',
                'admin.housingStock.project.created'
            );

            return $this->redirect(
                $this->generateUrl('housingStock_project_list')
            );
        }

        return $this->render(
            'KeirusHousingStockBundle:Project:create.html.twig',
            array('form' => $formHandler->getForm()->createView())
        );

    }


    /**
     * @param $id
     * @return RedirectResponse|Response
     */
    public function updateAction($id)
    {

        $manager = $this->get('housingstock_project_manager');
        $project = $manager->getProject($id);

        $formHandler = $this->get('housingstock_project_handler');
        $form = $this->createForm(new ProjectForm(), $project);
        $formHandler->setForm($form);

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'success',
                'admin.action.updated',
                'admin.housingStock.project.updated'
            );

            /* return $this->redirect(
                 $this->generateUrl('housingStock_project_list')
             );*/

            return $this->render(
                'KeirusHousingStockBundle:Project:update.html.twig',
                array(
                    'form' => $formHandler->getForm()->createView(),
                    'project' => $project
                )
            );
        }

        return $this->render(
            'KeirusHousingStockBundle:Project:update.html.twig',
            array(
                'form' => $formHandler->getForm()->createView(),
                'project' => $project
            )
        );
    }


    /**
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction($id)
    {
        $manager = $this->get('housingstock_project_manager');
        $project = $manager->getProject($id);

        if ($project instanceof Project) {
            $manager->removeAndFlush($project);

            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.action.deleted',
                'admin.housingStock.project.deleted'
            );
        } else {
            throw new NotFoundHttpException("Project not found");
        }

        return $this->redirect($this->generateUrl('housingStock_project_list'));
    }


    /**
     * @param $id
     * @return Response
     * @throws EntityNotFoundException
     */
    public function showAction($id)
    {
        $manager = $this->get('housingstock_project_manager');
        $project = $manager->getProject($id);

        if ($project instanceof Project) {
            return $this->render('KeirusHousingStockBundle:Project:show.html.twig', array('project' => $project));
        } else {
            throw new NotFoundHttpException("Project not found");
        }
    }
}