<?php
namespace Keirus\HousingStockBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Keirus\CoreBundle\Manager\BasePaginatorManager;
use Keirus\HousingStockBundle\Form\Type\ProjectStatusForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keirus\HousingStockBundle\Entity\ProjectStatus;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class ProjectStatusController
 * @package Keirus\HousingStockBundle\Controller
 */
class ProjectStatusController extends Controller
{

    /**
     * @return mixed
     */
    public function listAction($page, $key)
    {

        return $this->dataResult($page, $key);
    }

    /**
     * @param $page
     * @param $key
     * @return Response
     */
    private function dataResult($page, $key)
    {

        $manager = $this->get('housingstock_projectstatus_manager');
        $rpp = $this->container->getParameter('elements_per_page');


        list($result, $totalCount) = $manager->getAllProjectsStatusPaginated($page, $rpp, $key);

        $paginator = new BasePaginatorManager($page, $totalCount, $rpp);

        if ($totalCount < 1) {
            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.alert.info',
                'admin.search.noResults'
            );
        }

        return $this->render(
            'KeirusHousingStockBundle:ProjectStatus:list.html.twig',
            array(
                'projectsStatus' => $result,
                'paginator' => $paginator->getPagesList(),
                'cur' => $page,
                'total' => $paginator->getTotalPages(),
                'key' => $key
            )
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $key = $request->request->get('key');
        $page = 1;

        return $this->dataResult($page, $key);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction()
    {

        $formHandler = $this->get('housingstock_projectstatus_handler');

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'info',
                'admin.action.created',
                'admin.housingStock.projectStatus.created'
            );

            return $this->redirect(
                $this->generateUrl('housingStock_projectStatus_list')
            );
        }

        return $this->render(
            'KeirusHousingStockBundle:ProjectStatus:create.html.twig',
            array('form' => $formHandler->getForm()->createView())
        );
    }


    /**
     * @param $id
     * @return RedirectResponse|Response
     */
    public function updateAction($id)
    {

        $manager = $this->get('housingstock_projectstatus_manager');
        $projectStatus = $manager->getProjectStatus($id);

        $formHandler = $this->get('housingstock_projectstatus_handler');
        $form = $this->createForm(new ProjectStatusForm(), $projectStatus);
        $formHandler->setForm($form);

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'success',
                'admin.action.updated',
                'admin.housingStock.projectStatus.updated'
            );

            return $this->redirect(
                $this->generateUrl('housingStock_projectStatus_list')
            );
        }

        return $this->render(
            'KeirusHousingStockBundle:ProjectStatus:update.html.twig',
            array(
                'form' => $formHandler->getForm()->createView(),
                'projectStatus' => $projectStatus
            )
        );
    }


    /**
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction($id)
    {
        $manager = $this->get('housingstock_projectstatus_manager');
        $projectStatus = $manager->getProjectStatus($id);

        if ($projectStatus instanceof ProjectStatus) {
            $manager->removeAndFlush($projectStatus);

            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.action.deleted',
                'admin.housingStock.projectStatus.deleted'
            );
        } else {
            throw new NotFoundHttpException("ProjectStatus not found");
        }

        return $this->redirect($this->generateUrl('housingStock_projectStatus_list'));
    }


    /**
     * @param $id
     * @return Response
     * @throws EntityNotFoundException
     */
    public function showAction($id)
    {
        $manager = $this->get('housingstock_projectstatus_manager');
        $projectStatus = $manager->getProjectStatus($id);


        return $this->render(
            'KeirusHousingStockBundle:ProjectStatus:show.html.twig',
            array('projectStatus' => $projectStatus)
        );
    }
}