<?php
namespace Keirus\HousingStockBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Keirus\CoreBundle\Manager\BasePaginatorManager;
use Keirus\HousingStockBundle\Form\Type\ProjectFileForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keirus\HousingStockBundle\Entity\ProjectFile;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class ProjectFileController
 * @package Keirus\HousingStockBundle\Controller
 */
class ProjectFileController extends Controller
{

    /**
     * @return mixed
     */
    public function listAction($page, $key)
    {

        return $this->dataResult($page, $key);
    }

    /**
     * @param $page
     * @param $key
     * @return Response
     */
    private function dataResult($page, $key)
    {

        $manager = $this->get('housingstock_projectfile_manager');
        $rpp = $this->container->getParameter('elements_per_page');


        list($result, $totalCount) = $manager->getAllProjectFilesPaginated($page, $rpp, $key);

        $paginator = new BasePaginatorManager($page, $totalCount, $rpp);

        if ($totalCount < 1) {
            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.alert.info',
                'admin.search.noResults'
            );
        }

        return $this->render(
            'KeirusHousingStockBundle:ProjectFile:list.html.twig',
            array(
                'projectFiles' => $result,
                'paginator' => $paginator->getPagesList(),
                'cur' => $page,
                'total' => $paginator->getTotalPages(),
                'key' => $key
            )
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $key = $request->request->get('key');
        $page = 1;

        return $this->dataResult($page, $key);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction()
    {

        $formHandler = $this->get('housingstock_projectfile_handler');

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'info',
                'admin.action.created',
                'admin.housingStock.projectFile.created'
            );

            return $this->redirect(
                $this->generateUrl('housingStock_projectFile_list')
            );
        }

        return $this->render(
            'KeirusHousingStockBundle:ProjectFile:create.html.twig',
            array('form' => $formHandler->getForm()->createView())
        );
    }


    /**
     * @param $id
     * @return RedirectResponse|Response
     */
    public function updateAction($id)
    {

        $manager = $this->get('housingstock_projectfile_manager');
        $projectFile = $manager->getProjectFile($id);

        $formHandler = $this->get('housingstock_projectfile_handler');
        $form = $this->createForm(new ProjectFileForm(), $projectFile);
        $formHandler->setForm($form);

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'success',
                'admin.action.updated',
                'admin.housingStock.projectFile.updated'
            );

            return $this->redirect(
                $this->generateUrl('housingStock_projectFile_list')
            );
        }

        return $this->render(
            'KeirusHousingStockBundle:ProjectFile:update.html.twig',
            array(
                'form' => $formHandler->getForm()->createView(),
                'projectFile' => $projectFile
            )
        );
    }


    /**
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction($id)
    {
        $manager = $this->get('housingstock_projectfile_manager');
        $projectFile = $manager->getProjectFile($id);

        if ($projectFile instanceof ProjectFile) {
            $manager->removeAndFlush($projectFile);

            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.action.deleted',
                'admin.housingStock.projectFile.deleted'
            );
        } else {
            throw new NotFoundHttpException("ProjectFile not found");
        }

        return $this->redirect($this->generateUrl('housingStock_projectFile_list'));
    }


    /**
     * @param $id
     * @return Response
     * @throws EntityNotFoundException
     */
    public function showAction($id)
    {
        $manager = $this->get('housingstock_projectfile_manager');
        $projectFile = $manager->getProjectFile($id);


        return $this->render(
            'KeirusHousingStockBundle:ProjectFile:show.html.twig',
            array('projectFile' => $projectFile)
        );
    }
}