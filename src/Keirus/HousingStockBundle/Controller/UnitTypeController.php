<?php
namespace Keirus\HousingStockBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Keirus\CoreBundle\Manager\BasePaginatorManager;
use Keirus\HousingStockBundle\Form\Type\UnitTypeForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keirus\HousingStockBundle\Entity\UnitType;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class UnitTypeController
 * @package Keirus\HousingStockBundle\Controller
 */
class UnitTypeController extends Controller
{

    /**
     * @return mixed
     */
    public function listAction($page, $key)
    {

        return $this->dataResult($page, $key);
    }

    /**
     * @param $page
     * @param $key
     * @return Response
     */
    private function dataResult($page, $key)
    {

        $manager = $this->get('housingstock_unittype_manager');
        $rpp = $this->container->getParameter('elements_per_page');


        list($result, $totalCount) = $manager->getAllUnitTypesPaginated($page, $rpp, $key);

        $paginator = new BasePaginatorManager($page, $totalCount, $rpp);

        if ($totalCount < 1) {
            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.alert.info',
                'admin.search.noResults'
            );
        }

        return $this->render(
            'KeirusHousingStockBundle:UnitType:list.html.twig',
            array(
                'unitTypes' => $result,
                'paginator' => $paginator->getPagesList(),
                'cur' => $page,
                'total' => $paginator->getTotalPages(),
                'key' => $key
            )
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $key = $request->request->get('key');
        $page = 1;

        return $this->dataResult($page, $key);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction()
    {

        $formHandler = $this->get('housingstock_unittype_handler');

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'info',
                'admin.action.created',
                'admin.housingStock.unitType.created'
            );

            return $this->redirect(
                $this->generateUrl('housingStock_unitType_list')
            );
        }

        return $this->render(
            'KeirusHousingStockBundle:UnitType:create.html.twig',
            array('form' => $formHandler->getForm()->createView())
        );
    }


    /**
     * @param $id
     * @return RedirectResponse|Response
     */
    public function updateAction($id)
    {

        $manager = $this->get('housingstock_unittype_manager');
        $unitType = $manager->getUnitType($id);

        $formHandler = $this->get('housingstock_unittype_handler');
        $form = $this->createForm(new UnitTypeForm(), $unitType);
        $formHandler->setForm($form);

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'success',
                'admin.action.updated',
                'admin.housingStock.unitType.updated'
            );

            return $this->redirect(
                $this->generateUrl('housingStock_unitType_list')
            );
        }

        return $this->render(
            'KeirusHousingStockBundle:UnitType:update.html.twig',
            array(
                'form' => $formHandler->getForm()->createView(),
                'unitType' => $unitType
            )
        );
    }


    /**
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction($id)
    {
        $manager = $this->get('housingstock_unittype_manager');
        $unitType = $manager->getUnitType($id);

        if ($unitType instanceof UnitType) {
            $manager->removeAndFlush($unitType);

            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.action.deleted',
                'admin.housingStock.unitType.deleted'
            );
        } else {
            throw new NotFoundHttpException("UnitType not found");
        }

        return $this->redirect($this->generateUrl('housingStock_unitType_list'));
    }


    /**
     * @param $id
     * @return Response
     * @throws EntityNotFoundException
     */
    public function showAction($id)
    {
        $manager = $this->get('housingstock_unittype_manager');
        $unitType = $manager->getUnitType($id);


        return $this->render('KeirusHousingStockBundle:UnitType:show.html.twig', array('unitType' => $unitType));
    }
}