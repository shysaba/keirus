<?php
namespace Keirus\HousingStockBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Keirus\CoreBundle\Manager\BasePaginatorManager;
use Keirus\HousingStockBundle\Entity\Project;
use Keirus\HousingStockBundle\Form\Type\ProjectPropertyForm;
use Keirus\UserBundle\Entity\Builder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keirus\HousingStockBundle\Entity\ProjectProperty;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use JMS\SecurityExtraBundle\Annotation\Secure;


/**
 * Class ProjectPropertyController
 * @package Keirus\HousingStockBundle\Controller
 */
class ProjectPropertyController extends Controller
{

    /**
     * @return mixed
     */
    public function listAction($project, $page, $key)
    {

        return $this->dataResult($project, $page, $key);
    }

    /**
     * @param $page
     * @param $key
     * @return Response
     */
    private function dataResult($project, $page, $key)
    {

        $projectManager = $this->get('housingstock_project_manager');
        $project = $projectManager->getProject($project);

        if ($project instanceof Project) {

            $manager = $this->get('housingstock_projectproperty_manager');
            $rpp = $this->container->getParameter('elements_per_page');
            $id = $project->getId();

            list($result, $totalCount) = $manager->getAllProjectPropertiesPaginated($id, $page, $rpp, $key);

            $paginator = new BasePaginatorManager($page, $totalCount, $rpp);

            if ($totalCount < 1) {
                $this->get('core_flash_message_manager')->setMessage(
                    'danger',
                    'admin.alert.info',
                    'admin.search.noResults'
                );
            }

            return $this->render(
                'KeirusHousingStockBundle:ProjectProperty:list.html.twig',
                array(
                    'projectProperties' => $result,
                    'project' => $project,
                    'paginator' => $paginator->getPagesList(),
                    'cur' => $page,
                    'total' => $paginator->getTotalPages(),
                    'key' => $key
                )
            );

        } else {
            throw new NotFoundHttpException("Project not found");
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $key = $request->request->get('key');
        $page = 1;
        $project = null;

        return $this->dataResult($project, $page, $key);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @Secure(roles="ROLE_BUILDER")
     */
    public function createAction($project)
    {

        $projectManager = $this->get('housingstock_project_manager');
        $project = $projectManager->getProject($project);

        if ($project instanceof Project) {

            $formHandler = $this->get('housingstock_projectproperty_handler');


            if ($formHandler->process()) {

                $this->get('core_flash_message_manager')->setMessage(
                    'info',
                    'admin.action.created',
                    'admin.housingStock.projectProperty.created'
                );

                return $this->redirect(
                    $this->generateUrl('housingStock_projectProperty_list', array('project' => $project->getId()))
                );
            }

            return $this->render(
                'KeirusHousingStockBundle:ProjectProperty:create.html.twig',
                array(
                    'form' => $formHandler->getForm()->createView(),
                    'project' => $project
                )
            );
        } else {
            throw new NotFoundHttpException("Project not found");
        }
    }


    /**
     * @param $id
     * @return RedirectResponse|Response
     */
    public function updateAction($id)
    {
        $manager = $this->get('housingstock_projectproperty_manager');
        $projectProperty = $manager->getProjectProperty($id);

        if ($projectProperty instanceof ProjectProperty) {
            $formHandler = $this->get('housingstock_projectproperty_handler');
            $form = $this->createForm(new ProjectPropertyForm(), $projectProperty);
            $formHandler->setForm($form);

            if ($formHandler->process()) {
                $this->get('core_flash_message_manager')->setMessage(
                    'success',
                    'admin.action.updated',
                    'admin.housingStock.projectProperty.updated'
                );

                 return $this->redirect(
                     $this->generateUrl('housingStock_projectProperty_list', array('project' => $projectProperty->getProject()->getId()))
                 );
            }

            return $this->render(
                'KeirusHousingStockBundle:ProjectProperty:update.html.twig',
                array(
                    'form' => $formHandler->getForm()->createView(),
                    'projectProperty' => $projectProperty
                )
            );
        } else {
            throw new NotFoundHttpException("ProjectProperty not found");
        }
    }


    /**
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction($id)
    {
        $manager = $this->get('housingstock_projectproperty_manager');
        $projectproperty = $manager->getProjectProperty($id);
        $projectId = $projectproperty->getProject()->getId();

        if ($projectproperty instanceof ProjectProperty) {
            $manager->removeAndFlush($projectproperty);

            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.action.deleted',
                'admin.housingStock.projectProperty.deleted'
            );
        } else {
            throw new NotFoundHttpException("ProjectProperty not found");
        }

        return $this->redirect($this->generateUrl('housingStock_projectProperty_list', array('project' => $projectId)));
    }


    /**
     * @param $id
     * @return Response
     * @throws EntityNotFoundException
     */
    public function showAction($id)
    {

        $manager = $this->get('housingstock_projectproperty_manager');
        $projectProperty = $manager->getProjectProperty($id);

        if ($projectProperty instanceof ProjectProperty) {

            return $this->render(
                'KeirusHousingStockBundle:ProjectProperty:show.html.twig',
                array(
                    'projectProperty' => $projectProperty
                )
            );

        } else {
            throw new NotFoundHttpException("Project property not found");
        }
    }
}