<?php

namespace Keirus\RealEstateBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class AmenityRepository
 * @package Keirus\RealEstateBundle\Repository
 */
class AmenityRepository extends EntityRepository
{

    /**
     * Find all unit types
     * @param string $sort
     * @param string $order
     * @return array
     */
    public function getAllAmenitiesOrderBy($sort, $order)
    {
        return $this->createQueryBuilder('a')
            ->orderBy($sort, $order)
            ->getQuery();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllAmenitiesPaginated($page, $rpp, $key = null)
    {
        $count = $this->getCountAmenities($key);

        $query = $this->createQueryBuilder('a')
            ->leftJoin('a.category', 'c')
            ->addSelect('c')
            ->setMaxResults($rpp)
            ->setFirstResult(($page - 1) * $rpp)
            ->orderBy('c.name', 'ASC');

        if ($key <> null) {
            $query->Where('a.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $result = $query->getQuery()
            ->getResult();


        return array($result, $count);
    }

    /**
     * @param $key
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getCountAmenities($key)
    {
        $query = $this->createQueryBuilder('a');
        $query->select('COUNT(a)');

        if (!empty($key)) {
            $query->Where('a.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $menu = $query->getQuery()
            ->getSingleScalarResult();

        return $menu;
    }
}