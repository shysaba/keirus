<?php

namespace Keirus\RealEstateBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class HouseRepository
 * @package Keirus\RealEstateBundle\Repository
 */
class HouseRepository extends EntityRepository
{

    /**
     * Find all houses
     * @param string $sort
     * @param string $order
     * @return array
     */
    public function getAllHousesOrderBy($sort, $order)
    {
        return $this->createQueryBuilder('a')
            ->orderBy($sort, $order)
            ->getQuery();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllHousesPaginated($page, $rpp, $key = null)
    {
        $count = $this->getCountHouses($key);

        $query = $this->createQueryBuilder('a')
            ->setMaxResults($rpp)
            ->setFirstResult(($page - 1) * $rpp)
            ->orderBy('a.title', 'ASC');

        if ($key <> null) {
            $query->Where('a.title LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $result = $query->getQuery()
            ->getResult();


        return array($result, $count);
    }

    /**
     * @param $key
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getCountHouses($key)
    {
        $query = $this->createQueryBuilder('a');
        $query->select('COUNT(a)');

        if (!empty($key)) {
            $query->Where('a.title LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $menu = $query->getQuery()
            ->getSingleScalarResult();

        return $menu;
    }
}