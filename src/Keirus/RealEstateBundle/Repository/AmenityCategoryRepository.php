<?php

namespace Keirus\RealEstateBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class AmenityCategoryRepository
 * @package Keirus\RealEstateBundle\Repository
 */
class AmenityCategoryRepository extends EntityRepository
{

    /**
     * Find all unit types
     * @param string $sort
     * @param string $order
     * @return array
     */
    public function getAllAmenityCategoriesOrderBy($sort, $order)
    {
        return $this->createQueryBuilder('a')
            ->orderBy($sort, $order)
            ->getQuery();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllAmenityCategoriesPaginated($page, $rpp, $key = null)
    {
        $count = $this->getCountAmenityCategories($key);

        $query = $this->createQueryBuilder('a')
            ->setMaxResults($rpp)
            ->setFirstResult(($page - 1) * $rpp)
            ->orderBy('a.name', 'DESC');

        if ($key <> null) {
            $query->Where('a.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $result = $query->getQuery()
            ->getResult();


        return array($result, $count);
    }

    /**
     * @param $key
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getCountAmenityCategories($key)
    {
        $query = $this->createQueryBuilder('a');
        $query->select('COUNT(a)');

        if (!empty($key)) {
            $query->Where('a.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $menu = $query->getQuery()
            ->getSingleScalarResult();

        return $menu;
    }


    /**
     * Find all cities by terms
     * @param $search
     * @param $sort
     * @param $order
     * @return array
     */
    public function getAllAmenityCategoriesBy($search, $sort, $order)
    {

        return $this->createQueryBuilder('a')
            ->select('a.id', 'a.name as value')
            ->Where('a.name LIKE :category')
            ->setParameter('category', '%'.$search.'%')
            ->orderBy($sort, $order)
            ->getQuery()
            ->getArrayResult();
    }
}