<?php

namespace Keirus\RealEstateBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class PropertyPhotoRepository
 * @package Keirus\RealEstateBundle\Repository
 */
class PropertyPhotoRepository extends EntityRepository
{

    /**
     * Find all photos of properties
     * @param string $sort
     * @param string $order
     * @return array
     */
    public function getAllPropertyPhotosOrderBy($sort, $order)
    {
        return $this->createQueryBuilder('p')
            ->orderBy($sort, $order)
            ->getQuery();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllPropertyPhotosPaginated($page, $rpp, $key = null)
    {
        $count = $this->getCountPropertyPhotos($key);

        $query = $this->createQueryBuilder('p')
            ->setMaxResults($rpp)
            ->setFirstResult(($page - 1) * $rpp)
            ->orderBy('p.id', 'ASC');

        if ($key <> null) {
            $query->Where('p.id LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $result = $query->getQuery()
            ->getResult();


        return array($result, $count);
    }

    /**
     * @param $key
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getCountPropertyPhotos($key)
    {
        $query = $this->createQueryBuilder('p');
        $query->select('COUNT(p)');

        if (!empty($key)) {
            $query->Where('p.id LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $menu = $query->getQuery()
            ->getSingleScalarResult();

        return $menu;
    }
}