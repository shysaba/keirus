<?php

namespace Keirus\RealEstateBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class FlooringRepository
 * @package Keirus\RealEstateBundle\Repository
 */
class FlooringRepository extends EntityRepository
{

    /**
     * Find all unit types
     * @param string $sort
     * @param string $order
     * @return array
     */
    public function getAllFlooringsOrderBy($sort, $order)
    {
        return $this->createQueryBuilder('f')
            ->orderBy($sort, $order)
            ->getQuery();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllFlooringsPaginated($page, $rpp, $key = null)
    {
        $count = $this->getCountFloorings($key);

        $query = $this->createQueryBuilder('f')
            ->setMaxResults($rpp)
            ->setFirstResult(($page - 1) * $rpp)
            ->orderBy('f.name', 'DESC');

        if ($key <> null) {
            $query->Where('f.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $result = $query->getQuery()
            ->getResult();


        return array($result, $count);
    }

    /**
     * @param $key
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getCountFloorings($key)
    {
        $query = $this->createQueryBuilder('f');
        $query->select('COUNT(f)');

        if (!empty($key)) {
            $query->Where('f.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $menu = $query->getQuery()
            ->getSingleScalarResult();

        return $menu;
    }
}