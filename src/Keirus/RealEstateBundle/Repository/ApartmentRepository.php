<?php

namespace Keirus\RealEstateBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class ApartmentRepository
 * @package Keirus\RealEstateBundle\Repository
 */
class ApartmentRepository extends EntityRepository
{

    /**
     * Find all apartments
     * @param string $sort
     * @param string $order
     * @return array
     */
    public function getAllApartmentsOrderBy($sort, $order)
    {
        return $this->createQueryBuilder('a')
            ->orderBy($sort, $order)
            ->getQuery();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllApartmentsPaginated($page, $rpp, $key = null)
    {
        $count = $this->getCountApartments($key);

        $query = $this->createQueryBuilder('a')
            ->setMaxResults($rpp)
            ->setFirstResult(($page - 1) * $rpp)
            ->orderBy('a.title', 'ASC');

        if ($key <> null) {
            $query->Where('a.title LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $result = $query->getQuery()
            ->getResult();


        return array($result, $count);
    }

    /**
     * @param $key
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getCountApartments($key)
    {
        $query = $this->createQueryBuilder('a');
        $query->select('COUNT(a)');

        if (!empty($key)) {
            $query->Where('a.title LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $menu = $query->getQuery()
            ->getSingleScalarResult();

        return $menu;
    }


    public function getApartment($id)
    {
        $query = $this->createQueryBuilder('a')
            ->leftJoin('a.city', 'c')
            ->addSelect('c')
            ->leftJoin('a.directionFacing', 'd')
            ->addSelect('d')
            ->leftJoin('a.flooring', 'f')
            ->addSelect('f')
            ->leftJoin('a.ownershipType', 'o')
            ->addSelect('o')
            ->leftJoin('a.propertyPhotos', 'p')
            ->addSelect('p')
            ->leftJoin('a.amenities', 'm')
            ->addSelect('m')
            ->leftJoin('m.category', 'y')
            ->addSelect('y')
            ->leftJoin('p.kindPropertyPhoto', 'k')
            ->addSelect('k')
            ->leftJoin('a.broker', 'b')
            ->addSelect('b')
            ->leftJoin('a.individual', 'i')
            ->addSelect('i')
            ->where('a.id = :id')
            ->setParameter('id', $id)
            ->orderBy('m.category', 'ASC');

        $result = $query->getQuery()
            ->getOneOrNullResult();

        return $result;
    }
}