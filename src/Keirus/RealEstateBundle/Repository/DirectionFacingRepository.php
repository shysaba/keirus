<?php

namespace Keirus\RealEstateBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class DirectionFacingRepository
 * @package Keirus\RealEstateBundle\Repository
 */
class DirectionFacingRepository extends EntityRepository
{

    /**
     * Find all unit types
     * @param string $sort
     * @param string $order
     * @return array
     */
    public function getAllDirectionsFacingOrderBy($sort, $order)
    {
        return $this->createQueryBuilder('d')
            ->orderBy($sort, $order)
            ->getQuery();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllDirectionsFacingPaginated($page, $rpp, $key = null)
    {
        $count = $this->getCountDirectionsFacing($key);

        $query = $this->createQueryBuilder('d')
            ->setMaxResults($rpp)
            ->setFirstResult(($page - 1) * $rpp)
            ->orderBy('d.name', 'DESC');

        if ($key <> null) {
            $query->Where('d.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $result = $query->getQuery()
            ->getResult();


        return array($result, $count);
    }

    /**
     * @param $key
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getCountDirectionsFacing($key)
    {
        $query = $this->createQueryBuilder('d');
        $query->select('COUNT(d)');

        if (!empty($key)) {
            $query->Where('d.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $menu = $query->getQuery()
            ->getSingleScalarResult();

        return $menu;
    }
}