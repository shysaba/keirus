<?php

namespace Keirus\RealEstateBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class KindPropertyPhotoRepository
 * @package Keirus\RealEstateBundle\Repository
 */
class KindPropertyPhotoRepository extends EntityRepository
{

    /**
     * Find all unit types
     * @param string $sort
     * @param string $order
     * @return array
     */
    public function getAllKindsPhotoOrderBy($sort, $order)
    {
        return $this->createQueryBuilder('k')
            ->orderBy($sort, $order)
            ->getQuery();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllKindsPhotoPaginated($page, $rpp, $key = null)
    {
        $count = $this->getCountKindsPhoto($key);

        $query = $this->createQueryBuilder('k')
            ->setMaxResults($rpp)
            ->setFirstResult(($page - 1) * $rpp)
            ->orderBy('k.name', 'DESC');

        if ($key <> null) {
            $query->Where('k.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $result = $query->getQuery()
            ->getResult();


        return array($result, $count);
    }

    /**
     * @param $key
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getCountKindsPhoto($key)
    {
        $query = $this->createQueryBuilder('k');
        $query->select('COUNT(k)');

        if (!empty($key)) {
            $query->Where('k.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $menu = $query->getQuery()
            ->getSingleScalarResult();

        return $menu;
    }
}