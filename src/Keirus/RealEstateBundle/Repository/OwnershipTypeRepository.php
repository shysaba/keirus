<?php

namespace Keirus\RealEstateBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class OwnershipTypeRepository
 * @package Keirus\RealEstateBundle\Repository
 */
class OwnershipTypeRepository extends EntityRepository
{

    /**
     * Find all unit types
     * @param string $sort
     * @param string $order
     * @return array
     */
    public function getAllOwnershipTypesOrderBy($sort, $order)
    {
        return $this->createQueryBuilder('o')
            ->orderBy($sort, $order)
            ->getQuery();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllOwnershipTypesPaginated($page, $rpp, $key = null)
    {
        $count = $this->getCountOwnershipTypes($key);

        $query = $this->createQueryBuilder('o')
            ->setMaxResults($rpp)
            ->setFirstResult(($page - 1) * $rpp)
            ->orderBy('o.type', 'DESC');

        if ($key <> null) {
            $query->Where('o.type LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $result = $query->getQuery()
            ->getResult();


        return array($result, $count);
    }

    /**
     * @param $key
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getCountOwnershipTypes($key)
    {
        $query = $this->createQueryBuilder('o');
        $query->select('COUNT(o)');

        if (!empty($key)) {
            $query->Where('o.type LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $menu = $query->getQuery()
            ->getSingleScalarResult();

        return $menu;
    }
}