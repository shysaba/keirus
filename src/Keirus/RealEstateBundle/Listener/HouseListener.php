<?php

namespace Keirus\RealEstateBundle\Listener;

use Keirus\CoreBundle\Manager\LoggerManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

/**
 * Class HouseListener
 * @package Keirus\RealEstateBundle\Listener
 */
class HouseListener
{

    /**
     * @var LoggerManager
     */
    private $loggerManager;

    /**
     * @param LoggerManager $loggerManager
     */
    public function __construct(LoggerManager $loggerManager)
    {
        $this->loggerManager = $loggerManager;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function process(GetResponseEvent $event)
    {
        if ($event->getRequest()->attributes->get('_route') == "realestate_house_delete") {
            $this->loggerManager->write();
        }
    }
}