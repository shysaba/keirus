<?php
namespace Keirus\RealEstateBundle\Manager;

use Keirus\CoreBundle\Manager\BaseManager;
use Keirus\RealEstateBundle\Entity\AmenityCategory;
use Doctrine\ORM\EntityManager;


/**
 * Class AmenityCategoryManager
 * @package Keirus\RealEstateBundle\Manager
 */
class AmenityCategoryManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;


    /**
     * @var \Keirus\RealEstateBundle\Repository\AmenityCategoryRepository
     */
    protected $repository;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('KeirusRealEstateBundle:AmenityCategory');
    }

    /**
     * Persist and flush a unit type
     * @param AmenityCategory $amenityCategory
     */
    public function save(AmenityCategory $amenityCategory)
    {
        $this->persistAndFlush($amenityCategory);
    }

    /**
     * Delete a unit type
     * @param AmenityCategory $amenityCategory
     * @param boolean $flush
     */
    public function delete(AmenityCategory $amenityCategory, $flush = true)
    {
        if ($flush) {
            $this->removeAndFlush($amenityCategory);
        } else {
            $this->remove($amenityCategory);
        }
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllAmenityCategoriesPaginated($page, $rpp, $key = null)
    {
        return $this->repository->getAllAmenityCategoriesPaginated($page, $rpp, $key);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getAmenityCategory($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param $search
     * @param $sort
     * @param $order
     * @return array
     */
    public function getAllAmenityCategoriesBy($search, $sort, $order)
    {
        return $this->repository->getAllAmenityCategoriesBy($search, $sort, $order);
    }
}