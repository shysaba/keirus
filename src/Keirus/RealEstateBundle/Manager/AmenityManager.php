<?php
namespace Keirus\RealEstateBundle\Manager;

use Keirus\CoreBundle\Manager\BaseManager;
use Keirus\RealEstateBundle\Entity\Amenity;
use Doctrine\ORM\EntityManager;


/**
 * Class AmenityManager
 * @package Keirus\RealEstateBundle\Manager
 */
class AmenityManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;


    /**
     * @var \Keirus\RealEstateBundle\Repository\AmenityRepository
     */
    protected $repository;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('KeirusRealEstateBundle:Amenity');
    }

    /**
     * Persist and flush a unit type
     * @param Amenity $amenity
     */
    public function save(Amenity $amenity)
    {
        $this->persistAndFlush($amenity);
    }

    /**
     * Delete a unit type
     * @param Amenity $amenity
     * @param boolean $flush
     */
    public function delete(Amenity $amenity, $flush = true)
    {
        if ($flush) {
            $this->removeAndFlush($amenity);
        } else {
            $this->remove($amenity);
        }
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllAmenitiesPaginated($page, $rpp, $key = null)
    {
        return $this->repository->getAllAmenitiesPaginated($page, $rpp, $key);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getAmenity($id)
    {
        return $this->repository->find($id);
    }
}