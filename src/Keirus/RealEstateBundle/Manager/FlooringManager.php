<?php
namespace Keirus\RealEstateBundle\Manager;

use Keirus\CoreBundle\Manager\BaseManager;
use Keirus\RealEstateBundle\Entity\Flooring;
use Doctrine\ORM\EntityManager;


/**
 * Class FlooringManager
 * @package Keirus\RealEstateBundle\Manager
 */
class FlooringManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;


    /**
     * @var \Keirus\RealEstateBundle\Repository\FlooringRepository
     */
    protected $repository;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('KeirusRealEstateBundle:Flooring');
    }

    /**
     * Persist and flush a unit type
     * @param Flooring $flooring
     */
    public function save(Flooring $flooring)
    {
        $this->persistAndFlush($flooring);
    }

    /**
     * Delete a unit type
     * @param Flooring $flooring
     * @param boolean $flush
     */
    public function delete(Flooring $flooring, $flush = true)
    {
        if ($flush) {
            $this->removeAndFlush($flooring);
        } else {
            $this->remove($flooring);
        }
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllFlooringsPaginated($page, $rpp, $key = null)
    {
        return $this->repository->getAllFlooringsPaginated($page, $rpp, $key);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getFlooring($id)
    {
        return $this->repository->find($id);
    }
}