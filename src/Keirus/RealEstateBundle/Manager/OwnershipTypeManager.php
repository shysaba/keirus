<?php
namespace Keirus\RealEstateBundle\Manager;

use Keirus\CoreBundle\Manager\BaseManager;
use Keirus\RealEstateBundle\Entity\OwnershipType;
use Doctrine\ORM\EntityManager;


/**
 * Class OwnershipTypeManager
 * @package Keirus\RealEstateBundle\Manager
 */
class OwnershipTypeManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;


    /**
     * @var \Keirus\RealEstateBundle\Repository\OwnershipTypeRepository
     */
    protected $repository;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('KeirusRealEstateBundle:OwnershipType');
    }

    /**
     * Persist and flush a unit type
     * @param OwnershipType $ownershipType
     */
    public function save(OwnershipType $ownershipType)
    {
        $this->persistAndFlush($ownershipType);
    }

    /**
     * Delete a unit type
     * @param OwnershipType $ownershipType
     * @param boolean $flush
     */
    public function delete(OwnershipType $ownershipType, $flush = true)
    {
        if ($flush) {
            $this->removeAndFlush($ownershipType);
        } else {
            $this->remove($ownershipType);
        }
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllOwnershipTypesPaginated($page, $rpp, $key = null)
    {
        return $this->repository->getAllOwnershipTypesPaginated($page, $rpp, $key);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getOwnershipType($id)
    {
        return $this->repository->find($id);
    }
}