<?php
namespace Keirus\RealEstateBundle\Manager;

use Keirus\CoreBundle\Manager\BaseManager;
use Keirus\RealEstateBundle\Entity\DirectionFacing;
use Doctrine\ORM\EntityManager;


/**
 * Class DirectionFacingManager
 * @package Keirus\RealEstateBundle\Manager
 */
class DirectionFacingManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;


    /**
     * @var \Keirus\RealEstateBundle\Repository\DirectionFacingRepository
     */
    protected $repository;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('KeirusRealEstateBundle:DirectionFacing');
    }

    /**
     * Persist and flush a unit type
     * @param DirectionFacing $directionFacing
     */
    public function save(DirectionFacing $directionFacing)
    {
        $this->persistAndFlush($directionFacing);
    }

    /**
     * Delete a unit type
     * @param DirectionFacing $directionFacing
     * @param boolean $flush
     */
    public function delete(DirectionFacing $directionFacing, $flush = true)
    {
        if ($flush) {
            $this->removeAndFlush($directionFacing);
        } else {
            $this->remove($directionFacing);
        }
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllDirectionsFacingPaginated($page, $rpp, $key = null)
    {
        return $this->repository->getAllDirectionsFacingPaginated($page, $rpp, $key);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getDirectionFacing($id)
    {
        return $this->repository->find($id);
    }
}