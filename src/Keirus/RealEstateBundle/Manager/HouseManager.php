<?php
namespace Keirus\RealEstateBundle\Manager;

use Keirus\CoreBundle\Manager\BaseManager;
use Keirus\RealEstateBundle\Entity\House;
use Doctrine\ORM\EntityManager;


/**
 * Class HouseManager
 * @package Keirus\RealEstateBundle\Manager
 */
class HouseManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;


    /**
     * @var \Keirus\RealEstateBundle\Repository\HouseRepository
     */
    protected $repository;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('KeirusRealEstateBundle:House');
    }

    /**
     * Persist and flush a house
     * @param House $house
     */
    public function save(House $house)
    {
        $this->persistAndFlush($house);
    }

    /**
     * Delete a unit type
     * @param House $house
     * @param boolean $flush
     */
    public function delete(House $house, $flush = true)
    {
        if ($flush) {
            $this->removeAndFlush($house);
        } else {
            $this->remove($house);
        }
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllHousesPaginated($page, $rpp, $key = null)
    {
        return $this->repository->getAllHousesPaginated($page, $rpp, $key);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getHouse($id)
    {
        return $this->repository->find($id);
    }
}