<?php
namespace Keirus\RealEstateBundle\Manager;

use Keirus\CoreBundle\Manager\BaseManager;
use Keirus\RealEstateBundle\Entity\Apartment;
use Doctrine\ORM\EntityManager;


/**
 * Class ApartmentManager
 * @package Keirus\RealEstateBundle\Manager
 */
class ApartmentManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;


    /**
     * @var \Keirus\RealEstateBundle\Repository\ApartmentRepository
     */
    protected $repository;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('KeirusRealEstateBundle:Apartment');
    }

    /**
     * Persist and flush a house
     * @param Apartment $apartment
     */
    public function save(Apartment $apartment)
    {
        $this->persistAndFlush($apartment);
    }

    /**
     * Delete a unit type
     * @param Apartment $apartment
     * @param boolean $flush
     */
    public function delete(Apartment $apartment, $flush = true)
    {
        if ($flush) {
            $this->removeAndFlush($apartment);
        } else {
            $this->remove($apartment);
        }
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllApartmentsPaginated($page, $rpp, $key = null)
    {
        return $this->repository->getAllApartmentsPaginated($page, $rpp, $key);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getApartment($id)
    {
        return $this->repository->getApartment($id);
    }
}