<?php
namespace Keirus\RealEstateBundle\Manager;

use Keirus\CoreBundle\Manager\BaseManager;
use Keirus\RealEstateBundle\Entity\KindPropertyPhoto;
use Doctrine\ORM\EntityManager;


/**
 * Class KindPropertyPhotoManager
 * @package Keirus\RealEstateBundle\Manager
 */
class KindPropertyPhotoManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;


    /**
     * @var \Keirus\RealEstateBundle\Repository\KindPropertyPhotoRepository
     */
    protected $repository;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('KeirusRealEstateBundle:KindPropertyPhoto');
    }

    /**
     * Persist and flush a unit type
     * @param KindPropertyPhoto $kindPhoto
     */
    public function save(KindPropertyPhoto $kindPhoto)
    {
        $this->persistAndFlush($kindPhoto);
    }

    /**
     * Delete a unit type
     * @param KindPropertyPhoto $kindPhoto
     * @param boolean $flush
     */
    public function delete(KindPropertyPhoto $kindPhoto, $flush = true)
    {
        if ($flush) {
            $this->removeAndFlush($kindPhoto);
        } else {
            $this->remove($kindPhoto);
        }
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllKindsPhotoPaginated($page, $rpp, $key = null)
    {
        return $this->repository->getAllKindsPhotoPaginated($page, $rpp, $key);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getKindPropertyPhoto($id)
    {
        return $this->repository->find($id);
    }
}