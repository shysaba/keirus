<?php
namespace Keirus\RealEstateBundle\Manager;

use Keirus\CoreBundle\Manager\BaseManager;
use Keirus\RealEstateBundle\Entity\PropertyPhoto;
use Doctrine\ORM\EntityManager;


/**
 * Class PropertyPhotoManager
 * @package Keirus\RealEstateBundle\Manager
 */
class PropertyPhotoManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;


    /**
     * @var \Keirus\RealEstateBundle\Repository\PropertyPhotoRepository
     */
    protected $repository;

    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('KeirusRealEstateBundle:PropertyPhoto');
    }

    /**
     * Persist and flush a unit type
     * @param PropertyPhoto $propertyPhoto
     */
    public function save(PropertyPhoto $propertyPhoto)
    {
        $this->persistAndFlush($propertyPhoto);
    }

    /**
     * Delete a unit type
     * @param PropertyPhoto $propertyPhoto
     * @param boolean $flush
     */
    public function delete(PropertyPhoto $propertyPhoto, $flush = true)
    {
        if ($flush) {
            $this->removeAndFlush($propertyPhoto);
        } else {
            $this->remove($propertyPhoto);
        }
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllPropertyPhotosPaginated($page, $rpp, $key = null)
    {
        return $this->repository->getAllPropertyPhotosPaginated($page, $rpp, $key);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getPropertyPhoto($id)
    {
        return $this->repository->find($id);
    }
}