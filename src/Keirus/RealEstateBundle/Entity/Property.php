<?php

namespace Keirus\RealEstateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Property
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Keirus\RealEstateBundle\Repository\PropertyRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorMap({"apartment": "Apartment", "house": "House", "property": "Property"})
 */
abstract class Property
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, unique=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text")
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="Keirus\LocalizationBundle\Entity\City")
     * @ORM\JoinColumn(nullable=false)
     */
    private $city;

    /**
     * @var float
     *
     * @ORM\Column(name="sqftArea", type="float")
     */
    private $sqftArea;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(name="registrationCharges", type="float")
     */
    private $registrationCharges;

    /**
     * @var integer
     *
     * @ORM\Column(name="noBathrooms", type="integer")
     */
    private $noBathrooms;

    /**
     * @var integer
     *
     * @ORM\Column(name="noRooms", type="integer")
     */
    private $noRooms;

    /**
     * @ORM\ManyToOne(targetEntity="Keirus\RealEstateBundle\Entity\DirectionFacing")
     * @ORM\JoinColumn(nullable=false)
     */
    private $directionFacing;

    /**
     * @ORM\ManyToOne(targetEntity="Keirus\RealEstateBundle\Entity\Flooring")
     * @ORM\JoinColumn(nullable=false)
     */
    private $flooring;

    /**
     * @ORM\ManyToOne(targetEntity="Keirus\RealEstateBundle\Entity\OwnershipType")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ownershipType;

    /**
     * @var boolean
     *
     * @ORM\Column(name="validationRequest", type="boolean")
     */
    private $validationRequest;

    /**
     * @var boolean
     *
     * @ORM\Column(name="validated", type="boolean")
     */
    private $validated;

    /**
     * @var boolean
     *
     * @ORM\Column(name="online", type="boolean")
     */
    private $online;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="submittedDate", type="datetime")
     */
    private $submittedDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="postedOnDate", type="datetime")
     */
    private $postedOnDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="views", type="integer")
     */
    private $views;

    /**
     * @var string
     *
     * @ORM\Column(name="brokerageTerms", type="text")
     */
    private $brokerageTerms;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="possessionFrom", type="date")
     */
    private $possessionFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="yearConstruction", type="date")
     */
    private $yearConstruction;

    /**
     * @ORM\ManyToMany(targetEntity="Keirus\RealEstateBundle\Entity\Amenity", cascade={"persist"})
     * @ORM\JoinTable(name="PropertyAmenities")
     */
    private $amenities;

    /**
     * @ORM\ManyToOne(targetEntity="Keirus\UserBundle\Entity\Broker")
     * @ORM\JoinColumn(nullable=true)
     */
    private $broker;

    /**
     * @ORM\ManyToOne(targetEntity="Keirus\UserBundle\Entity\Individual")
     * @ORM\JoinColumn(nullable=true)
     */
    private $individual;

    /**
     * @ORM\OneToMany(targetEntity="Keirus\RealEstateBundle\Entity\PropertyPhoto", mappedBy="property", cascade={"all"}, orphanRemoval=true)
     * @ORM\JoinColumn(nullable=true)
     */
    private $propertyPhotos;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setViews(0);
        $this->setValidated(false);
        $this->setValidationRequest(false);
        $this->setOnline(false);

        $this->setPostedOnDate(new \DateTime());
        $this->setSubmittedDate(new \DateTime());
        $this->amenities = new ArrayCollection();
        $this->propertyPhotos = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Property
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Property
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Property
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set sqftArea
     *
     * @param float $sqftArea
     *
     * @return Property
     */
    public function setSqftArea($sqftArea)
    {
        $this->sqftArea = $sqftArea;

        return $this;
    }

    /**
     * Get sqftArea
     *
     * @return float
     */
    public function getSqftArea()
    {
        return $this->sqftArea;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Property
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set registrationCharges
     *
     * @param float $registrationCharges
     *
     * @return Property
     */
    public function setRegistrationCharges($registrationCharges)
    {
        $this->registrationCharges = $registrationCharges;

        return $this;
    }

    /**
     * Get registrationCharges
     *
     * @return float
     */
    public function getRegistrationCharges()
    {
        return $this->registrationCharges;
    }

    /**
     * Set noBathrooms
     *
     * @param integer $noBathrooms
     *
     * @return Property
     */
    public function setNoBathrooms($noBathrooms)
    {
        $this->noBathrooms = $noBathrooms;

        return $this;
    }

    /**
     * Get noBathrooms
     *
     * @return integer
     */
    public function getNoBathrooms()
    {
        return $this->noBathrooms;
    }

    /**
     * Set noRooms
     *
     * @param integer $noRooms
     *
     * @return Property
     */
    public function setNoRooms($noRooms)
    {
        $this->noRooms = $noRooms;

        return $this;
    }

    /**
     * Get noRooms
     *
     * @return integer
     */
    public function getNoRooms()
    {
        return $this->noRooms;
    }

    /**
     * Set validated
     *
     * @param boolean $validated
     *
     * @return Property
     */
    public function setValidated($validated)
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * Get validated
     *
     * @return boolean
     */
    public function getValidated()
    {
        return $this->validated;
    }

    /**
     * Set online
     *
     * @param boolean $online
     *
     * @return Property
     */
    public function setOnline($online)
    {
        $this->online = $online;

        return $this;
    }

    /**
     * Get online
     *
     * @return boolean
     */
    public function getOnline()
    {
        return $this->online;
    }

    /**
     * Set submittedDate
     *
     * @param \DateTime $submittedDate
     *
     * @return Property
     */
    public function setSubmittedDate($submittedDate)
    {
        $this->submittedDate = $submittedDate;

        return $this;
    }

    /**
     * Get submittedDate
     *
     * @return \DateTime
     */
    public function getSubmittedDate()
    {
        return $this->submittedDate;
    }

    /**
     * Set postedOnDate
     *
     * @param \DateTime $postedOnDate
     *
     * @return Property
     */
    public function setPostedOnDate($postedOnDate)
    {
        $this->postedOnDate = $postedOnDate;

        return $this;
    }

    /**
     * Get postedOnDate
     *
     * @return \DateTime
     */
    public function getPostedOnDate()
    {
        return $this->postedOnDate;
    }

    /**
     * Set views
     *
     * @param integer $views
     *
     * @return Property
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return integer
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Set brokerageTerms
     *
     * @param string $brokerageTerms
     *
     * @return Property
     */
    public function setBrokerageTerms($brokerageTerms)
    {
        $this->brokerageTerms = $brokerageTerms;

        return $this;
    }

    /**
     * Get brokerageTerms
     *
     * @return string
     */
    public function getBrokerageTerms()
    {
        return $this->brokerageTerms;
    }

    /**
     * Set possessionFrom
     *
     * @param \DateTime $possessionFrom
     *
     * @return Property
     */
    public function setPossessionFrom($possessionFrom)
    {
        $this->possessionFrom = $possessionFrom;

        return $this;
    }

    /**
     * Get possessionFrom
     *
     * @return \DateTime
     */
    public function getPossessionFrom()
    {
        return $this->possessionFrom;
    }

    /**
     * Set yearConstruction
     *
     * @param \DateTime $yearConstruction
     *
     * @return Property
     */
    public function setYearConstruction($yearConstruction)
    {
        $this->yearConstruction = $yearConstruction;

        return $this;
    }

    /**
     * Get yearConstruction
     *
     * @return \DateTime
     */
    public function getYearConstruction()
    {
        return $this->yearConstruction;
    }

    /**
     * Set city
     *
     * @param \Keirus\LocalizationBundle\Entity\City $city
     *
     * @return Property
     */
    public function setCity(\Keirus\LocalizationBundle\Entity\City $city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \Keirus\LocalizationBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set directionFacing
     *
     * @param \Keirus\RealEstateBundle\Entity\DirectionFacing $directionFacing
     *
     * @return Property
     */
    public function setDirectionFacing(\Keirus\RealEstateBundle\Entity\DirectionFacing $directionFacing)
    {
        $this->directionFacing = $directionFacing;

        return $this;
    }

    /**
     * Get directionFacing
     *
     * @return \Keirus\RealEstateBundle\Entity\DirectionFacing
     */
    public function getDirectionFacing()
    {
        return $this->directionFacing;
    }

    /**
     * Set flooring
     *
     * @param \Keirus\RealEstateBundle\Entity\Flooring $flooring
     *
     * @return Property
     */
    public function setFlooring(\Keirus\RealEstateBundle\Entity\Flooring $flooring)
    {
        $this->flooring = $flooring;

        return $this;
    }

    /**
     * Get flooring
     *
     * @return \Keirus\RealEstateBundle\Entity\Flooring
     */
    public function getFlooring()
    {
        return $this->flooring;
    }

    /**
     * Set ownershipType
     *
     * @param \Keirus\RealEstateBundle\Entity\OwnershipType $ownershipType
     *
     * @return Property
     */
    public function setOwnershipType(\Keirus\RealEstateBundle\Entity\OwnershipType $ownershipType)
    {
        $this->ownershipType = $ownershipType;

        return $this;
    }

    /**
     * Get ownershipType
     *
     * @return \Keirus\RealEstateBundle\Entity\OwnershipType
     */
    public function getOwnershipType()
    {
        return $this->ownershipType;
    }

    /**
     * Set broker
     *
     * @param \Keirus\UserBundle\Entity\Broker $broker
     *
     * @return Property
     */
    public function setBroker(\Keirus\UserBundle\Entity\Broker $broker = null)
    {
        $this->broker = $broker;

        return $this;
    }

    /**
     * Get broker
     *
     * @return \Keirus\UserBundle\Entity\Broker
     */
    public function getBroker()
    {
        return $this->broker;
    }

    /**
     * Set individual
     *
     * @param \Keirus\UserBundle\Entity\Individual $individual
     *
     * @return Property
     */
    public function setIndividual(\Keirus\UserBundle\Entity\Individual $individual = null)
    {
        $this->individual = $individual;

        return $this;
    }

    /**
     * Get individual
     *
     * @return \Keirus\UserBundle\Entity\Individual
     */
    public function getIndividual()
    {
        return $this->individual;
    }

    /**
     * Add amenity
     *
     * @param \Keirus\RealEstateBundle\Entity\Amenity $amenity
     *
     * @return Property
     */
    public function addAmenity(\Keirus\RealEstateBundle\Entity\Amenity $amenity)
    {
        $this->amenities[] = $amenity;

        return $this;
    }

    /**
     * Remove amenity
     *
     * @param \Keirus\RealEstateBundle\Entity\Amenity $amenity
     */
    public function removeAmenity(\Keirus\RealEstateBundle\Entity\Amenity $amenity)
    {
        $this->amenities->removeElement($amenity);
    }

    /**
     * Get amenities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAmenities()
    {
        return $this->amenities;
    }

    /**
     * Add propertyPhoto
     *
     * @param \Keirus\RealEstateBundle\Entity\PropertyPhoto $propertyPhoto
     *
     * @return Property
     */
    public function addPropertyPhoto(\Keirus\RealEstateBundle\Entity\PropertyPhoto $propertyPhoto)
    {
        $this->propertyPhotos[] = $propertyPhoto;
        $propertyPhoto->setProperty($this);

        return $this;
    }

    /**
     * Remove propertyPhoto
     *
     * @param \Keirus\RealEstateBundle\Entity\PropertyPhoto $propertyPhoto
     */
    public function removePropertyPhoto(\Keirus\RealEstateBundle\Entity\PropertyPhoto $propertyPhoto)
    {
        $this->propertyPhotos->removeElement($propertyPhoto);
    }

    /**
     * Get propertyPhotos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPropertyPhotos()
    {
        return $this->propertyPhotos;
    }

    /**
     * Set validationRequest
     *
     * @param boolean $validationRequest
     *
     * @return Property
     */
    public function setValidationRequest($validationRequest)
    {
        $this->validationRequest = $validationRequest;

        return $this;
    }

    /**
     * Get validationRequest
     *
     * @return boolean
     */
    public function getValidationRequest()
    {
        return $this->validationRequest;
    }
}
