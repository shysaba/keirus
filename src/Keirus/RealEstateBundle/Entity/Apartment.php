<?php

namespace Keirus\RealEstateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Apartment
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Keirus\RealEstateBundle\Repository\ApartmentRepository")
 */
class Apartment extends Property
{
    /**
     * @var integer
     *
     * @ORM\Column(name="totalFloors", type="integer")
     */
    private $totalFloors;

    /**
     * @var integer
     *
     * @ORM\Column(name="propertyOn", type="integer")
     */
    private $propertyOn;


    /**
     * Set totalFloors
     *
     * @param integer $totalFloors
     * @return Apartment
     */
    public function setTotalFloors($totalFloors)
    {
        $this->totalFloors = $totalFloors;

        return $this;
    }

    /**
     * Get totalFloors
     *
     * @return integer
     */
    public function getTotalFloors()
    {
        return $this->totalFloors;
    }

    /**
     * Set propertyOn
     *
     * @param integer $propertyOn
     * @return Apartment
     */
    public function setPropertyOn($propertyOn)
    {
        $this->propertyOn = $propertyOn;

        return $this;
    }

    /**
     * Get propertyOn
     *
     * @return integer
     */
    public function getPropertyOn()
    {
        return $this->propertyOn;
    }
}
