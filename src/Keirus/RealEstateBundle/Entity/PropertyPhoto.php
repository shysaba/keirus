<?php

namespace Keirus\RealEstateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Keirus\CoreBundle\Entity\FileManager;

/**
 * PropertyPhoto
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Keirus\RealEstateBundle\Repository\PropertyPhotoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class PropertyPhoto extends FileManager
{


    /**
     * @ORM\ManyToOne(targetEntity="Keirus\RealEstateBundle\Entity\KindPropertyPhoto")
     * @ORM\JoinColumn(nullable=false)
     */
    private $kindPropertyPhoto;

    /**
     * @ORM\ManyToOne(targetEntity="Keirus\RealEstateBundle\Entity\Property", inversedBy="propertyPhotos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $property;


    /**
     * Set kindPropertyPhoto
     *
     * @param \Keirus\RealEstateBundle\Entity\KindPropertyPhoto $kindPropertyPhoto
     * @return PropertyPhoto
     */
    public function setKindPropertyPhoto(\Keirus\RealEstateBundle\Entity\KindPropertyPhoto $kindPropertyPhoto)
    {
        $this->kindPropertyPhoto = $kindPropertyPhoto;

        return $this;
    }

    /**
     * Get kindPropertyPhoto
     *
     * @return \Keirus\RealEstateBundle\Entity\KindPropertyPhoto
     */
    public function getKindPropertyPhoto()
    {
        return $this->kindPropertyPhoto;
    }

    /**
     * Set property
     *
     * @param \Keirus\RealEstateBundle\Entity\Property $property
     * @return PropertyPhoto
     */
    public function setProperty(\Keirus\RealEstateBundle\Entity\Property $property)
    {
        $this->property = $property;

        return $this;
    }

    /**
     * Get property
     *
     * @return \Keirus\RealEstateBundle\Entity\Property
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * @return string
     */
    protected function getUploadDir()
    {
        return 'uploads/propertyPhotos';
    }
}
