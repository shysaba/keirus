<?php

namespace Keirus\RealEstateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OwnershipType
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Keirus\RealEstateBundle\Repository\OwnershipTypeRepository")
 */
class OwnershipType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, unique=true)
     */
    private $type;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return OwnershipType
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    function __toString()
    {
        return $this->type;
    }
}
