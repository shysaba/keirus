<?php
namespace Keirus\RealEstateBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Keirus\CoreBundle\Manager\BasePaginatorManager;
use Keirus\RealEstateBundle\Form\Type\RealEstateForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keirus\RealEstateBundle\Entity\RealEstate;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class RealEstateController
 * @package Keirus\RealEstateBundle\Controller
 */
class RealEstateController extends Controller
{

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getAmenityCategoriesAction(Request $request)
    {
        $term = $request->query->get('term', null);

        $manager = $this->get('realestate_amenityCategory_manager');
        $states = $manager->getAllAmenityCategoriesBy($term, 'a.name', 'ASC');

        return new JsonResponse($states);
    }
}