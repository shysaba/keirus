<?php
namespace Keirus\RealEstateBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Keirus\CoreBundle\Manager\BasePaginatorManager;
use Keirus\RealEstateBundle\Form\Type\ApartmentForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keirus\RealEstateBundle\Entity\Apartment;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class ApartmentController
 * @package Keirus\RealEstateBundle\Controller
 */
class ApartmentController extends Controller
{

    /**
     * @return mixed
     */
    public function listAction($page, $key)
    {

        return $this->dataResult($page, $key);
    }

    /**
     * @param $page
     * @param $key
     * @return Response
     */
    private function dataResult($page, $key)
    {

        $manager = $this->get('realestate_apartment_manager');
        $rpp = $this->container->getParameter('elements_per_page');


        list($result, $totalCount) = $manager->getAllApartmentsPaginated($page, $rpp, $key);

        $paginator = new BasePaginatorManager($page, $totalCount, $rpp);

        if ($totalCount < 1) {
            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.alert.info',
                'admin.search.noResults'
            );
        }

        return $this->render(
            'KeirusRealEstateBundle:Apartment:list.html.twig',
            array(
                'apartments' => $result,
                'paginator' => $paginator->getPagesList(),
                'cur' => $page,
                'total' => $paginator->getTotalPages(),
                'key' => $key
            )
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $key = $request->request->get('key');
        $page = 1;

        return $this->dataResult($page, $key);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction()
    {

        $formHandler = $this->get('realestate_apartment_handler');

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'info',
                'admin.action.created',
                'admin.realEstate.apartment.created'
            );

            return $this->redirect(
                $this->generateUrl('realEstate_apartment_list')
            );
        }

        return $this->render(
            'KeirusRealEstateBundle:Apartment:create.html.twig',
            array('form' => $formHandler->getForm()->createView())
        );
    }


    /**
     * @param $id
     * @return RedirectResponse|Response
     */
    public function updateAction($id)
    {

        $manager = $this->get('realestate_apartment_manager');
        $apartment = $manager->getApartment($id);

        $formHandler = $this->get('realestate_apartment_handler');
        $form = $this->createForm(new ApartmentForm(), $apartment);
        $formHandler->setForm($form);

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'success',
                'admin.action.updated',
                'admin.realEstate.apartment.updated'
            );

            return $this->redirect(
                $this->generateUrl('realEstate_apartment_list')
            );
        }

        return $this->render(
            'KeirusRealEstateBundle:Apartment:update.html.twig',
            array(
                'form' => $formHandler->getForm()->createView(),
                'apartment' => $apartment
            )
        );
    }


    /**
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction($id)
    {
        $manager = $this->get('realestate_apartment_manager');
        $apartment = $manager->getApartment($id);

        if ($apartment instanceof Apartment) {
            $manager->removeAndFlush($apartment);

            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.action.deleted',
                'admin.realEstate.apartment.deleted'
            );
        } else {
            throw new NotFoundHttpException("Apartment not found");
        }

        return $this->redirect($this->generateUrl('realEstate_apartment_list'));
    }


    /**
     * @param $id
     * @return Response
     * @throws EntityNotFoundException
     */
    public function showAction($id)
    {
        $manager = $this->get('realestate_apartment_manager');
        $apartment = $manager->getApartment($id);


        return $this->render('KeirusRealEstateBundle:Apartment:show.html.twig', array('apartment' => $apartment));
    }
}