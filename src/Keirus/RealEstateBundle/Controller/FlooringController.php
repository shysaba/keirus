<?php
namespace Keirus\RealEstateBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Keirus\CoreBundle\Manager\BasePaginatorManager;
use Keirus\RealEstateBundle\Form\Type\FlooringForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keirus\RealEstateBundle\Entity\Flooring;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class FlooringController
 * @package Keirus\RealEstateBundle\Controller
 */
class FlooringController extends Controller
{

    /**
     * @return mixed
     */
    public function listAction($page, $key)
    {

        return $this->dataResult($page, $key);
    }

    /**
     * @param $page
     * @param $key
     * @return Response
     */
    private function dataResult($page, $key)
    {

        $manager = $this->get('realestate_flooring_manager');
        $rpp = $this->container->getParameter('elements_per_page');


        list($result, $totalCount) = $manager->getAllFlooringsPaginated($page, $rpp, $key);

        $paginator = new BasePaginatorManager($page, $totalCount, $rpp);

        if ($totalCount < 1) {
            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.alert.info',
                'admin.search.noResults'
            );
        }

        return $this->render(
            'KeirusRealEstateBundle:Flooring:list.html.twig',
            array(
                'floorings' => $result,
                'paginator' => $paginator->getPagesList(),
                'cur' => $page,
                'total' => $paginator->getTotalPages(),
                'key' => $key
            )
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $key = $request->request->get('key');
        $page = 1;

        return $this->dataResult($page, $key);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction()
    {

        $formHandler = $this->get('realestate_flooring_handler');

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'info',
                'admin.action.created',
                'admin.realEstate.flooring.created'
            );

            return $this->redirect(
                $this->generateUrl('realEstate_flooring_list')
            );
        }

        return $this->render(
            'KeirusRealEstateBundle:Flooring:create.html.twig',
            array('form' => $formHandler->getForm()->createView())
        );
    }


    /**
     * @param $id
     * @return RedirectResponse|Response
     */
    public function updateAction($id)
    {

        $manager = $this->get('realestate_flooring_manager');
        $flooring = $manager->getFlooring($id);

        $formHandler = $this->get('realestate_flooring_handler');
        $form = $this->createForm(new FlooringForm(), $flooring);
        $formHandler->setForm($form);

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'success',
                'admin.action.updated',
                'admin.realEstate.flooring.updated'
            );

            return $this->redirect(
                $this->generateUrl('realEstate_flooring_list')
            );
        }

        return $this->render(
            'KeirusRealEstateBundle:Flooring:update.html.twig',
            array(
                'form' => $formHandler->getForm()->createView(),
                'flooring' => $flooring
            )
        );
    }


    /**
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction($id)
    {
        $manager = $this->get('realestate_flooring_manager');
        $flooring = $manager->getFlooring($id);

        if ($flooring instanceof Flooring) {
            $manager->removeAndFlush($flooring);

            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.action.deleted',
                'admin.realEstate.flooring.deleted'
            );
        } else {
            throw new NotFoundHttpException("Flooring not found");
        }

        return $this->redirect($this->generateUrl('realEstate_flooring_list'));
    }


    /**
     * @param $id
     * @return Response
     * @throws EntityNotFoundException
     */
    public function showAction($id)
    {
        $manager = $this->get('realestate_flooring_manager');
        $flooring = $manager->getFlooring($id);


        return $this->render('KeirusRealEstateBundle:Flooring:show.html.twig', array('flooring' => $flooring));
    }
}