<?php
namespace Keirus\RealEstateBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Keirus\CoreBundle\Manager\BasePaginatorManager;
use Keirus\RealEstateBundle\Form\Type\OwnershipTypeForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keirus\RealEstateBundle\Entity\OwnershipType;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class OwnershipTypeController
 * @package Keirus\RealEstateBundle\Controller
 */
class OwnershipTypeController extends Controller
{

    /**
     * @return mixed
     */
    public function listAction($page, $key)
    {

        return $this->dataResult($page, $key);
    }

    /**
     * @param $page
     * @param $key
     * @return Response
     */
    private function dataResult($page, $key)
    {

        $manager = $this->get('realestate_ownershipType_manager');
        $rpp = $this->container->getParameter('elements_per_page');


        list($result, $totalCount) = $manager->getAllOwnershipTypesPaginated($page, $rpp, $key);

        $paginator = new BasePaginatorManager($page, $totalCount, $rpp);

        if ($totalCount < 1) {
            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.alert.info',
                'admin.search.noResults'
            );
        }

        return $this->render(
            'KeirusRealEstateBundle:OwnershipType:list.html.twig',
            array(
                'ownershipTypes' => $result,
                'paginator' => $paginator->getPagesList(),
                'cur' => $page,
                'total' => $paginator->getTotalPages(),
                'key' => $key
            )
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $key = $request->request->get('key');
        $page = 1;

        return $this->dataResult($page, $key);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction()
    {

        $formHandler = $this->get('realestate_ownershipType_handler');

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'info',
                'admin.action.created',
                'admin.realEstate.ownershipType.created'
            );

            return $this->redirect(
                $this->generateUrl('realEstate_ownershipType_list')
            );
        }

        return $this->render(
            'KeirusRealEstateBundle:OwnershipType:create.html.twig',
            array('form' => $formHandler->getForm()->createView())
        );
    }


    /**
     * @param $id
     * @return RedirectResponse|Response
     */
    public function updateAction($id)
    {

        $manager = $this->get('realestate_ownershipType_manager');
        $ownershipType = $manager->getOwnershipType($id);

        $formHandler = $this->get('realestate_ownershipType_handler');
        $form = $this->createForm(new OwnershipTypeForm(), $ownershipType);
        $formHandler->setForm($form);

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'success',
                'admin.action.updated',
                'admin.realEstate.ownershipType.updated'
            );

            return $this->redirect(
                $this->generateUrl('realEstate_ownershipType_list')
            );
        }

        return $this->render(
            'KeirusRealEstateBundle:OwnershipType:update.html.twig',
            array(
                'form' => $formHandler->getForm()->createView(),
                'ownershipType' => $ownershipType
            )
        );
    }


    /**
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction($id)
    {
        $manager = $this->get('realestate_ownershipType_manager');
        $ownershipType = $manager->getOwnershipType($id);

        if ($ownershipType instanceof OwnershipType) {
            $manager->removeAndFlush($ownershipType);

            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.action.deleted',
                'admin.realEstate.ownershipType.deleted'
            );
        } else {
            throw new NotFoundHttpException("Kind photo not found");
        }

        return $this->redirect($this->generateUrl('realEstate_ownershipType_list'));
    }


    /**
     * @param $id
     * @return Response
     * @throws EntityNotFoundException
     */
    public function showAction($id)
    {
        $manager = $this->get('realestate_ownershipType_manager');
        $ownershipType = $manager->getOwnershipType($id);


        return $this->render('KeirusRealEstateBundle:OwnershipType:show.html.twig', array('ownershipType' => $ownershipType));
    }
}