<?php
namespace Keirus\RealEstateBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Keirus\CoreBundle\Manager\BasePaginatorManager;
use Keirus\RealEstateBundle\Form\Type\AmenityCategoryForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keirus\RealEstateBundle\Entity\AmenityCategory;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class AmenityCategoryController
 * @package Keirus\RealEstateBundle\Controller
 */
class AmenityCategoryController extends Controller
{

    /**
     * @return mixed
     */
    public function listAction($page, $key)
    {

        return $this->dataResult($page, $key);
    }

    /**
     * @param $page
     * @param $key
     * @return Response
     */
    private function dataResult($page, $key)
    {

        $manager = $this->get('realestate_amenityCategory_manager');
        $rpp = $this->container->getParameter('elements_per_page');


        list($result, $totalCount) = $manager->getAllAmenityCategoriesPaginated($page, $rpp, $key);

        $paginator = new BasePaginatorManager($page, $totalCount, $rpp);

        if ($totalCount < 1) {
            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.alert.info',
                'admin.search.noResults'
            );
        }

        return $this->render(
            'KeirusRealEstateBundle:AmenityCategory:list.html.twig',
            array(
                'amenityCategories' => $result,
                'paginator' => $paginator->getPagesList(),
                'cur' => $page,
                'total' => $paginator->getTotalPages(),
                'key' => $key
            )
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $key = $request->request->get('key');
        $page = 1;

        return $this->dataResult($page, $key);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction()
    {

        $formHandler = $this->get('realestate_amenityCategory_handler');

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'info',
                'admin.action.created',
                'admin.realEstate.amenityCategory.created'
            );

            return $this->redirect(
                $this->generateUrl('realEstate_amenityCategory_list')
            );
        }

        return $this->render(
            'KeirusRealEstateBundle:AmenityCategory:create.html.twig',
            array('form' => $formHandler->getForm()->createView())
        );
    }


    /**
     * @param $id
     * @return RedirectResponse|Response
     */
    public function updateAction($id)
    {

        $manager = $this->get('realestate_amenityCategory_manager');
        $amenityCategory = $manager->getAmenityCategory($id);

        $formHandler = $this->get('realestate_amenityCategory_handler');
        $form = $this->createForm(new AmenityCategoryForm(), $amenityCategory);
        $formHandler->setForm($form);

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'success',
                'admin.action.updated',
                'admin.realEstate.amenityCategory.updated'
            );

            return $this->redirect(
                $this->generateUrl('realEstate_amenityCategory_list')
            );
        }

        return $this->render(
            'KeirusRealEstateBundle:AmenityCategory:update.html.twig',
            array(
                'form' => $formHandler->getForm()->createView(),
                'amenityCategory' => $amenityCategory
            )
        );
    }


    /**
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction($id)
    {
        $manager = $this->get('realestate_amenityCategory_manager');
        $amenityCategory = $manager->getAmenityCategory($id);

        if ($amenityCategory instanceof AmenityCategory) {
            $manager->removeAndFlush($amenityCategory);

            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.action.deleted',
                'admin.realEstate.amenityCategory.deleted'
            );
        } else {
            throw new NotFoundHttpException("AmenityCategory not found");
        }

        return $this->redirect($this->generateUrl('realEstate_amenityCategory_list'));
    }


    /**
     * @param $id
     * @return Response
     * @throws EntityNotFoundException
     */
    public function showAction($id)
    {
        $manager = $this->get('realestate_amenityCategory_manager');
        $amenityCategory = $manager->getAmenityCategory($id);


        return $this->render('KeirusRealEstateBundle:AmenityCategory:show.html.twig', array('amenityCategory' => $amenityCategory));
    }
}