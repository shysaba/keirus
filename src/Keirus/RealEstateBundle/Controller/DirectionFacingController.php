<?php
namespace Keirus\RealEstateBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Keirus\CoreBundle\Manager\BasePaginatorManager;
use Keirus\RealEstateBundle\Form\Type\DirectionFacingForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keirus\RealEstateBundle\Entity\DirectionFacing;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class DirectionFacingController
 * @package Keirus\RealEstateBundle\Controller
 */
class DirectionFacingController extends Controller
{

    /**
     * @return mixed
     */
    public function listAction($page, $key)
    {

        return $this->dataResult($page, $key);
    }

    /**
     * @param $page
     * @param $key
     * @return Response
     */
    private function dataResult($page, $key)
    {

        $manager = $this->get('realestate_directionFacing_manager');
        $rpp = $this->container->getParameter('elements_per_page');


        list($result, $totalCount) = $manager->getAllDirectionsFacingPaginated($page, $rpp, $key);

        $paginator = new BasePaginatorManager($page, $totalCount, $rpp);

        if ($totalCount < 1) {
            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.alert.info',
                'admin.search.noResults'
            );
        }

        return $this->render(
            'KeirusRealEstateBundle:DirectionFacing:list.html.twig',
            array(
                'directionFacings' => $result,
                'paginator' => $paginator->getPagesList(),
                'cur' => $page,
                'total' => $paginator->getTotalPages(),
                'key' => $key
            )
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $key = $request->request->get('key');
        $page = 1;

        return $this->dataResult($page, $key);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction()
    {

        $formHandler = $this->get('realestate_directionFacing_handler');

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'info',
                'admin.action.created',
                'admin.realEstate.directionFacing.created'
            );

            return $this->redirect(
                $this->generateUrl('realEstate_directionFacing_list')
            );
        }

        return $this->render(
            'KeirusRealEstateBundle:DirectionFacing:create.html.twig',
            array('form' => $formHandler->getForm()->createView())
        );
    }


    /**
     * @param $id
     * @return RedirectResponse|Response
     */
    public function updateAction($id)
    {

        $manager = $this->get('realestate_directionFacing_manager');
        $directionFacing = $manager->getDirectionFacing($id);

        $formHandler = $this->get('realestate_directionFacing_handler');
        $form = $this->createForm(new DirectionFacingForm(), $directionFacing);
        $formHandler->setForm($form);

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'success',
                'admin.action.updated',
                'admin.realEstate.directionFacing.updated'
            );

            return $this->redirect(
                $this->generateUrl('realEstate_directionFacing_list')
            );
        }

        return $this->render(
            'KeirusRealEstateBundle:DirectionFacing:update.html.twig',
            array(
                'form' => $formHandler->getForm()->createView(),
                'directionFacing' => $directionFacing
            )
        );
    }


    /**
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction($id)
    {
        $manager = $this->get('realestate_directionFacing_manager');
        $directionFacing = $manager->getDirectionFacing($id);

        if ($directionFacing instanceof DirectionFacing) {
            $manager->removeAndFlush($directionFacing);

            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.action.deleted',
                'admin.realEstate.directionFacing.deleted'
            );
        } else {
            throw new NotFoundHttpException("Direction Facing not found");
        }

        return $this->redirect($this->generateUrl('realEstate_directionFacing_list'));
    }


    /**
     * @param $id
     * @return Response
     * @throws EntityNotFoundException
     */
    public function showAction($id)
    {
        $manager = $this->get('realestate_directionFacing_manager');
        $directionFacing = $manager->getDirectionFacing($id);


        return $this->render('KeirusRealEstateBundle:DirectionFacing:show.html.twig', array('directionFacing' => $directionFacing));
    }
}