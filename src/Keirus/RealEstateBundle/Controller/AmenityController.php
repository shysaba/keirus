<?php
namespace Keirus\RealEstateBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Keirus\CoreBundle\Manager\BasePaginatorManager;
use Keirus\RealEstateBundle\Form\Type\AmenityForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keirus\RealEstateBundle\Entity\Amenity;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class AmenityController
 * @package Keirus\RealEstateBundle\Controller
 */
class AmenityController extends Controller
{

    /**
     * @return mixed
     */
    public function listAction($page, $key)
    {

        return $this->dataResult($page, $key);
    }

    /**
     * @param $page
     * @param $key
     * @return Response
     */
    private function dataResult($page, $key)
    {

        $manager = $this->get('realestate_amenity_manager');
        $rpp = $this->container->getParameter('elements_per_page');


        list($result, $totalCount) = $manager->getAllAmenitiesPaginated($page, $rpp, $key);

        $paginator = new BasePaginatorManager($page, $totalCount, $rpp);

        if ($totalCount < 1) {
            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.alert.info',
                'admin.search.noResults'
            );
        }

        return $this->render(
            'KeirusRealEstateBundle:Amenity:list.html.twig',
            array(
                'amenities' => $result,
                'paginator' => $paginator->getPagesList(),
                'cur' => $page,
                'total' => $paginator->getTotalPages(),
                'key' => $key
            )
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $key = $request->request->get('key');
        $page = 1;

        return $this->dataResult($page, $key);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction()
    {

        $formHandler = $this->get('realestate_amenity_handler');

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'info',
                'admin.action.created',
                'admin.realEstate.amenity.created'
            );

            return $this->redirect(
                $this->generateUrl('realEstate_amenity_list')
            );
        }

        return $this->render(
            'KeirusRealEstateBundle:Amenity:create.html.twig',
            array('form' => $formHandler->getForm()->createView())
        );
    }


    /**
     * @param $id
     * @return RedirectResponse|Response
     */
    public function updateAction($id)
    {

        $manager = $this->get('realestate_amenity_manager');
        $amenity = $manager->getAmenity($id);

        $formHandler = $this->get('realestate_amenity_handler');
        $form = $this->createForm(new AmenityForm(), $amenity);
        $formHandler->setForm($form);

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'success',
                'admin.action.updated',
                'admin.realEstate.amenity.updated'
            );

            return $this->redirect(
                $this->generateUrl('realEstate_amenity_list')
            );
        }

        return $this->render(
            'KeirusRealEstateBundle:Amenity:update.html.twig',
            array(
                'form' => $formHandler->getForm()->createView(),
                'amenity' => $amenity
            )
        );
    }


    /**
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction($id)
    {
        $manager = $this->get('realestate_amenity_manager');
        $amenity = $manager->getAmenity($id);

        if ($amenity instanceof Amenity) {
            $manager->removeAndFlush($amenity);

            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.action.deleted',
                'admin.realEstate.amenity.deleted'
            );
        } else {
            throw new NotFoundHttpException("Amenity not found");
        }

        return $this->redirect($this->generateUrl('realEstate_amenity_list'));
    }


    /**
     * @param $id
     * @return Response
     * @throws EntityNotFoundException
     */
    public function showAction($id)
    {
        $manager = $this->get('realestate_amenity_manager');
        $amenity = $manager->getAmenity($id);


        return $this->render('KeirusRealEstateBundle:Amenity:show.html.twig', array('amenity' => $amenity));
    }
}