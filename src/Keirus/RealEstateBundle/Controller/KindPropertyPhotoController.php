<?php
namespace Keirus\RealEstateBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Keirus\CoreBundle\Manager\BasePaginatorManager;
use Keirus\RealEstateBundle\Form\Type\KindPropertyPhotoForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keirus\RealEstateBundle\Entity\KindPropertyPhoto;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class KindPropertyPhotoController
 * @package Keirus\RealEstateBundle\Controller
 */
class KindPropertyPhotoController extends Controller
{

    /**
     * @return mixed
     */
    public function listAction($page, $key)
    {

        return $this->dataResult($page, $key);
    }

    /**
     * @param $page
     * @param $key
     * @return Response
     */
    private function dataResult($page, $key)
    {

        $manager = $this->get('realestate_kindPhoto_manager');
        $rpp = $this->container->getParameter('elements_per_page');


        list($result, $totalCount) = $manager->getAllKindsPhotoPaginated($page, $rpp, $key);

        $paginator = new BasePaginatorManager($page, $totalCount, $rpp);

        if ($totalCount < 1) {
            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.alert.info',
                'admin.search.noResults'
            );
        }

        return $this->render(
            'KeirusRealEstateBundle:KindPropertyPhoto:list.html.twig',
            array(
                'kindsPhoto' => $result,
                'paginator' => $paginator->getPagesList(),
                'cur' => $page,
                'total' => $paginator->getTotalPages(),
                'key' => $key
            )
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $key = $request->request->get('key');
        $page = 1;

        return $this->dataResult($page, $key);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction()
    {

        $formHandler = $this->get('realestate_kindPhoto_handler');

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'info',
                'admin.action.created',
                'admin.realEstate.kindPhoto.created'
            );

            return $this->redirect(
                $this->generateUrl('realEstate_kindPhoto_list')
            );
        }

        return $this->render(
            'KeirusRealEstateBundle:KindPropertyPhoto:create.html.twig',
            array('form' => $formHandler->getForm()->createView())
        );
    }


    /**
     * @param $id
     * @return RedirectResponse|Response
     */
    public function updateAction($id)
    {

        $manager = $this->get('realestate_kindPhoto_manager');
        $kindPhoto = $manager->getKindPropertyPhoto($id);

        $formHandler = $this->get('realestate_kindPhoto_handler');
        $form = $this->createForm(new KindPropertyPhotoForm(), $kindPhoto);
        $formHandler->setForm($form);

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'success',
                'admin.action.updated',
                'admin.realEstate.kindPhoto.updated'
            );

            return $this->redirect(
                $this->generateUrl('realEstate_kindPhoto_list')
            );
        }

        return $this->render(
            'KeirusRealEstateBundle:KindPropertyPhoto:update.html.twig',
            array(
                'form' => $formHandler->getForm()->createView(),
                'kindPhoto' => $kindPhoto
            )
        );
    }


    /**
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction($id)
    {
        $manager = $this->get('realestate_kindPhoto_manager');
        $kindPhoto = $manager->getKindPropertyPhoto($id);

        if ($kindPhoto instanceof KindPropertyPhoto) {
            $manager->removeAndFlush($kindPhoto);

            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.action.deleted',
                'admin.realEstate.kindPhoto.deleted'
            );
        } else {
            throw new NotFoundHttpException("Kind photo not found");
        }

        return $this->redirect($this->generateUrl('realEstate_kindPhoto_list'));
    }


    /**
     * @param $id
     * @return Response
     * @throws EntityNotFoundException
     */
    public function showAction($id)
    {
        $manager = $this->get('realestate_kindPhoto_manager');
        $kindPhoto = $manager->getKindPropertyPhoto($id);


        return $this->render('KeirusRealEstateBundle:KindPropertyPhoto:show.html.twig', array('kindPhoto' => $kindPhoto));
    }
}