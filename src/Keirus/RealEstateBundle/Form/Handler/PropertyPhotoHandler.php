<?php

namespace Keirus\RealEstateBundle\Form\Handler;


use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Keirus\CoreBundle\Form\Handler\BaseHandler;
use Keirus\RealEstateBundle\Manager\PropertyPhotoManager;

/**
 * Class PropertyPhotoHandler
 * @package Keirus\RealEstateBundle\Form\Handler
 */
class PropertyPhotoHandler extends BaseHandler
{


    /**
     * @var PropertyPhotoManager
     */
    protected $propertyPhoto;


    /**
     * @param Form $form
     * @param Request $request
     * @param SecurityContext $security
     * @param PropertyPhotoManager $propertyPhoto
     */
    public function __construct(
        Form $form,
        Request $request,
        SecurityContext $security,
        PropertyPhotoManager $propertyPhoto
    ) {
        parent::__construct($form, $request, $security);
        $this->propertyPhoto = $propertyPhoto;
    }


    /**
     *
     */
    protected function onSuccess()
    {
        $entity = $this->form->getData();

        $this->propertyPhoto->save($entity);
    }

}