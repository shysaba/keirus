<?php

namespace Keirus\RealEstateBundle\Form\Handler;


use Keirus\UserBundle\Entity\Broker;
use Keirus\UserBundle\Entity\Individual;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Keirus\CoreBundle\Form\Handler\BaseHandler;
use Keirus\RealEstateBundle\Manager\ApartmentManager;

/**
 * Class ApartmentHandler
 * @package Keirus\RealEstateBundle\Form\Handler
 */
class ApartmentHandler extends BaseHandler
{


    /**
     * @var ApartmentManager
     */
    protected $apartmentManager;


    /**
     * @param Form $form
     * @param Request $request
     * @param SecurityContext $security
     * @param ApartmentManager $apartmentManager
     */
    public function __construct(
        Form $form,
        Request $request,
        SecurityContext $security,
        ApartmentManager $apartmentManager
    ) {
        parent::__construct($form, $request, $security);
        $this->apartmentManager = $apartmentManager;
    }


    /**
     *
     */
    protected function onSuccess()
    {
        $entity = $this->form->getData();


        $user = $this->security->getToken()->getUser();

        if ($user instanceof Broker) {
            $entity->setBroker($user);
        } elseif ($user instanceof Individual) {
            $entity->setIndividual($user);
        } else {
            return;
        }
        
        $this->apartmentManager->save($entity);
    }

}