<?php

namespace Keirus\RealEstateBundle\Form\Handler;


use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Keirus\CoreBundle\Form\Handler\BaseHandler;
use Keirus\RealEstateBundle\Manager\AmenityCategoryManager;

/**
 * Class AmenityCategoryHandler
 * @package Keirus\RealEstateBundle\Form\Handler
 */
class AmenityCategoryHandler extends BaseHandler
{


    /**
     * @var AmenityCategoryManager
     */
    protected $amenityCategoryManager;


    /**
     * @param Form $form
     * @param Request $request
     * @param SecurityContext $security
     * @param AmenityCategoryManager $amenityCategoryManager
     */
    public function __construct(
        Form $form,
        Request $request,
        SecurityContext $security,
        AmenityCategoryManager $amenityCategoryManager
    ) {
        parent::__construct($form, $request, $security);
        $this->amenityCategoryManager = $amenityCategoryManager;
    }


    /**
     *
     */
    protected function onSuccess()
    {
        $entity = $this->form->getData();

        $this->amenityCategoryManager->save($entity);
    }

}