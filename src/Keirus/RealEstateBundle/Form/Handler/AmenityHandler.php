<?php

namespace Keirus\RealEstateBundle\Form\Handler;


use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Keirus\CoreBundle\Form\Handler\BaseHandler;
use Keirus\RealEstateBundle\Manager\AmenityManager;

/**
 * Class AmenityHandler
 * @package Keirus\RealEstateBundle\Form\Handler
 */
class AmenityHandler extends BaseHandler
{


    /**
     * @var AmenityManager
     */
    protected $amenityManager;


    /**
     * @param Form $form
     * @param Request $request
     * @param SecurityContext $security
     * @param AmenityManager $amenityManager
     */
    public function __construct(
        Form $form,
        Request $request,
        SecurityContext $security,
        AmenityManager $amenityManager
    ) {
        parent::__construct($form, $request, $security);
        $this->amenityManager = $amenityManager;
    }


    /**
     *
     */
    protected function onSuccess()
    {
        $entity = $this->form->getData();

        $this->amenityManager->save($entity);
    }

}