<?php

namespace Keirus\RealEstateBundle\Form\Handler;


use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Keirus\CoreBundle\Form\Handler\BaseHandler;
use Keirus\RealEstateBundle\Manager\DirectionFacingManager;

/**
 * Class DirectionFacingHandler
 * @package Keirus\RealEstateBundle\Form\Handler
 */
class DirectionFacingHandler extends BaseHandler
{


    /**
     * @var DirectionFacingManager
     */
    protected $directionFacingManager;


    /**
     * @param Form $form
     * @param Request $request
     * @param SecurityContext $security
     * @param DirectionFacingManager $directionFacingManager
     */
    public function __construct(
        Form $form,
        Request $request,
        SecurityContext $security,
        DirectionFacingManager $directionFacingManager
    ) {
        parent::__construct($form, $request, $security);
        $this->directionFacingManager = $directionFacingManager;
    }


    /**
     *
     */
    protected function onSuccess()
    {
        $entity = $this->form->getData();

        $this->directionFacingManager->save($entity);
    }

}