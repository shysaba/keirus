<?php

namespace Keirus\RealEstateBundle\Form\Handler;


use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Keirus\CoreBundle\Form\Handler\BaseHandler;
use Keirus\RealEstateBundle\Manager\OwnershipTypeManager;

/**
 * Class OwnershipTypeHandler
 * @package Keirus\RealEstateBundle\Form\Handler
 */
class OwnershipTypeHandler extends BaseHandler
{


    /**
     * @var OwnershipTypeManager
     */
    protected $ownershipTypeManager;


    /**
     * @param Form $form
     * @param Request $request
     * @param SecurityContext $security
     * @param OwnershipTypeManager $ownershipTypeManager
     */
    public function __construct(
        Form $form,
        Request $request,
        SecurityContext $security,
        OwnershipTypeManager $ownershipTypeManager
    ) {
        parent::__construct($form, $request, $security);
        $this->ownershipTypeManager = $ownershipTypeManager;
    }


    /**
     *
     */
    protected function onSuccess()
    {
        $entity = $this->form->getData();

        $this->ownershipTypeManager->save($entity);
    }

}