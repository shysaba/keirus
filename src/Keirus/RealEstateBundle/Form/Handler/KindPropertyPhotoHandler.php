<?php

namespace Keirus\RealEstateBundle\Form\Handler;


use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Keirus\CoreBundle\Form\Handler\BaseHandler;
use Keirus\RealEstateBundle\Manager\KindPropertyPhotoManager;

/**
 * Class KindPropertyPhotoHandler
 * @package Keirus\RealEstateBundle\Form\Handler
 */
class KindPropertyPhotoHandler extends BaseHandler
{


    /**
     * @var KindPropertyPhotoManager
     */
    protected $kindPropertyPhotoManager;


    /**
     * @param Form $form
     * @param Request $request
     * @param SecurityContext $security
     * @param KindPropertyPhotoManager $kindPropertyPhotoManager
     */
    public function __construct(
        Form $form,
        Request $request,
        SecurityContext $security,
        KindPropertyPhotoManager $kindPropertyPhotoManager
    ) {
        parent::__construct($form, $request, $security);
        $this->kindPropertyPhotoManager = $kindPropertyPhotoManager;
    }


    /**
     *
     */
    protected function onSuccess()
    {
        $entity = $this->form->getData();

        $this->kindPropertyPhotoManager->save($entity);
    }

}