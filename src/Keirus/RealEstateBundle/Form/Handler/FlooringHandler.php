<?php

namespace Keirus\RealEstateBundle\Form\Handler;


use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Keirus\CoreBundle\Form\Handler\BaseHandler;
use Keirus\RealEstateBundle\Manager\FlooringManager;

/**
 * Class FlooringHandler
 * @package Keirus\RealEstateBundle\Form\Handler
 */
class FlooringHandler extends BaseHandler
{


    /**
     * @var FlooringManager
     */
    protected $flooringManager;


    /**
     * @param Form $form
     * @param Request $request
     * @param SecurityContext $security
     * @param FlooringManager $flooringManager
     */
    public function __construct(
        Form $form,
        Request $request,
        SecurityContext $security,
        FlooringManager $flooringManager
    ) {
        parent::__construct($form, $request, $security);
        $this->flooringManager = $flooringManager;
    }


    /**
     *
     */
    protected function onSuccess()
    {
        $entity = $this->form->getData();

        $this->flooringManager->save($entity);
    }

}