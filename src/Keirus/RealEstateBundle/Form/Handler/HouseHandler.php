<?php

namespace Keirus\RealEstateBundle\Form\Handler;


use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Keirus\CoreBundle\Form\Handler\BaseHandler;
use Keirus\RealEstateBundle\Manager\HouseManager;

/**
 * Class HouseHandler
 * @package Keirus\RealEstateBundle\Form\Handler
 */
class HouseHandler extends BaseHandler
{


    /**
     * @var HouseManager
     */
    protected $houseManager;


    /**
     * @param Form $form
     * @param Request $request
     * @param SecurityContext $security
     * @param HouseManager $houseManager
     */
    public function __construct(
        Form $form,
        Request $request,
        SecurityContext $security,
        HouseManager $houseManager
    ) {
        parent::__construct($form, $request, $security);
        $this->houseManager = $houseManager;
    }


    /**
     *
     */
    protected function onSuccess()
    {
        $entity = $this->form->getData();

        $this->houseManager->save($entity);
    }

}