<?php

namespace Keirus\RealEstateBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class ApartmentForm
 * @package Keirus\RealEstateBundle\Form\Type
 */
class ApartmentForm extends PropertyForm
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('totalFloors', 'number', array('label' => 'admin.realEstate.apartment.totalFloors', 'error_bubbling' => true))
            ->add('propertyOn', 'number', array('label' => 'admin.realEstate.apartment.propertyOn', 'error_bubbling' => true));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'translation_domain' => 'admin',
                'data_class' => 'Keirus\RealEstateBundle\Entity\Apartment'
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'realestate_apartment_form';
    }
}