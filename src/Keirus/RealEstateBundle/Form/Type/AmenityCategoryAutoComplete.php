<?php

namespace Keirus\RealEstateBundle\Form\Type;

use Keirus\CoreBundle\Form\Type\BaseAutoComplete;
use Keirus\RealEstateBundle\Form\DataTransformer\AmenityCategoryTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\Common\Persistence\ObjectManager;


/**
 * Class AmenityCategoryAutoComplete
 * @package Keirus\RealEstateBundle\Form\Type
 */
class AmenityCategoryAutoComplete extends BaseAutoComplete
{


    /**
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        parent::__construct($objectManager);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $class = $options['class'];

        $transformer = new AmenityCategoryTransformer(parent::getObjectManager(), $class);
        $builder->addViewTransformer($transformer);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'autocomplete_category';
    }
}