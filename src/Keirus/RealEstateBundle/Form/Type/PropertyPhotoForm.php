<?php

namespace Keirus\RealEstateBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class PropertyPhotoForm
 * @package Keirus\RealEstateBundle\Form\Type
 */
class PropertyPhotoForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'kindPropertyPhoto',
                'entity',
                array(
                    'class' => 'KeirusRealEstateBundle:KindPropertyPhoto',
                    'property' => 'name',
                    'multiple' => false,
                    'label' => 'admin.realEstate.kindPhoto.name'
                )
            )
            ->add(
                'file',
                'file',
                array(
                    'label' => 'admin.realEstate.property.propertyPhoto',
                    'required' => false,
                    'image_path' => 'webPath'
                )
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'translation_domain' => 'admin',
                'data_class' => 'Keirus\RealEstateBundle\Entity\PropertyPhoto',
                'cascade_validation' => true
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'realestate_propertyphoto_form';
    }
}