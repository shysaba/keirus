<?php

namespace Keirus\RealEstateBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class PropertyForm
 * @package Keirus\RealEstateBundle\Form\Type
 */
class PropertyForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array('label' => 'admin.realEstate.property.title', 'error_bubbling' => true))
            ->add(
                'description',
                'textarea',
                array(
                    'label' => 'admin.realEstate.property.description',
                    'error_bubbling' => true,
                    'attr' => array('class' => 'textarea textareaHTML')
                )
            )
            ->add('address', 'text', array('label' => 'admin.realEstate.property.address', 'error_bubbling' => true))
            ->add(
                'city',
                'autocomplete_city',
                [
                    'label' => 'admin.realEstate.property.city',
                    'error_bubbling' => true,
                    'class' => 'KeirusLocalizationBundle:City',
                    'update_route' => 'localization_cities_list',
                    'required' => true,
                    'attr' => ['class' => 'form-control']
                ]
            )
            ->add(
                'sqftArea',
                'number',
                array('label' => 'admin.realEstate.property.sqftArea', 'error_bubbling' => true)
            )
            ->add('price', 'number', array('label' => 'admin.realEstate.property.price', 'error_bubbling' => true))
            ->add(
                'registrationCharges',
                'number',
                array('label' => 'admin.realEstate.property.registrationCharges', 'error_bubbling' => true)
            )
            ->add(
                'noBathrooms',
                'number',
                array('label' => 'admin.realEstate.property.noBathrooms', 'error_bubbling' => true)
            )
            ->add('noRooms', 'number', array('label' => 'admin.realEstate.property.noRooms', 'error_bubbling' => true))
            ->add(
                'directionFacing',
                'entity',
                array(
                    'class' => 'KeirusRealEstateBundle:DirectionFacing',
                    'property' => 'name',
                    'multiple' => false,
                    'label' => 'admin.realEstate.property.directionFacing',
                    'error_bubbling' => true
                )
            )
            ->add(
                'flooring',
                'entity',
                array(
                    'class' => 'KeirusRealEstateBundle:Flooring',
                    'property' => 'name',
                    'multiple' => false,
                    'label' => 'admin.realEstate.property.flooring',
                    'error_bubbling' => true
                )
            )
            ->add(
                'ownershipType',
                'entity',
                array(
                    'class' => 'KeirusRealEstateBundle:OwnershipType',
                    'property' => 'type',
                    'multiple' => false,
                    'label' => 'admin.realEstate.property.ownershipType',
                    'error_bubbling' => true
                )
            )
            ->add(
                'brokerageTerms',
                'textarea',
                array('label' => 'admin.realEstate.property.brokerageTerms', 'error_bubbling' => true)
            )
            ->add(
                'possessionFrom',
                'date',
                array('label' => 'admin.realEstate.property.possessionFrom', 'error_bubbling' => true)
            )
            ->add(
                'yearConstruction',
                'date',
                array('label' => 'admin.realEstate.property.yearConstruction', 'error_bubbling' => true)
            )
            ->add(
                'amenities',
                'entity',
                array(
                    'class' => 'KeirusRealEstateBundle:Amenity',
                    'property' => 'name',
                    'multiple' => true,
                    'label' => 'admin.realEstate.property.amenities',
                    'error_bubbling' => true
                )
            )
            ->add(
                'propertyPhotos',
                'collection',
                [
                    'label' => 'admin.realEstate.property.propertyPhotos',
                    'error_bubbling' => true,
                    'required' => false,
                    'attr' => ['class' => 'col-sm-8'],
                    'type' => new PropertyPhotoForm(),
                    'allow_add' => true,
                    'by_reference' => false,
                    'allow_delete' => true
                ]
            )
            ->add('submit', 'submit');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'translation_domain' => 'admin',
                'data_class' => 'Keirus\RealEstateBundle\Entity\Property'
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'realestate_property_form';
    }
}