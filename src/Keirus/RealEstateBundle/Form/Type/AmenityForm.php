<?php

namespace Keirus\RealEstateBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class AmenityForm
 * @package Keirus\RealEstateBundle\Form\Type
 */
class AmenityForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('label' => 'admin.realEstate.amenity.name', 'error_bubbling' => true))
            ->add('icon', 'text', array('label' => 'admin.realEstate.amenity.icon', 'error_bubbling' => true))
            ->add(
                'category',
                'autocomplete_category',
                [
                    'label' => 'admin.realEstate.amenity.category',
                    'error_bubbling' => true,
                    'class' => 'KeirusRealEstateBundle:AmenityCategory',
                    'update_route' => 'realEstate_amenityCategories_list',
                    'required' => true,
                    'attr' => ['class' => 'form-control']
                ]
            )
            ->add('submit', 'submit');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'translation_domain' => 'admin',
                'data_class' => 'Keirus\RealEstateBundle\Entity\Amenity'
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'realestate_amenity_form';
    }
}