<?php

namespace Keirus\RealEstateBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class DirectionFacingForm
 * @package Keirus\RealEstateBundle\Form\Type
 */
class DirectionFacingForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('label' => 'admin.realEstate.directionFacing.name', 'error_bubbling' => true))
            ->add('submit', 'submit');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'translation_domain' => 'admin',
                'data_class' => 'Keirus\RealEstateBundle\Entity\DirectionFacing'
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'realestate_directionFacing_form';
    }
}