<?php

namespace Keirus\RealEstateBundle\Form\DataTransformer;

use Keirus\CoreBundle\Form\DataTransformer\BaseTransformer;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class AmenityCategoryTransformer
 * @package Keirus\RealEstateBundle\Form\DataTransformer
 */
class AmenityCategoryTransformer extends BaseTransformer
{

    /**
     * @param ObjectManager $objectManager
     * @param $entityClass
     */
    public function __construct(ObjectManager $objectManager, $entityClass)
    {
        parent::__construct($objectManager, $entityClass);
    }

}