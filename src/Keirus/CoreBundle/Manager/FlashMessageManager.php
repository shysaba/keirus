<?php
namespace Keirus\CoreBundle\Manager;


use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Translation\TranslatorInterface as Translator;

/**
 * Class FlashMessageManager
 * @package Keirus\CoreBundle\Manager
 */
class FlashMessageManager
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * @var Translator
     */
    protected $translator;

    /**
     * @var
     */
    protected $message;


    /**
     * @param Session $session
     * @param Translator $translator
     */
    public function __construct(Session $session, Translator $translator)
    {
        $this->session = $session;
        $this->translator = $translator;
    }


    /**
     * @param Session $message
     */
    public function setMessage($alert, $title, $message)
    {
        $this->session->getFlashBag()->add(
            'notice',
            array(
                'alert' => $alert,
                'title' => $this->getTranslator($title),
                'message' => $this->getTranslator($message)
            )
        );
    }

    /**
     * @return Translator
     */
    public function getTranslator($value)
    {
        return $this->translator->trans($value, array(), 'admin', 'fr');
    }


}