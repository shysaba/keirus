<?php

namespace Keirus\CoreBundle\Manager;


/**
 * Class BasePaginatorManager
 * @package Keirus\CoreBundle\Manager
 */
class BasePaginatorManager
{
    /**
     * @var float
     */
    private $totalPages;
    /**
     * @var
     */
    private $page;
    /**
     * @var
     */
    private $rpp;

    /**
     * @param $page
     * @param totalCount
     * @param $rpp
     */
    public function __construct($page, $totalCount, $rpp)
    {
        $this->rpp = $rpp;
        $this->page = $page;

        $this->totalPages = $this->setTotalPages($totalCount, $rpp);
    }


    /**
     * @param $totalCount : the total count of records
     * @param $rpp : the record per page
     * @return float
     */
    private function setTotalPages($totalCount, $rpp)
    {
        if ($rpp == 0) {
            $rpp = 20; // In case we did not provide a number for $rpp
        }

        $this->totalPages = ceil($totalCount / $rpp);

        return $this->totalPages;
    }

    /**
     * @return float
     */
    public function getTotalPages()
    {
        return $this->totalPages;
    }

    /**
     * @return array
     */
    public function getPagesList()
    {
        $pageCount = 5;
        if ($this->totalPages <= $pageCount) //Less than total 5 pages
        {
            return array(1, 2, 3, 4, 5);
        }

        if ($this->page <= 3) {
            return array(1, 2, 3, 4, 5);
        }

        $i = $pageCount;
        $r = array();
        $half = floor($pageCount / 2);
        if ($this->page + $half > $this->totalPages) // Close to end
        {
            while ($i >= 1) {
                $r[] = $this->totalPages - $i + 1;
                $i--;
            }

            return $r;
        } else {
            while ($i >= 1) {
                $r[] = $this->page - $i + $half + 1;
                $i--;
            }

            return $r;
        }
    }

}