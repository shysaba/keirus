<?php
namespace Keirus\CoreBundle\Manager;


use Monolog\Logger;

/**
 * Class LoggerManager
 * @package Keirus\CoreBundle\Manager
 */
class LoggerManager
{
    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     *
     */
    public function write()
    {

        $this->logger->info('Je fais quelque chose');
    }
}