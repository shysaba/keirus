<?php
namespace Keirus\CoreBundle\Manager;


/**
 * Class BaseManager
 * @package Keirus\CoreBundle\Entity\Manager
 * @author shysaba
 */
abstract class BaseManager
{

    /**
     * Remove and flush an entity
     * @param $entity
     */
    public function removeAndFlush($entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }

    /**
     * Remove an entity
     */
    public function remove($entity)
    {
        $this->em->remove($entity);
    }

    /**
     * Persist an entity
     * @param $entity
     */
    public function persist($entity)
    {
        $this->em->persist($entity);
    }

    /**
     * Flush an entity manager
     */
    public function flush()
    {
        $this->em->flush();
    }

    /**
     * Persist and flush an entity
     * @param $entity
     */
    protected function persistAndFlush($entity)
    {
        $this->em->persist($entity);
        $this->em->flush($entity);

        return $entity;
    }

}