<?php
namespace Keirus\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Keirus\UserBundle\Entity\Role;

/**
 * Class LoadRoleData
 * @package Keirus\CoreBundle\DataFixtures\ORM
 */
class LoadRoleData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $superAdmin = new Role();
        $superAdmin->setName('ROLE_SUPERADMIN');
        $superAdmin->setLabel('Super administrator');
        $superAdmin->setParent(null);

        $this->addReference('ROLE_SUPERADMIN', $superAdmin);
        $manager->persist($superAdmin);


        $underSuperAdmin = array(
            ['ROLE_ADMIN', 'ROLE_ADMIN', 'Administrator'],
            ['ROLE_ALLOWED_TO_SWITCH', 'ROLE_ALLOWED_TO_SWITCH', 'Allowed to switch'],
        );


        foreach ($underSuperAdmin as list($reference, $role, $label)) {
            $role_underSuperAdmin = new Role();
            $role_underSuperAdmin->setName($role);
            $role_underSuperAdmin->setLabel($label);
            $role_underSuperAdmin->setParent($this->getReference('ROLE_SUPERADMIN'));

            $this->addReference($reference, $role_underSuperAdmin);
            $manager->persist($role_underSuperAdmin);
        }


        $underAdmin = array(
            ['ROLE_USER', 'ROLE_USER', 'User'],
            ['ROLE_MODERATOR', 'ROLE_MODERATOR', 'Moderator'],
        );

        foreach ($underAdmin as list($reference, $role, $label)) {
            $role_underAdmin = new Role();
            $role_underAdmin->setName($role);
            $role_underAdmin->setLabel($label);
            $role_underAdmin->setParent($this->getReference('ROLE_ADMIN'));

            $this->addReference($reference, $role_underAdmin);
            $manager->persist($role_underAdmin);
        }


        $underUser = array(
            ['ROLE_BROKER', 'ROLE_BROKER', 'Broker'],
            ['ROLE_BUILDER', 'ROLE_BUILDER', 'Builder'],
            ['ROLE_INDIVIDUAL', 'ROLE_INDIVIDUAL', 'Individual'],
            ['ROLE_VISITOR', 'ROLE_VISITOR', 'Tourist'],
        );

        foreach ($underUser as list($reference, $role, $label)) {
            $role_underUser = new Role();
            $role_underUser->setName($role);
            $role_underUser->setLabel($label);
            $role_underUser->setParent($this->getReference('ROLE_USER'));

            $this->addReference($reference, $role_underUser);
            $manager->persist($role_underUser);
        }


        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 10;
    }
}