<?php
namespace Keirus\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Keirus\UserBundle\Entity\Language;

/**
 * Class LoadLanguageData
 * @package Keirus\CoreBundle\DataFixtures\ORM
 */
class LoadLanguageData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $languages = array(
            ['LANG_FR', 'Français', 'FR'],
            ['LANG_EN', 'Anglais', 'EN'],
            ['LANG_HI', 'Hindi', 'HI'],
        );


        foreach ($languages as list($reference, $name, $label)) {
            $language = new Language();
            $language->setLabel($label);
            $language->setName($name);

            $this->addReference($reference, $language);
            $manager->persist($language);
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 6;
    }
}