<?php
namespace Keirus\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Keirus\HousingStockBundle\Entity\ProjectStatus;

/**
 * Class LoadProjectStatusData
 * @package Keirus\CoreBundle\DataFixtures\ORM
 */
class LoadProjectStatusData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $projectsStatus = array(
            ['PROJECT_STATUS_1', 'In project', '1'],
            ['PROJECT_STATUS_2', 'Under Construction', '2'],
            ['PROJECT_STATUS_3', 'New launch', '3'],
            ['PROJECT_STATUS_4', 'Ready to Move', '4'],
            ['PROJECT_STATUS_5', '0-1 Year Old Property', '5'],
            ['PROJECT_STATUS_6', '2-5 Year Old Property', '6'],
        );


        foreach ($projectsStatus as list($reference, $status, $ranking)) {
            $projectStatus = new ProjectStatus();
            $projectStatus->setName($status);
            $projectStatus->setRanking($ranking);

            $this->addReference($reference, $projectStatus);
            $manager->persist($projectStatus);
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 8;
    }
}