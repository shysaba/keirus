<?php
namespace Keirus\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Keirus\HousingStockBundle\Entity\UnitType;

/**
 * Class LoadUnitTypeData
 * @package Keirus\CoreBundle\DataFixtures\ORM
 */
class LoadUnitTypeData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $unitTypes = array(
            ['1_BHK', '1 BHK'],
            ['2_BHK', '2 BHK'],
            ['3_BHK', '3 BHK'],
            ['4_BHK', '4 BHK'],
            ['5_BHK', '5 BHK'],
        );


        foreach ($unitTypes as list($reference, $name)) {
            $unitType = new UnitType();
            $unitType->setType($name);

            $this->addReference($reference, $unitType);
            $manager->persist($unitType);
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 11;
    }
}