<?php
namespace Keirus\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Keirus\UserBundle\Entity\Administrator;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadAdministratorData
 * @package Keirus\CoreBundle\DataFixtures\ORM
 */
class LoadAdministratorData extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface, ContainerAwareInterface
{


    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {


        $admin = new Administrator();
        $admin->setFirstName('Administrator');
        $admin->setLastName('Simple');
        $admin->setUsername('admin');
        $admin->setEmail($this->container->getParameter('administrator_email'));
        $admin->setEnabled(true);
        $admin->setOptInMailing(false);
        $admin->setPreferedLanguage($this->getReference('LANG_FR'));

        $encoder = $this->container
            ->get('security.encoder_factory')
            ->getEncoder($admin);

        $admin->setPassword($encoder->encodePassword('12101989', $admin->getSalt()));

        $manager->persist($admin);
        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 15;
    }
}