<?php
namespace Keirus\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Keirus\UserBundle\Entity\Visitor;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadVisitorData
 * @package Keirus\CoreBundle\DataFixtures\ORM
 */
class LoadVisitorData extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface, ContainerAwareInterface
{


    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {


        $visitor = new Visitor();
        $visitor->setFirstName('Shyam');
        $visitor->setLastName('SABAPATHI');
        $visitor->setUsername('visitor_demo');

        $visitor->setEmail('visitor@demo.com');
        $visitor->setCitySearch($this->getReference('BANGALORE'));
        $visitor->setNri(true);
        $visitor->setEnabled(true);
        $visitor->setOptInMailing(true);
        $visitor->setPreferedLanguage($this->getReference('LANG_FR'));

        $encoder = $this->container
            ->get('security.encoder_factory')
            ->getEncoder($visitor);

        $visitor->setPassword($encoder->encodePassword('12101989', $visitor->getSalt()));

        $manager->persist($visitor);
        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 17;
    }
}