<?php
namespace Keirus\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Keirus\RealEstateBundle\Entity\OwnershipType;

/**
 * Class LoadOwnerShipTypeData
 * @package Keirus\CoreBundle\DataFixtures\ORM
 */
class LoadOwnerShipTypeData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $ownershipTypes = array(
            ['FREEHOLD', 'Freehold'],
            ['LEASEHOLD', 'Leasehold'],
        );


        foreach ($ownershipTypes as list($reference, $name)) {
            $ownershipType = new OwnershipType();
            $ownershipType->setType($name);

            $this->addReference($reference, $ownershipType);
            $manager->persist($ownershipType);
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 7;
    }
}