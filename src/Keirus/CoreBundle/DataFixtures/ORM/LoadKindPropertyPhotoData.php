<?php
namespace Keirus\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Keirus\RealEstateBundle\Entity\KindPropertyPhoto;

/**
 * Class LoadKindPropertyPhotoData
 * @package Keirus\CoreBundle\DataFixtures\ORM
 */
class LoadKindPropertyPhotoData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $kindPhotos = array(
            ['KIND_BUILDING', 'Building'],
            ['KIND_ROOM', 'Room'],
            ['KIND_KITCHEN', 'Kitchen'],
            ['KIND_FLOOR_PLAN', 'Floor plan'],
            ['KIND_MASTER_PLAN', 'Master plan'],
            ['KIND_DIRECTION', 'Direction'],
            ['KIND_LOCALITY', 'Locality'],
            ['KIND_CONSTRUCTION_STATUS', 'Construction Status'],
        );


        foreach ($kindPhotos as list($reference, $name)) {
            $kindPhoto = new KindPropertyPhoto();
            $kindPhoto->setName($name);

            $this->addReference($reference, $kindPhoto);
            $manager->persist($kindPhoto);
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 5;
    }
}