<?php
namespace Keirus\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Keirus\RealEstateBundle\Entity\DirectionFacing;

/**
 * Class LoadDirectionFacingData
 * @package Keirus\CoreBundle\DataFixtures\ORM
 */
class LoadDirectionFacingData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $directions = array(
            ['DIRECTION_NORTH', 'North'],
            ['DIRECTION_SOUTH', 'South'],
            ['DIRECTION_WEST', 'West'],
            ['DIRECTION_NORTHEAST', 'Northeast'],
            ['DIRECTION_SOUTHEAST', 'Southeast'],
            ['DIRECTION_SOUTHWEST', 'Southwest'],
            ['DIRECTION_NORTHWEST', 'Northwest'],
        );

        foreach ($directions as list($reference, $name)) {
            $direction = new DirectionFacing();
            $direction->setName($name);

            $this->addReference($reference, $direction);
            $manager->persist($direction);
        }

        $manager->flush();


    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 3;
    }
}