<?php
namespace Keirus\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Keirus\HousingStockBundle\Entity\PropertyType;

/**
 * Class LoadPropertyTypeData
 * @package Keirus\CoreBundle\DataFixtures\ORM
 */
class LoadPropertyTypeData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $propertyTypes = array(
            ['LOCAL_COMMERCIAL', 'Local commercial'],
            ['RESIDENTIAL_APARTMENT', 'Residential Apartment'],
            ['INDEPENDENT_FLOOR', 'Independent/Builder Floor'],
            ['INDEPENDENT_VILLA', 'Independent House/Villa'],
            ['RESIDENTIAL_LAND', 'Residential Land'],
            ['STUDIO_APARTMENT', 'Studio Apartment'],
            ['FARM_HOUSE', 'Farm House'],
            ['SERVICES_APARTMENTS', 'Serviced Apartments'],
            ['OTHER_PROPERTY_TYPE', 'Other'],
        );


        foreach ($propertyTypes as list($reference, $type)) {
            $propertyType = new PropertyType();
            $propertyType->setType($type);

            $this->addReference($reference, $propertyType);
            $manager->persist($propertyType);
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 9;
    }
}