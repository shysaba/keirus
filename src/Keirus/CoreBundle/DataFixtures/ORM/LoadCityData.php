<?php
namespace Keirus\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Keirus\LocalizationBundle\Entity\City;

/**
 * Class LoadCityData
 * @package Keirus\CoreBundle\DataFixtures\ORM
 */
class LoadCityData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $cities = array(
            ['KARIKAL', 'Karaikal', '609602', 'TN'],
            ['CHENNAI', 'Chennai', '600001', 'TN'],
            ['BANGALORE', 'Bangalore', '560300', 'KA'],
        );


        foreach ($cities as list($reference, $name, $pinCode, $state)) {
            $city = new City();
            $city->setName($name);
            $city->setPinCode($pinCode);
            $city->setState($this->getReference($state));

            $this->addReference($reference, $city);
            $manager->persist($city);
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 13;
    }
}