<?php
namespace Keirus\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Keirus\UserBundle\Entity\Individual;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadIndividualData
 * @package Keirus\CoreBundle\DataFixtures\ORM
 */
class LoadIndividualData extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface, ContainerAwareInterface
{


    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {


        $individual = new Individual();
        $individual->setFirstName('Patrick');
        $individual->setLastName('Ganapathy');
        $individual->setUsername('individual_demo');
        $individual->setEmail('individual@demo.com');
        $individual->setEnabled(true);
        $individual->setOptInMailing(true);
        $individual->setPreferedLanguage($this->getReference('LANG_FR'));

        $encoder = $this->container
            ->get('security.encoder_factory')
            ->getEncoder($individual);

        $individual->setPassword($encoder->encodePassword('12101989', $individual->getSalt()));

        $manager->persist($individual);
        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 18;
    }
}