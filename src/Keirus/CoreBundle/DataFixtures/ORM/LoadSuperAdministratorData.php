<?php
namespace Keirus\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Keirus\UserBundle\Entity\SuperAdministrator;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadSuperAdministratorData
 * @package Keirus\CoreBundle\DataFixtures\ORM
 */
class LoadSuperAdministratorData extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface, ContainerAwareInterface
{


    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {


        $superAdmin = new SuperAdministrator();
        $superAdmin->setFirstName('Administrator');
        $superAdmin->setLastName('Super');
        $superAdmin->setUsername('superadmin');
        $superAdmin->setEmail($this->container->getParameter('super_administrator_email'));
        $superAdmin->setEnabled(true);
        $superAdmin->setOptInMailing(false);
        $superAdmin->setPreferedLanguage($this->getReference('LANG_FR'));

        $encoder = $this->container
            ->get('security.encoder_factory')
            ->getEncoder($superAdmin);

        $superAdmin->setPassword($encoder->encodePassword('12101989', $superAdmin->getSalt()));

        $manager->persist($superAdmin);
        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 14;
    }
}