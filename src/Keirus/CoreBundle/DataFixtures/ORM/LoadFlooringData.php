<?php
namespace Keirus\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Keirus\RealEstateBundle\Entity\Flooring;

/**
 * Class LoadFlooringData
 * @package Keirus\CoreBundle\DataFixtures\ORM
 */
class LoadFlooringData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $floorings = array(
            ['VITR_GLOSSY', 'Vitrified Glossy'],
            ['VITR_MATT', 'Vitrified Matt'],
            ['VITR_POLISH', 'Vitrified Polished'],
            ['VITR_RUSTIC', 'Vitrified Rustic'],
            ['VITR_TEXTURED', 'Vitrified Textured'],
            ['VITR_UNPOLISH', 'Vitrified Unpolished'],
            ['VITR_SATIN_MATT', 'Vitrified Satin Matt'],
            ['VITR_LAPATO', 'Vitrified Lapato'],
            ['CERAMIC_GLOSSY', 'Ceramic Glossy'],
            ['CERAMIC_MATT', 'Ceramic Matt'],
            ['CERAMIC_POLISHED', 'Ceramic Polished'],
            ['CERAMIC_TEXTURED', 'Ceramic Textured'],
            ['CERAMIC_MATT_RUSTIC', 'Ceramic Matt & Rustic'],
        );


        foreach ($floorings as list($reference, $name)) {
            $flooring = new Flooring();
            $flooring->setName($name);

            $this->addReference($reference, $flooring);
            $manager->persist($flooring);
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 4;
    }
}