<?php
namespace Keirus\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Keirus\RealEstateBundle\Entity\Amenity;

/**
 * Class LoadAmenityData
 * @package Keirus\CoreBundle\DataFixtures\ORM
 */
class LoadAmenityData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $amenities = array(
            ['POWER_BACK_UP', 'Power Back-up', 'HOME_FEATURES', 'bolt'],
            ['FIRE_ALARM', 'Security / Fire Alarm', 'HOME_FEATURES', 'bell'],
            ['AIR_COND', 'Centrally Air Conditioned', 'HOME_FEATURES', 'cloud'],
            ['PRIVATE_GARDER', 'Private Garden / Terrace', 'HOME_FEATURES', 'child'],
            ['INTERCOM', 'Intercom Facility', 'HOME_FEATURES', 'qrcode'],
            ['LIFTS', 'Lift(s)', 'HOME_FEATURES', 'caret-square-o-up'],
            ['WATER_PURIFIER', 'Water purifier', 'HOME_FEATURES', 'circle'],
            ['RESERVED_PARKING', 'Reserved Parking', 'HOME_FEATURES', 'car'],
            ['WATER_STORAGE', 'Water Storage', 'SOC_BUILD_FEATURES', 'circle'],
            ['PIPED_GAS', 'Piped-gas', 'SOC_BUILD_FEATURES', 'circle'],
            ['WIFI_CON', 'Internet/wi-fi connectivity', 'SOC_BUILD_FEATURES', 'wifi'],
            ['VISITOR_PARKING', 'Visitor Parking', 'SOC_BUILD_FEATURES', 'cab'],
            ['SWIMMING_POOL', 'Swimming Pool', 'SOC_BUILD_FEATURES', 'life-ring'],
            ['PARK', 'Park', 'SOC_BUILD_FEATURES', 'soccer-ball-o'],
            ['MAINTENANCE_STAFF', 'Maintenance Staff', 'SOC_BUILD_FEATURES', 'gears'],
            ['BANK_ATTACHED', 'Bank Attached Property', 'SOC_BUILD_FEATURES', 'bank'],
            ['VIDEO_SECURITY', 'Video Security', 'SOC_BUILD_FEATURES', 'video-camera'],
            ['DISABLED_ACCESS', 'Disabled accessibility', 'SOC_BUILD_FEATURES', 'wheelchair'],
            ['FITNESS_GYM', 'Fitness Centre / GYM', 'OTHER_FEATURES', 'heart'],
            ['CLUB_HOUSE', 'Club house / Community Center', 'OTHER_FEATURES', 'users'],
            ['WATER_PLANT', 'Water softening plant', 'OTHER_FEATURES', 'circle'],
            ['SHOPPING_CENTRE', 'Shopping Centre', 'OTHER_FEATURES', 'shopping-cart'],
            ['RAIN_WATER', 'Rain Water Harvesting', 'OTHER_FEATURES', 'umbrella'],
        );

        foreach ($amenities as list($reference, $name, $category, $icon)) {
            $amenity = new Amenity();
            $amenity->setName($name);
            $amenity->setIcon($icon);
            $amenity->setCategory($this->getReference($category));

            $this->addReference($reference, $amenity);
            $manager->persist($amenity);
        }

        $manager->flush();


    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 2;
    }
}