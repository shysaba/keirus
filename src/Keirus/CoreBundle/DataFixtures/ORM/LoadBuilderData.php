<?php
namespace Keirus\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Keirus\UserBundle\Entity\Builder;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadBuilderData
 * @package Keirus\CoreBundle\DataFixtures\ORM
 */
class LoadBuilderData extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface, ContainerAwareInterface
{


    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {


        $builder = new Builder();
        $builder->setFirstName('Sinmar');
        $builder->setLastName('Verona');
        $builder->setUsername('builder_demo');
        $builder->setName('Oxoniya P Limited');
        $builder->setEmail('builder@demo.com');
        $builder->setEnabled(true);
        $builder->setOptInMailing(true);
        $builder->setPreferedLanguage($this->getReference('LANG_FR'));
        $builder->setAddress('44/3496A, Elizabeth Enclave, Deshabhimani Road');
        $builder->setCity($this->getReference('BANGALORE'));
        $builder->setContactEmail('builder.bangalore@demo.com');
        $builder->setContactPhone('0484-2536661');
        $builder->setContactMobile('09633170707');
        $builder->setProfile('Oxoniya P Limited Profile');

        $encoder = $this->container
            ->get('security.encoder_factory')
            ->getEncoder($builder);

        $builder->setPassword($encoder->encodePassword('12101989', $builder->getSalt()));

        $manager->persist($builder);
        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 16;
    }
}