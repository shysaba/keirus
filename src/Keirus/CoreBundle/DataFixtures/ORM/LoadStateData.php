<?php
namespace Keirus\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Keirus\LocalizationBundle\Entity\State;

/**
 * Class LoadStateData
 * @package Keirus\CoreBundle\DataFixtures\ORM
 */
class LoadStateData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $states = array(
            ['Andhra Pradesh', 'AP'],
            ['Arunachal Pradesh', 'AR'],
            ['Assam', 'AS'],
            ['Bihar', 'BR'],
            ['Chhattisgarh', 'CT'],
            ['Goa', 'GA'],
            ['Gujarat', 'GJ'],
            ['Haryana', 'HR'],
            ['Himachal Pradesh', 'HP'],
            ['Jammu and Kashmir', 'JK'],
            ['Jharkhand', 'JH'],
            ['Karnataka', 'KA'],
            ['Kerala', 'KL'],
            ['Madhya Pradesh', 'MP'],
            ['Maharashtra', 'MH'],
            ['Manipur', 'MN'],
            ['Meghalaya', 'ML'],
            ['Mizoram', 'MZ'],
            ['Nagaland', 'NL'],
            ['Odisha', 'OR'],
            ['Punjab', 'PB'],
            ['Rajasthan', 'RJ'],
            ['Sikkim', 'SK'],
            ['Tamil Nadu', 'TN'],
            ['Telangana', 'TG'],
            ['Tripura', 'TR'],
            ['Uttar Pradesh', 'UP'],
            ['Uttarakhand', 'UT'],
            ['West Bengal', 'WB'],
            ['Andaman and Nicobar Islands', 'AN'],
            ['Chandigarh', 'CH'],
            ['Dadra and Nagar Haveli', 'DN'],
            ['Daman and Diu', 'DD'],
            ['Lakshadweep', 'LD'],
            ['National Capital Territory of Delhi', 'DL'],
            ['Puducherry', 'PY']
        );


        foreach ($states as list($name, $code)) {
            $state = new State();
            $state->setName($name);
            $state->setCode($code);

            $this->addReference($code, $state);
            $manager->persist($state);
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 12;
    }
}