<?php
namespace Keirus\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Keirus\RealEstateBundle\Entity\AmenityCategory;

/**
 * Class LoadAmenityCategoryData
 * @package Keirus\CoreBundle\DataFixtures\ORM
 */
class LoadAmenityCategoryData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $amenityCategories = array(
            ['HOME_FEATURES', 'Home Features'],
            ['SOC_BUILD_FEATURES', 'Society/ Building Features'],
            ['OTHER_FEATURES', 'Other Features'],
        );

        foreach ($amenityCategories as list($reference, $name)) {
            $amenityCategory = new AmenityCategory();
            $amenityCategory->setName($name);
            $this->addReference($reference, $amenityCategory);
            $manager->persist($amenityCategory);
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 1;
    }
}