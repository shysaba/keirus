<?php
namespace Keirus\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Keirus\UserBundle\Entity\Broker;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadBrokerData
 * @package Keirus\CoreBundle\DataFixtures\ORM
 */
class LoadBrokerData extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface, ContainerAwareInterface
{


    /**
     * @var ContainerInterface
     */
    private $container;


    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {


        $broker = new Broker();
        $broker->setFirstName('Samath');
        $broker->setLastName('Rao');
        $broker->setUsername('broker_demo');

        $broker->setPhone('0422-2587576561');
        $broker->setAgreementNo('Z769-TP-1596');
        $broker->setVerifiedAccount(true);

        $broker->setEmail('broker@demo.com');
        $broker->setEnabled(true);
        $broker->setOptInMailing(true);
        $broker->setPreferedLanguage($this->getReference('LANG_FR'));

        $encoder = $this->container
            ->get('security.encoder_factory')
            ->getEncoder($broker);

        $broker->setPassword($encoder->encodePassword('12101989', $broker->getSalt()));

        $manager->persist($broker);
        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 19;
    }
}