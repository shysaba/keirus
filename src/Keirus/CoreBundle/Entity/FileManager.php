<?php

namespace Keirus\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
abstract class FileManager
{

    /**
     * @return mixed
     */
    abstract protected function getUploadDir();

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    private $path;

    /**
     * @var UploadedFile file
     * @Assert\File(maxSize="16000000")
     */
    public $file;
    /**
     * @var
     */
    public $extension;
    /**
     * @var
     */
    private $tempFilename;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }


    /**
     * @param File $file
     */
    public function setFile(File $file)
    {
        $this->file = $file;
        $this->extension = $file->guessExtension();

        if (null !== $this->path) {
            $this->tempFilename = $this->path;
        }
    }


    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    final public function preUpload()
    {
        if (null === $this->file) {
            return;
        }

        $this->setPath(uniqid().'.'.$this->extension);
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    final public function upload()
    {

        if (null === $this->file) {
            return;
        }

        if (null !== $this->tempFilename) {

            $oldFile = $this->getUploadRootDir().'/'.$this->tempFilename;

            if (file_exists($oldFile)) {
                unlink($oldFile);
            }

        }

        $this->file->move($this->getUploadRootDir(), $this->getPath());
    }


    /**
     * @ORM\PreRemove()
     */
    public function preRemoveUpload()
    {
        $this->tempFilename = $this->getUploadRootDir().'/'.$this->path;
    }

    /**
     * @ORM\PostRemove()
     */
    final public function removeUpload()
    {
        if (file_exists($this->tempFilename)) {
            unlink($this->tempFilename);
        }
    }


    /**
     * @return null|string
     */
    public function getAbsolutePath()
    {
        return null === $this->getPath() ? null : $this->getUploadRootDir().'/'.$this->getPath();
    }

    /**
     * @return null|string
     */
    public function getWebPath()
    {
        return null === $this->getPath() ? null : $this->getUploadDir().'/'.$this->getPath();
    }

    /**
     * @return string
     */
    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

}