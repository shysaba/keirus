<?php

namespace Keirus\CoreBundle\Twig;

/**
 * Class RupeeExtension
 * @package Keirus\CoreBundle\Twig
 */
class RupeeExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('priceRupee', array($this, 'priceFilter')),
        );
    }

    /**
     * PriceFilter function
     * @param $amount
     * @return string
     */
    public function priceFilter($amount)
    {
        if (strlen($amount) >= 6) {
            $priceFiltered = sprintf('₹ %2d.%2d Lack', ($amount / 100000), ($amount / 1000 % 100));
        } else {
            $format = number_format($amount, 0, ',', ',');
            $priceFiltered = sprintf('₹ %s', ($format));
        }

        return $priceFiltered;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'core_rupee_extension';
    }
}