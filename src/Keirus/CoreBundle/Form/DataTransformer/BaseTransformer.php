<?php

namespace Keirus\CoreBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;


/**
 * Class BaseTransformer
 * @package Keirus\CoreBundle\Form\DataTransformer
 */
abstract class BaseTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;
    /**
     * @var
     */
    protected $entityClass;

    /**
     * @param ObjectManager $objectManager
     * @param $entityClass
     */
    public function __construct(ObjectManager $objectManager, $entityClass)
    {
        $this->objectManager = $objectManager;
        $this->entityClass = $entityClass;
    }

    /**
     * @param mixed $entity
     * @return string
     */
    public function transform($entity)
    {
        if (null === $entity) {
            return "";
        }

        return $entity->getId();
    }

    /**
     * @param mixed $id
     * @return object
     */
    public function reverseTransform($id)
    {
        $entity = $this->objectManager->getRepository($this->entityClass)->find($id);

        if (null === $entity) {
            throw new TransformationFailedException(
                sprintf(
                    'There is no entity of %s with id %s',
                    $this->entityClass,
                    $id
                )
            );
        }

        return $entity;
    }

    /**
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }


    /**
     * @return mixed
     */
    public function getEntityClass()
    {
        return $this->entityClass;
    }


}