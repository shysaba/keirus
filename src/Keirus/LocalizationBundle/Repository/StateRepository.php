<?php

namespace Keirus\LocalizationBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class StateRepository
 * @package Keirus\LocalizationBundle\Repository
 * @author shysaba
 */
class StateRepository extends EntityRepository
{

    /**
     * Find all states
     * @param string $sort
     * @param string $order
     * @return array
     */
    public function getAllStatesOrderBy($sort, $order)
    {
        return $this->createQueryBuilder('s')
            ->orderBy($sort, $order);
    }


    /**
     * Find all states by terms
     * @param $search
     * @param $sort
     * @param $order
     * @return array
     */
    public function getAllStatesBy($search, $sort, $order)
    {

        return $this->createQueryBuilder('s')
            ->select('s.id', 's.name as value')
            ->Where('s.name LIKE :name')
            ->setParameter('name', '%'.$search.'%')
            ->orderBy($sort, $order)
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllStatesPaginated($page, $rpp, $key = null)
    {
        $count = $this->getCountStates($key);

        $query = $this->createQueryBuilder('s')
            ->setMaxResults($rpp)
            ->setFirstResult(($page - 1) * $rpp)
            ->orderBy('s.name', 'ASC');

        if ($key <> null) {
            $query->Where('s.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $result = $query->getQuery()
            ->getResult();


        return array($result, $count);
    }

    /**
     * @param $key
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getCountStates($key)
    {
        $query = $this->createQueryBuilder('s');
        $query->select('COUNT(s)');

        if (!empty($key)) {
            $query->Where('s.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $menu = $query->getQuery()
            ->getSingleScalarResult();

        return $menu;
    }

}