<?php

namespace Keirus\LocalizationBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class CityRepository
 * @package Keirus\LocalizationBundle\Repository
 * @author shysaba
 */
class CityRepository extends EntityRepository
{

    /**
     * Find all cities
     * @param string $sort
     * @param string $order
     * @return array
     */
    public function getAllCitiesOrderBy($sort, $order)
    {
        return $this->createQueryBuilder('c')
            ->orderBy($sort, $order)
            ->getQuery();
    }


    public function getAllDetailed()
    {

        return $this->createQueryBuilder('c')
            ->leftJoin('c.state', 's')
            ->addSelect('s')
            ->orderBy('c.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllCitiesPaginated($page, $rpp, $key = null)
    {
        $count = $this->getCountCities($key);

        $query = $this->createQueryBuilder('c')
            ->leftJoin('c.state', 's')
            ->addSelect('s')
            ->setMaxResults($rpp)
            ->setFirstResult(($page - 1) * $rpp)
            ->orderBy('c.name', 'ASC');

        if ($key <> null) {
            $query->Where('c.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $result = $query->getQuery()
            ->getResult();


        return array($result, $count);
    }

    /**
     * @param $key
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getCountCities($key)
    {
        $query = $this->createQueryBuilder('c');
        $query->select('COUNT(c)');

        if (!empty($key)) {
            $query->Where('c.name LIKE :key')
                ->setParameter('key', '%'.$key.'%');
        }

        $menu = $query->getQuery()
            ->getSingleScalarResult();

        return $menu;
    }


    /**
     * Find all cities by terms
     * @param $search
     * @param $sort
     * @param $order
     * @return array
     */
    public function getAllCitiesBy($search, $sort, $order)
    {

        return $this->createQueryBuilder('c')
            ->select('c.id', 'c.name as value')
            ->Where('c.name LIKE :city')
            ->setParameter('city', '%'.$search.'%')
            ->orderBy($sort, $order)
            ->getQuery()
            ->getArrayResult();
    }
}