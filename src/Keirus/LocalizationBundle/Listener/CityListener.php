<?php

namespace Keirus\LocalizationBundle\Listener;

use Keirus\CoreBundle\Manager\LoggerManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

/**
 * Class CityListener
 * @package Keirus\LocalizationBundle\Listener
 */
class CityListener
{

    /**
     * @var LoggerManager
     */
    private $loggerManager;

    /**
     * @param LoggerManager $loggerManager
     */
    public function __construct(LoggerManager $loggerManager)
    {
        $this->loggerManager = $loggerManager;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function process(GetResponseEvent $event)
    {
        if ($event->getRequest()->attributes->get('_route') == "localization_city_delete") {
            $this->loggerManager->write();
        }
    }
}