<?php

namespace Keirus\LocalizationBundle\Form\Handler;


use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Keirus\CoreBundle\Form\Handler\BaseHandler;
use Keirus\LocalizationBundle\Manager\StateManager;

class StateHandler extends BaseHandler
{

    protected $stateManager;

    /**
     * @param Form $form
     * @param Request $request
     */
    public function __construct(Form $form, Request $request, SecurityContext $security, StateManager $stateManager)
    {
        parent::__construct($form, $request, $security);
        $this->stateManager = $stateManager;
    }


    /**
     *
     */
    protected function onSuccess()
    {
        $entity = $this->form->getData();

        $this->stateManager->save($entity);
    }

}