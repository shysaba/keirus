<?php

namespace Keirus\LocalizationBundle\Form\Handler;


use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Keirus\CoreBundle\Form\Handler\BaseHandler;
use Keirus\LocalizationBundle\Manager\CityManager;

class CityHandler extends BaseHandler
{

    protected $cityManager;

    /**
     * @param Form $form
     * @param Request $request
     */
    public function __construct(Form $form, Request $request, SecurityContext $security, CityManager $cityManager)
    {
        parent::__construct($form, $request, $security);
        $this->cityManager = $cityManager;
    }


    /**
     *
     */
    protected function onSuccess()
    {
        $entity = $this->form->getData();

        $this->cityManager->save($entity);
    }

}