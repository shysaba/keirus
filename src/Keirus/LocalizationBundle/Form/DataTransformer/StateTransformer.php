<?php

namespace Keirus\LocalizationBundle\Form\DataTransformer;

use Keirus\CoreBundle\Form\DataTransformer\BaseTransformer;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class StateTransformer
 * @package Keirus\LocalizationBundle\Form\DataTransformer
 */
class StateTransformer extends BaseTransformer
{

    /**
     * @param ObjectManager $objectManager
     * @param $entityClass
     */
    public function __construct(ObjectManager $objectManager, $entityClass)
    {
        parent::__construct($objectManager, $entityClass);
    }

}