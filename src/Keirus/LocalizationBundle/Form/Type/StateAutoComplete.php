<?php

namespace Keirus\LocalizationBundle\Form\Type;

use Keirus\CoreBundle\Form\Type\BaseAutoComplete;
use Keirus\LocalizationBundle\Form\DataTransformer\StateTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\Common\Persistence\ObjectManager;


/**
 * Class StateAutoComplete
 * @package Keirus\LocalizationBundle\Form\Type
 */
class StateAutoComplete extends BaseAutoComplete
{


    /**
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        parent::__construct($objectManager);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $class = $options['class'];

        $transformer = new StateTransformer(parent::getObjectManager(), $class);
        $builder->addViewTransformer($transformer);

    }
}