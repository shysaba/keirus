<?php

namespace Keirus\LocalizationBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class StateForm
 * @package Keirus\LocalizationBundle\Form\Type
 */
class StateForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('label' => 'admin.localization.state.name', 'error_bubbling' => true))
            ->add('code', 'text', array('label' => 'admin.localization.state.code', 'error_bubbling' => true))
            ->add('submit', 'submit');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'translation_domain' => 'admin',
                'data_class' => 'Keirus\LocalizationBundle\Entity\State'
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'localization_state_form';
    }
}