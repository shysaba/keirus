<?php

namespace Keirus\LocalizationBundle\Form\Type;

use Keirus\LocalizationBundle\Repository\StateRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class CityForm
 * @package Keirus\LocalizationBundle\Form\Type
 */
class CityForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('label' => 'admin.localization.city.name', 'error_bubbling' => true))
            ->add('pinCode', 'number', array('label' => 'admin.localization.city.pinCode', 'error_bubbling' => true))
            ->add(
                'state',
                'autocomplete_entity',
                [
                    'label' => 'admin.localization.state.name',
                    'error_bubbling' => true,
                    'class' => 'KeirusLocalizationBundle:State',
                    'update_route' => 'localization_states_list',
                    'required' => true,
                    'attr' => ['class' => 'form-control']
                ]
            )
            ->add('submit', 'submit');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'translation_domain' => 'admin',
                'data_class' => 'Keirus\LocalizationBundle\Entity\City'
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'localization_city_form';
    }
}