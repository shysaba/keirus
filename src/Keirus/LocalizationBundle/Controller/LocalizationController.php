<?php
namespace Keirus\LocalizationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class LocalizationController
 * @package Keirus\LocalizationBundle\Controller
 */
class LocalizationController extends Controller
{


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getStatesAction(Request $request)
    {
        $term = $request->query->get('term', null);

        $manager = $this->get('localization_state_manager');
        $states = $manager->getAllStatesBy($term, 's.name', 'ASC');

        return new JsonResponse($states);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getCitiesAction(Request $request)
    {
        $term = $request->query->get('term', null);

        $manager = $this->get('localization_city_manager');
        $states = $manager->getAllCitiesBy($term, 'c.name', 'ASC');

        return new JsonResponse($states);
    }

}