<?php
namespace Keirus\LocalizationBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Keirus\CoreBundle\Manager\BasePaginatorManager;
use Keirus\LocalizationBundle\Form\Type\CityForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keirus\LocalizationBundle\Entity\City;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class CityController extends Controller
{

    /**
     * @return mixed
     */
    public function listAction($page, $key)
    {

        return $this->dataResult($page, $key);
    }

    /**
     * @param $page
     * @param $key
     * @return Response
     */
    private function dataResult($page, $key)
    {

        $manager = $this->get('localization_city_manager');
        $rpp = $this->container->getParameter('elements_per_page');


        list($result, $totalCount) = $manager->getAllCitiesPaginated($page, $rpp, $key);

        $paginator = new BasePaginatorManager($page, $totalCount, $rpp);

        if ($totalCount < 1) {
            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.search.action',
                'admin.search.noResults'
            );
        }

        return $this->render(
            'KeirusLocalizationBundle:City:list.html.twig',
            array(
                'cities' => $result,
                'paginator' => $paginator->getPagesList(),
                'cur' => $page,
                'total' => $paginator->getTotalPages(),
                'key' => $key
            )
        );
    }

    /**
     * Search action
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $key = $request->request->get('key');
        $page = 1;

        return $this->dataResult($page, $key);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction()
    {

        $formHandler = $this->get('localization_city_handler');

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'info',
                'admin.action.created',
                'admin.localization.city.created'
            );

            return $this->redirect(
                $this->generateUrl('localization_city_list')
            );
        }

        return $this->render(
            'KeirusLocalizationBundle:City:create.html.twig',
            array('form' => $formHandler->getForm()->createView())
        );
    }


    /**
     * @param $id
     * @return RedirectResponse|Response
     */
    public function updateAction($id)
    {

        $manager = $this->get('localization_city_manager');
        $city = $manager->getCity($id);

        $formHandler = $this->get('localization_city_handler');
        $form = $this->createForm(new CityForm(), $city);
        $formHandler->setForm($form);

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'success',
                'admin.action.updated',
                'admin.localization.city.updated'
            );

            return $this->redirect(
                $this->generateUrl('localization_city_list')
            );
        }

        return $this->render(
            'KeirusLocalizationBundle:City:update.html.twig',
            array(
                'form' => $formHandler->getForm()->createView(),
                'city' => $city
            )
        );
    }


    /**
     * @param $id
     */
    public function deleteAction($id)
    {
        $manager = $this->get('localization_city_manager');
        $city = $manager->getCity($id);

        if ($city instanceof City) {
            $manager->removeAndFlush($city);

            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.action.deleted',
                'admin.localization.city.deleted'
            );
        } else {
            throw new NotFoundHttpException("City not found");
        }

        return $this->redirect($this->generateUrl('localization_city_list'));
    }


    /**
     * @param $id
     * @return Response
     * @throws EntityNotFoundException
     */
    public function showAction($id)
    {
        $manager = $this->get('localization_city_manager');
        $city = $manager->getCity($id);


        return $this->render('KeirusLocalizationBundle:City:show.html.twig', array('city' => $city));
    }
}