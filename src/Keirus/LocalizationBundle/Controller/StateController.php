<?php
namespace Keirus\LocalizationBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Keirus\LocalizationBundle\Form\Type\StateForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keirus\LocalizationBundle\Entity\State;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
Use Keirus\CoreBundle\Manager\BasePaginatorManager;

/**
 * Class StateController
 * @package Keirus\LocalizationBundle\Controller
 */
class StateController extends Controller
{


    /**
     * @param $page
     * @param $key
     * @return Response
     */
    public function listAction($page, $key)
    {
        return $this->dataResult($page, $key);
    }

    /**
     * @param $page
     * @param $key
     * @return Response
     */
    private function dataResult($page, $key)
    {

        $manager = $this->get('localization_state_manager');
        $rpp = $this->container->getParameter('elements_per_page');


        list($res, $totalCount) = $manager->getAllStatesPaginated($page, $rpp, $key);

        $paginator = new BasePaginatorManager($page, $totalCount, $rpp);

        if ($totalCount < 1) {
            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.search.action',
                'admin.search.noResults'
            );
        }

        return $this->render(
            'KeirusLocalizationBundle:State:list.html.twig',
            array(
                'states' => $res,
                'paginator' => $paginator->getPagesList(),
                'cur' => $page,
                'total' => $paginator->getTotalPages(),
                'key' => $key
            )
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $key = $request->request->get('key');
        $page = 1;

        return $this->dataResult($page, $key);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction()
    {

        $formHandler = $this->get('localization_state_handler');

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'info',
                'admin.action.created',
                'admin.localization.state.created'
            );

            return $this->redirect(
                $this->generateUrl('localization_state_list')
            );
        }

        return $this->render(
            'KeirusLocalizationBundle:State:create.html.twig',
            array('form' => $formHandler->getForm()->createView())
        );
    }


    /**
     * @param $id
     * @return RedirectResponse|Response
     */
    public function updateAction($id)
    {

        $manager = $this->get('localization_state_manager');
        $state = $manager->getState($id);

        $formHandler = $this->get('localization_state_handler');
        $form = $this->createForm(new StateForm(), $state);
        $formHandler->setForm($form);

        if ($formHandler->process()) {

            $this->get('core_flash_message_manager')->setMessage(
                'success',
                'admin.action.updated',
                'admin.localization.state.updated'
            );

            return $this->redirect(
                $this->generateUrl('localization_state_list')
            );
        }

        return $this->render(
            'KeirusLocalizationBundle:State:update.html.twig',
            array(
                'form' => $formHandler->getForm()->createView(),
                'state' => $state
            )
        );
    }


    /**
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAction($id)
    {
        $manager = $this->get('localization_state_manager');
        $state = $manager->getState($id);

        if ($state instanceof State) {
            $manager->removeAndFlush($state);

            $this->get('core_flash_message_manager')->setMessage(
                'danger',
                'admin.action.deleted',
                'admin.localization.state.deleted'
            );
        } else {
            throw new NotFoundHttpException("State not found");
        }

        return $this->redirect($this->generateUrl('localization_state_list'));
    }


    /**
     * @param $id
     * @return Response
     * @throws EntityNotFoundException
     */
    public function showAction($id)
    {
        $manager = $this->get('localization_state_manager');
        $state = $manager->getState($id);


        return $this->render('KeirusLocalizationBundle:State:show.html.twig', array('state' => $state));
    }
}