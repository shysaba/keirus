<?php
namespace Keirus\LocalizationBundle\Manager;

use Keirus\CoreBundle\Manager\BaseManager;
use Keirus\LocalizationBundle\Entity\State;
use Doctrine\ORM\EntityManager;

/**
 * Class StateManager
 * @package Keirus\LocalizationBundle\Manager
 */
class StateManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var \Keirus\LocalizationBundle\Repository\StateRepository
     */
    protected $repository;


    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('KeirusLocalizationBundle:State');
    }


    /**
     * Persist and flush a state
     * @param State $state
     */
    public function save(State $state)
    {
        $this->persistAndFlush($state);
    }

    /**
     * Delete a state
     * @param State $state
     * @param boolean $flush
     */
    public function delete(State $state, $flush = true)
    {
        if ($flush) {
            $this->removeAndFlush($state);
        } else {
            $this->remove($state);
        }
    }


    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }


    /**
     * @param $search
     * @param $sort
     * @param $order
     * @return array
     */
    public function getAllStatesBy($search, $sort, $order)
    {
        return $this->repository->getAllStatesBy($search, $sort, $order);
    }


    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllStatesPaginated($page, $rpp, $key = null)
    {
        return $this->repository->getAllStatesPaginated($page, $rpp, $key);
    }


    /**
     * @param $id
     * @return mixed
     */
    public function getState($id)
    {
        return $this->repository->find($id);
    }
}