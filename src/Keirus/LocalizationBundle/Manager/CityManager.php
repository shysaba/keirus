<?php
namespace Keirus\LocalizationBundle\Manager;

use Keirus\CoreBundle\Manager\BaseManager;
use Keirus\LocalizationBundle\Entity\City;
use Doctrine\ORM\EntityManager;


/**
 * Class CityManager
 * @package Keirus\LocalizationBundle\Manager
 */
class CityManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var \Keirus\LocalizationBundle\Repository\CityRepository
     */
    protected $repository;


    /**
     * Constructor
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('KeirusLocalizationBundle:City');
    }


    /**
     * Persist and flush a city
     * @param City $city
     */
    public function save(City $city)
    {
        $this->persistAndFlush($city);
    }

    /**
     * Delete a city
     * @param City $city
     * @param boolean $flush
     */
    public function delete(City $city, $flush = true)
    {
        if ($flush) {
            $this->removeAndFlush($city);
        } else {
            $this->remove($city);
        }
    }


    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @return mixed
     */
    public function getAllDetailed()
    {
        return $this->repository->getAllDetailed();
    }


    /**
     * @param $page
     * @param $rpp
     * @param null $key
     * @return array
     */
    public function getAllCitiesPaginated($page, $rpp, $key = null)
    {
        return $this->repository->getAllCitiesPaginated($page, $rpp, $key);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCity($id)
    {
        return $this->repository->find($id);
    }


    /**
     * @param $search
     * @param $sort
     * @param $order
     * @return array
     */
    public function getAllCitiesBy($search, $sort, $order)
    {
        return $this->repository->getAllCitiesBy($search, $sort, $order);
    }
}