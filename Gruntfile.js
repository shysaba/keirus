'use strict';
module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            // If any .less file changes in directory "build/less/" run the "less"-task.
            files: ["build/less/*.less", "build/less/skins/*.less", "dist/js/app.js"],
            tasks: ["less", "uglify"]
        },
        bowercopy: {
            options: {
                srcPrefix: 'app/Resources/lib',
                destPrefix: 'web/assets'
            },
            scripts: {
                files: {
                    'js/jquery.js': 'jquery/dist/jquery.js',
                    'js/bootstrap.js': 'bootstrap/dist/js/bootstrap.js',
                    'js/jquery.slimscroll.js': 'slimScroll/jquery.slimscroll.js',
                    'js/fastclick.js': 'fastclick/lib/fastclick.js',
                    'js/jquery-ui.js': 'jquery-ui/jquery-ui.js',
                    'js/bootstrap3-wysihtml5.all.min.js': 'bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.min.js',
                    'js/icheck.min.js': 'iCheck/icheck.min.js'
                }
            },
            stylesheets: {
                files: {
                    'css/bootstrap.css': 'bootstrap/dist/css/bootstrap.css',
                    'css/font-awesome.css': 'font-awesome/css/font-awesome.css',
                    'css/ionicons.css': 'ionicons/css/ionicons.css',
                    'css/jquery-ui.css': 'jquery-ui/themes/smoothness/jquery-ui.css',
                    'css/bootstrap3-wysihtml5.min.css': 'bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.min.css',
                    'css/square-blue.css': 'iCheck/skins/square/blue.css',
                    'css/flat-green.css': 'iCheck/skins/flat/green.css'
                }
            },
            fonts: {
                files: {
                    'fonts': ['font-awesome/fonts', 'bootstrap/fonts']
                }
            }
        },


        less: {
            // Development not compressed
            development: {
                options: {
                    compress: false
                },
                files: {
                    // compilation.css  :  source.less
                    "web/assets/css/main-admin.css": "src/keirus/CoreBundle/Resources/public/less/AdminLTE.less",
                    //Non minified skin files
                    "web/assets/css/skins/skin-blue.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-blue.less",
                    "web/assets/css/skins/skin-black.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-black.less",
                    "web/assets/css/skins/skin-yellow.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-yellow.less",
                    "web/assets/css/skins/skin-green.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-green.less",
                    "web/assets/css/skins/skin-red.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-red.less",
                    "web/assets/css/skins/skin-purple.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-purple.less",
                    "web/assets/css/skins/skin-blue-light.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-blue-light.less",
                    "web/assets/css/skins/skin-black-light.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-black-light.less",
                    "web/assets/css/skins/skin-yellow-light.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-yellow-light.less",
                    "web/assets/css/skins/skin-green-light.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-green-light.less",
                    "web/assets/css/skins/skin-red-light.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-red-light.less",
                    "web/assets/css/skins/skin-purple-light.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-purple-light.less",
                    "web/assets/css/skins/_all-skins.css": "src/keirus/CoreBundle/Resources/public/less/skins/_all-skins.less",
                    "web/assets/css/override.css": "src/keirus/CoreBundle/Resources/public/less/override.less"
                }
            },
            // Production compresses version
            production: {
                options: {
                    // Whether to compress or not
                    compress: true
                },
                files: {
                    // compilation.css  :  source.less
                    "web/assets/css/main-admin.min.css": "src/keirus/CoreBundle/Resources/public/less/AdminLTE.less",
                    //Non minified skin files
                    "web/assets/css/skins/skin-blue.min.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-blue.less",
                    "web/assets/css/skins/skin-black.min.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-black.less",
                    "web/assets/css/skins/skin-yellow.min.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-yellow.less",
                    "web/assets/css/skins/skin-green.min.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-green.less",
                    "web/assets/css/skins/skin-red.min.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-red.less",
                    "web/assets/css/skins/skin-purple.min.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-purple.less",
                    "web/assets/css/skins/skin-blue-light.min.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-blue-light.less",
                    "web/assets/css/skins/skin-black-light.min.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-black-light.less",
                    "web/assets/css/skins/skin-yellow-light.min.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-yellow-light.less",
                    "web/assets/css/skins/skin-green-light.min.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-green-light.less",
                    "web/assets/css/skins/skin-red-light.min.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-red-light.less",
                    "web/assets/css/skins/skin-purple-light.min.css": "src/keirus/CoreBundle/Resources/public/less/skins/skin-purple-light.less",
                    "web/assets/css/skins/_all-skins.min.css": "src/keirus/CoreBundle/Resources/public/less/skins/_all-skins.less"
                }
            }
        },
        // Uglify task info. Compress the js files.
        uglify: {
            options: {
                mangle: true,
                preserveComments: 'some'
            },
            my_target: {
                files: {
                    'web/assets/js/app.min.js': ['src/keirus/CoreBundle/Resources/public/js/app.js'],
                    'web/assets/js/project.min.js': ['src/keirus/CoreBundle/Resources/public/js/project.js'],
                    'web/assets/js/project-property.min.js': ['src/keirus/CoreBundle/Resources/public/js/project-property.js'],
                    'web/assets/js/apartment.min.js': ['src/keirus/CoreBundle/Resources/public/js/apartment.js'],
                    'web/assets/js/house.min.js': ['src/keirus/CoreBundle/Resources/public/js/house.js']
                }
            }
        },


        image: {
            static: {
                options: {
                    pngquant: true,
                    optipng: true,
                    advpng: true,
                    zopflipng: true,
                    pngcrush: true,
                    pngout: true,
                    mozjpeg: true,
                    jpegRecompress: true,
                    jpegoptim: true,
                    gifsicle: true,
                    svgo: true
                },
                files: {
                    'web/assets/css/blue.png': 'app/Resources/lib/iCheck/skins/square/blue.png',
                    'web/assets/css/green.png': 'app/Resources/lib/iCheck/skins/flat/green.png',
                    'web/assets/css/blue@2x.png': 'app/Resources/lib/iCheck/skins/square/blue@2x.png',
                    'web/assets/css/green@2x.png': 'app/Resources/lib/iCheck/skins/flat/green@2x.png'
                }
            }
        }
    });

    // Load all grunt tasks

    grunt.loadNpmTasks('grunt-bowercopy');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-image');

    grunt.registerTask('default', ['bowercopy', 'uglify', 'less', 'image']);
};